"use strict";
(function () {
this.md_NameVerifyRequest = function (
customerID,
options,
inFullName,
inCompany
)
{
///<summary>
/// 
///</summary>
///<param name="customerID"  type="String">
/// [Add Description]
///</param>
///<param name="options"  type="String">
/// [Add Description]
///</param>
///<param name="inFullName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCompany" optional="true" type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_NameVerifyRequest)) {
return new Sdk.md_NameVerifyRequest(customerID, options, inFullName, inCompany);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _CustomerID = null;
var _Options = null;
var _inFullName = null;
var _inCompany = null;

// internal validation functions

function _setValidCustomerID(value) {
 if (typeof value == "string") {
  _CustomerID = value;
 }
 else {
  throw new Error("Sdk.md_NameVerifyRequest CustomerID property is required and must be a String.")
 }
}

function _setValidOptions(value) {
 if (typeof value == "string") {
  _Options = value;
 }
 else {
  throw new Error("Sdk.md_NameVerifyRequest Options property is required and must be a String.")
 }
}

function _setValidInFullName(value) {
 if (value == null || typeof value == "string") {
  _inFullName = value;
 }
 else {
  throw new Error("Sdk.md_NameVerifyRequest InFullName property must be a String or null.")
 }
}

function _setValidInCompany(value) {
 if (value == null || typeof value == "string") {
  _inCompany = value;
 }
 else {
  throw new Error("Sdk.md_NameVerifyRequest InCompany property must be a String or null.")
 }
}

//Set internal properties from constructor parameters
  if (typeof customerID != "undefined") {
   _setValidCustomerID(customerID);
  }
  if (typeof options != "undefined") {
   _setValidOptions(options);
  }
  if (typeof inFullName != "undefined") {
   _setValidInFullName(inFullName);
  }
  if (typeof inCompany != "undefined") {
   _setValidInCompany(inCompany);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>CustomerID</b:key>",
           (_CustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _CustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Options</b:key>",
           (_Options == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Options, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inFullName</b:key>",
           (_inFullName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inFullName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCompany</b:key>",
           (_inCompany == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCompany, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_NameVerify</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_NameVerifyResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInFullName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInFullName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCompany = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCompany(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_NameVerifyRequest.__class = true;

this.md_NameVerifyResponse = function (responseXml) {
  ///<summary>
  /// Response to md_NameVerifyRequest
  ///</summary>
  if (!(this instanceof Sdk.md_NameVerifyResponse)) {
   return new Sdk.md_NameVerifyResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outFullName = null;
  var _outCompany = null;
  var _outGender = null;
  var _outGender2 = null;
  var _outNameFirst = null;
  var _outNameFirst2 = null;
  var _outNameLast = null;
  var _outNameLast2 = null;
  var _outNameMiddle = null;
  var _outNameMiddle2 = null;
  var _outNamePrefix = null;
  var _outNamePrefix2 = null;
  var _outNameSuffix = null;
  var _outNameSuffix2 = null;
  var _outRecordID = null;
  var _outResults = null;
  var _outVersion = null;
  var _outError = null;
  var _outTransmissionResults = null;

  // Internal property setter functions

  function _setOutFullName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outFullName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outFullName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCompany(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCompany']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCompany = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutGender(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outGender']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outGender = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutGender2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outGender2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outGender2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameFirst(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameFirst = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameFirst2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameFirst2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameLast(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameLast']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameLast = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameLast2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameLast2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameLast2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameMiddle(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameMiddle = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameMiddle2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameMiddle2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNamePrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNamePrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNamePrefix2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNamePrefix2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameSuffix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameSuffix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameSuffix2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameSuffix2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutRecordID(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outRecordID']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outRecordID = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutVersion(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outVersion']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outVersion = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutError(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outError = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTransmissionResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTransmissionResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTransmissionResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutFullName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outFullName;
  }
  this.getOutCompany = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCompany;
  }
  this.getOutGender = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outGender;
  }
  this.getOutGender2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outGender2;
  }
  this.getOutNameFirst = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameFirst;
  }
  this.getOutNameFirst2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameFirst2;
  }
  this.getOutNameLast = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameLast;
  }
  this.getOutNameLast2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameLast2;
  }
  this.getOutNameMiddle = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameMiddle;
  }
  this.getOutNameMiddle2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameMiddle2;
  }
  this.getOutNamePrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNamePrefix;
  }
  this.getOutNamePrefix2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNamePrefix2;
  }
  this.getOutNameSuffix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameSuffix;
  }
  this.getOutNameSuffix2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameSuffix2;
  }
  this.getOutRecordID = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outRecordID;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutVersion = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outVersion;
  }
  this.getOutError = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outError;
  }
  this.getOutTransmissionResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTransmissionResults;
  }

  //Set property values from responseXml constructor parameter
  _setOutFullName(responseXml);
  _setOutCompany(responseXml);
  _setOutGender(responseXml);
  _setOutGender2(responseXml);
  _setOutNameFirst(responseXml);
  _setOutNameFirst2(responseXml);
  _setOutNameLast(responseXml);
  _setOutNameLast2(responseXml);
  _setOutNameMiddle(responseXml);
  _setOutNameMiddle2(responseXml);
  _setOutNamePrefix(responseXml);
  _setOutNamePrefix2(responseXml);
  _setOutNameSuffix(responseXml);
  _setOutNameSuffix2(responseXml);
  _setOutRecordID(responseXml);
  _setOutResults(responseXml);
  _setOutVersion(responseXml);
  _setOutError(responseXml);
  _setOutTransmissionResults(responseXml);
 }
 this.md_NameVerifyResponse.__class = true;
}).call(Sdk)

Sdk.md_NameVerifyRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_NameVerifyResponse.prototype = new Sdk.OrganizationResponse();
