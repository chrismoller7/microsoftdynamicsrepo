"use strict";
(function () {
this.md_PersonatorActionRequest = function (
customerID,
options,
actions,
columns,
inAddressLine1,
inAddressLine2,
inCity,
inCompanyName,
inCountry,
inEmailAddress,
inFirstName,
inFreeForm,
inFullName,
inLastLine,
inLastName,
inPhoneNumber,
inPostalCode,
inRecordID,
inReserved,
inState
)
{
///<summary>
/// 
///</summary>
///<param name="customerID"  type="String">
/// [Add Description]
///</param>
///<param name="options"  type="String">
/// [Add Description]
///</param>
///<param name="actions"  type="String">
/// [Add Description]
///</param>
///<param name="columns" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine1" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine2" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCity" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCompanyName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCountry" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inEmailAddress" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inFirstName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inFreeForm" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inFullName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inLastLine" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inLastName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inPhoneNumber" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inPostalCode" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inRecordID" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inReserved" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inState" optional="true" type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_PersonatorActionRequest)) {
return new Sdk.md_PersonatorActionRequest(customerID, options, actions, columns, inAddressLine1, inAddressLine2, inCity, inCompanyName, inCountry, inEmailAddress, inFirstName, inFreeForm, inFullName, inLastLine, inLastName, inPhoneNumber, inPostalCode, inRecordID, inReserved, inState);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _CustomerID = null;
var _Options = null;
var _Actions = null;
var _Columns = null;
var _inAddressLine1 = null;
var _inAddressLine2 = null;
var _inCity = null;
var _inCompanyName = null;
var _inCountry = null;
var _inEmailAddress = null;
var _inFirstName = null;
var _inFreeForm = null;
var _inFullName = null;
var _inLastLine = null;
var _inLastName = null;
var _inPhoneNumber = null;
var _inPostalCode = null;
var _inRecordID = null;
var _inReserved = null;
var _inState = null;

// internal validation functions

function _setValidCustomerID(value) {
 if (typeof value == "string") {
  _CustomerID = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest CustomerID property is required and must be a String.")
 }
}

function _setValidOptions(value) {
 if (typeof value == "string") {
  _Options = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest Options property is required and must be a String.")
 }
}

function _setValidActions(value) {
 if (typeof value == "string") {
  _Actions = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest Actions property is required and must be a String.")
 }
}

function _setValidColumns(value) {
 if (value == null || typeof value == "string") {
  _Columns = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest Columns property must be a String or null.")
 }
}

function _setValidInAddressLine1(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine1 = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InAddressLine1 property must be a String or null.")
 }
}

function _setValidInAddressLine2(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine2 = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InAddressLine2 property must be a String or null.")
 }
}

function _setValidInCity(value) {
 if (value == null || typeof value == "string") {
  _inCity = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InCity property must be a String or null.")
 }
}

function _setValidInCompanyName(value) {
 if (value == null || typeof value == "string") {
  _inCompanyName = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InCompanyName property must be a String or null.")
 }
}

function _setValidInCountry(value) {
 if (value == null || typeof value == "string") {
  _inCountry = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InCountry property must be a String or null.")
 }
}

function _setValidInEmailAddress(value) {
 if (value == null || typeof value == "string") {
  _inEmailAddress = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InEmailAddress property must be a String or null.")
 }
}

function _setValidInFirstName(value) {
 if (value == null || typeof value == "string") {
  _inFirstName = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InFirstName property must be a String or null.")
 }
}

function _setValidInFreeForm(value) {
 if (value == null || typeof value == "string") {
  _inFreeForm = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InFreeForm property must be a String or null.")
 }
}

function _setValidInFullName(value) {
 if (value == null || typeof value == "string") {
  _inFullName = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InFullName property must be a String or null.")
 }
}

function _setValidInLastLine(value) {
 if (value == null || typeof value == "string") {
  _inLastLine = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InLastLine property must be a String or null.")
 }
}

function _setValidInLastName(value) {
 if (value == null || typeof value == "string") {
  _inLastName = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InLastName property must be a String or null.")
 }
}

function _setValidInPhoneNumber(value) {
 if (value == null || typeof value == "string") {
  _inPhoneNumber = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InPhoneNumber property must be a String or null.")
 }
}

function _setValidInPostalCode(value) {
 if (value == null || typeof value == "string") {
  _inPostalCode = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InPostalCode property must be a String or null.")
 }
}

function _setValidInRecordID(value) {
 if (value == null || typeof value == "string") {
  _inRecordID = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InRecordID property must be a String or null.")
 }
}

function _setValidInReserved(value) {
 if (value == null || typeof value == "string") {
  _inReserved = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InReserved property must be a String or null.")
 }
}

function _setValidInState(value) {
 if (value == null || typeof value == "string") {
  _inState = value;
 }
 else {
  throw new Error("Sdk.md_PersonatorActionRequest InState property must be a String or null.")
 }
}

//Set internal properties from constructor parameters
  if (typeof customerID != "undefined") {
   _setValidCustomerID(customerID);
  }
  if (typeof options != "undefined") {
   _setValidOptions(options);
  }
  if (typeof actions != "undefined") {
   _setValidActions(actions);
  }
  if (typeof columns != "undefined") {
   _setValidColumns(columns);
  }
  if (typeof inAddressLine1 != "undefined") {
   _setValidInAddressLine1(inAddressLine1);
  }
  if (typeof inAddressLine2 != "undefined") {
   _setValidInAddressLine2(inAddressLine2);
  }
  if (typeof inCity != "undefined") {
   _setValidInCity(inCity);
  }
  if (typeof inCompanyName != "undefined") {
   _setValidInCompanyName(inCompanyName);
  }
  if (typeof inCountry != "undefined") {
   _setValidInCountry(inCountry);
  }
  if (typeof inEmailAddress != "undefined") {
   _setValidInEmailAddress(inEmailAddress);
  }
  if (typeof inFirstName != "undefined") {
   _setValidInFirstName(inFirstName);
  }
  if (typeof inFreeForm != "undefined") {
   _setValidInFreeForm(inFreeForm);
  }
  if (typeof inFullName != "undefined") {
   _setValidInFullName(inFullName);
  }
  if (typeof inLastLine != "undefined") {
   _setValidInLastLine(inLastLine);
  }
  if (typeof inLastName != "undefined") {
   _setValidInLastName(inLastName);
  }
  if (typeof inPhoneNumber != "undefined") {
   _setValidInPhoneNumber(inPhoneNumber);
  }
  if (typeof inPostalCode != "undefined") {
   _setValidInPostalCode(inPostalCode);
  }
  if (typeof inRecordID != "undefined") {
   _setValidInRecordID(inRecordID);
  }
  if (typeof inReserved != "undefined") {
   _setValidInReserved(inReserved);
  }
  if (typeof inState != "undefined") {
   _setValidInState(inState);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>CustomerID</b:key>",
           (_CustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _CustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Options</b:key>",
           (_Options == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Options, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Actions</b:key>",
           (_Actions == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Actions, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Columns</b:key>",
           (_Columns == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Columns, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine1</b:key>",
           (_inAddressLine1 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine1, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine2</b:key>",
           (_inAddressLine2 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine2, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCity</b:key>",
           (_inCity == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCity, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCompanyName</b:key>",
           (_inCompanyName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCompanyName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountry</b:key>",
           (_inCountry == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountry, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inEmailAddress</b:key>",
           (_inEmailAddress == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inEmailAddress, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inFirstName</b:key>",
           (_inFirstName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inFirstName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inFreeForm</b:key>",
           (_inFreeForm == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inFreeForm, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inFullName</b:key>",
           (_inFullName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inFullName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inLastLine</b:key>",
           (_inLastLine == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inLastLine, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inLastName</b:key>",
           (_inLastName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inLastName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPhoneNumber</b:key>",
           (_inPhoneNumber == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPhoneNumber, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPostalCode</b:key>",
           (_inPostalCode == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPostalCode, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inRecordID</b:key>",
           (_inRecordID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inRecordID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inReserved</b:key>",
           (_inReserved == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inReserved, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inState</b:key>",
           (_inState == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inState, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_PersonatorAction</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_PersonatorActionResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setActions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidActions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setColumns = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidColumns(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine1 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine1(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine2 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine2(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCity = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCity(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCompanyName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCompanyName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountry(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInEmailAddress = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInEmailAddress(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInFirstName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInFirstName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInFreeForm = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInFreeForm(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInFullName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInFullName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInLastLine = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInLastLine(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInLastName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInLastName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPhoneNumber = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPhoneNumber(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPostalCode = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPostalCode(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInRecordID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInRecordID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInReserved = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInReserved(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInState = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInState(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_PersonatorActionRequest.__class = true;

this.md_PersonatorActionResponse = function (responseXml) {
  ///<summary>
  /// Response to md_PersonatorActionRequest
  ///</summary>
  if (!(this instanceof Sdk.md_PersonatorActionResponse)) {
   return new Sdk.md_PersonatorActionResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _transmissionResults = null;
  var _errorMessage = null;
  var _outAddressDeliveryInstallation = null;
  var _outAddressExtras = null;
  var _outAddressHouseNumber = null;
  var _outAddressKey = null;
  var _outAddressLine1 = null;
  var _outAddressLine2 = null;
  var _outAddressLockBox = null;
  var _outAddressPostDirection = null;
  var _outAddressPreDirection = null;
  var _outAddressPrivateMailboxName = null;
  var _outAddressPrivateMailboxRange = null;
  var _outAddressRouteService = null;
  var _outAddressStreetName = null;
  var _outAddressStreetSuffix = null;
  var _outAddressSuiteName = null;
  var _outAddressSuiteNumber = null;
  var _outAddressTypeCode = null;
  var _outAreaCode = null;
  var _outCarrierRoute = null;
  var _outCBSACode = null;
  var _outCBSADivisionCode = null;
  var _outCBSADivisionLevel = null;
  var _outCBSADivisionTitle = null;
  var _outCBSALevel = null;
  var _outCBSATitle = null;
  var _outCensusBlock = null;
  var _outCensusKey = null;
  var _outCensusTract = null;
  var _outCity = null;
  var _outCityAbbreviation = null;
  var _outCompanyName = null;
  var _outCongressionalDistrict = null;
  var _outCountryCode = null;
  var _outCountryName = null;
  var _outCountyFIPS = null;
  var _outCountyName = null;
  var _outCountySubdivisionCode = null;
  var _outCountySubdivisionName = null;
  var _outDateOfBirth = null;
  var _outDateOfDeath = null;
  var _outDeliveryIndicator = null;
  var _outDeliveryPointCheckDigit = null;
  var _outDeliveryPointCode = null;
  var _outDemographicsGender = null;
  var _outDemographicsResults = null;
  var _outDomainName = null;
  var _outElementarySchoolDistrictCode = null;
  var _outElementarySchoolDistrictName = null;
  var _outEmailAddress = null;
  var _outGender = null;
  var _outGender2 = null;
  var _outHouseholdIncome = null;
  var _outLatitude = null;
  var _outLengthOfResidence = null;
  var _outLongitude = null;
  var _outMailboxName = null;
  var _outMaritalStatus = null;
  var _outMelissaAddressKey = null;
  var _outNameFirst = null;
  var _outNameFirst2 = null;
  var _outNameFull = null;
  var _outNameLast = null;
  var _outNameLast2 = null;
  var _outNameMiddle = null;
  var _outNameMiddle2 = null;
  var _outNamePrefix = null;
  var _outNamePrefix2 = null;
  var _outNameSuffix = null;
  var _outNameSuffix2 = null;
  var _outNewAreaCode = null;
  var _outOccupation = null;
  var _outOwnRent = null;
  var _outPhoneCountryCode = null;
  var _outPhoneCountryName = null;
  var _outPhoneExtension = null;
  var _outPhoneNumber = null;
  var _outPhonePrefix = null;
  var _outPhoneSuffix = null;
  var _outPlaceCode = null;
  var _outPlaceName = null;
  var _outPlus4 = null;
  var _outPostalCode = null;
  var _outPresenceOfChildren = null;
  var _outPrivateMailBox = null;
  var _outRecordExtras = null;
  var _outRecordID = null;
  var _outReserved = null;
  var _outResults = null;
  var _outSalutation = null;
  var _outSecondarySchoolDistrictCode = null;
  var _outSecondarySchoolDistrictName = null;
  var _outState = null;
  var _outStateDistrictLower = null;
  var _outStateDistrictUpper = null;
  var _outStateName = null;
  var _outSuite = null;
  var _outTopLevelDomain = null;
  var _outUnifiedSchoolDistrictCode = null;
  var _outUnifiedSchoolDistrictName = null;
  var _outUrbanizationName = null;
  var _outUTC = null;
  var _outError = null;

  // Internal property setter functions

  function _setTransmissionResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='TransmissionResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _transmissionResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setErrorMessage(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='ErrorMessage']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _errorMessage = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressDeliveryInstallation(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressDeliveryInstallation']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressDeliveryInstallation = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressExtras(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressExtras']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressExtras = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressHouseNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressHouseNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressHouseNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressKey(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressKey']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressKey = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLockBox(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLockBox']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLockBox = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressPostDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressPostDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressPostDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressPreDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressPreDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressPreDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressPrivateMailboxName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressPrivateMailboxName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressPrivateMailboxName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressPrivateMailboxRange(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressPrivateMailboxRange']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressPrivateMailboxRange = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressRouteService(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressRouteService']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressRouteService = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressStreetName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressStreetName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressStreetName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressStreetSuffix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressStreetSuffix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressStreetSuffix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressSuiteName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressSuiteName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressSuiteName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressSuiteNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressSuiteNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressSuiteNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressTypeCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressTypeCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressTypeCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAreaCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAreaCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAreaCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCarrierRoute(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCarrierRoute']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCarrierRoute = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSACode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSACode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSACode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSADivisionCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSADivisionCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSADivisionLevel(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionLevel']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSADivisionLevel = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSADivisionTitle(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionTitle']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSADivisionTitle = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSALevel(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSALevel']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSALevel = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCBSATitle(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCBSATitle']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCBSATitle = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCensusBlock(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCensusBlock']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCensusBlock = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCensusKey(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCensusKey']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCensusKey = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCensusTract(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCensusTract']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCensusTract = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCity(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCity']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCity = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCityAbbreviation(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCityAbbreviation']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCityAbbreviation = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCompanyName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCompanyName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCompanyName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCongressionalDistrict(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCongressionalDistrict']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCongressionalDistrict = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountryCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountryCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountryCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountryName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountryName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountryName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountyFIPS(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountyFIPS']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountyFIPS = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountyName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountyName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountyName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountySubdivisionCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountySubdivisionCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountySubdivisionCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountySubdivisionName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountySubdivisionName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountySubdivisionName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDateOfBirth(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDateOfBirth']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDateOfBirth = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDateOfDeath(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDateOfDeath']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDateOfDeath = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDeliveryIndicator(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryIndicator']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDeliveryIndicator = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDeliveryPointCheckDigit(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryPointCheckDigit']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDeliveryPointCheckDigit = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDeliveryPointCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryPointCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDeliveryPointCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDemographicsGender(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDemographicsGender']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDemographicsGender = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDemographicsResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDemographicsResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDemographicsResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDomainName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDomainName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDomainName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutElementarySchoolDistrictCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outElementarySchoolDistrictCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outElementarySchoolDistrictCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutElementarySchoolDistrictName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outElementarySchoolDistrictName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outElementarySchoolDistrictName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutEmailAddress(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outEmailAddress']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outEmailAddress = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutGender(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outGender']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outGender = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutGender2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outGender2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outGender2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutHouseholdIncome(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outHouseholdIncome']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outHouseholdIncome = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLatitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLatitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLengthOfResidence(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLengthOfResidence']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLengthOfResidence = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLongitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLongitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMailboxName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMailboxName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMailboxName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMaritalStatus(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMaritalStatus']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMaritalStatus = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMelissaAddressKey(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMelissaAddressKey']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMelissaAddressKey = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameFirst(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameFirst = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameFirst2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameFirst2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameFull(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameFull']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameFull = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameLast(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameLast']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameLast = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameLast2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameLast2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameLast2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameMiddle(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameMiddle = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameMiddle2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameMiddle2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNamePrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNamePrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNamePrefix2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNamePrefix2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameSuffix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameSuffix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNameSuffix2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNameSuffix2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNewAreaCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNewAreaCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNewAreaCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutOccupation(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outOccupation']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outOccupation = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutOwnRent(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outOwnRent']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outOwnRent = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneCountryCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneCountryCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneCountryName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneCountryName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneExtension(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneExtension']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneExtension = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhonePrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhonePrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhonePrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneSuffix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneSuffix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneSuffix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlaceCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlaceCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlaceCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlaceName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlaceName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlaceName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlus4(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlus4']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlus4 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPostalCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPostalCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPostalCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPresenceOfChildren(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPresenceOfChildren']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPresenceOfChildren = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPrivateMailBox(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPrivateMailBox']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPrivateMailBox = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutRecordExtras(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outRecordExtras']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outRecordExtras = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutRecordID(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outRecordID']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outRecordID = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutReserved(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outReserved']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outReserved = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSalutation(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSalutation']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSalutation = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSecondarySchoolDistrictCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSecondarySchoolDistrictCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSecondarySchoolDistrictCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSecondarySchoolDistrictName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSecondarySchoolDistrictName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSecondarySchoolDistrictName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutState(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outState']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outState = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutStateDistrictLower(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outStateDistrictLower']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outStateDistrictLower = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutStateDistrictUpper(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outStateDistrictUpper']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outStateDistrictUpper = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutStateName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outStateName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outStateName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSuite(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSuite']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSuite = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTopLevelDomain(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTopLevelDomain']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTopLevelDomain = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUnifiedSchoolDistrictCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUnifiedSchoolDistrictCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUnifiedSchoolDistrictCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUnifiedSchoolDistrictName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUnifiedSchoolDistrictName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUnifiedSchoolDistrictName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUrbanizationName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUrbanizationName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUrbanizationName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUTC(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUTC']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUTC = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutError(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outError = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getTransmissionResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _transmissionResults;
  }
  this.getErrorMessage = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _errorMessage;
  }
  this.getOutAddressDeliveryInstallation = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressDeliveryInstallation;
  }
  this.getOutAddressExtras = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressExtras;
  }
  this.getOutAddressHouseNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressHouseNumber;
  }
  this.getOutAddressKey = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressKey;
  }
  this.getOutAddressLine1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine1;
  }
  this.getOutAddressLine2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine2;
  }
  this.getOutAddressLockBox = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLockBox;
  }
  this.getOutAddressPostDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressPostDirection;
  }
  this.getOutAddressPreDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressPreDirection;
  }
  this.getOutAddressPrivateMailboxName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressPrivateMailboxName;
  }
  this.getOutAddressPrivateMailboxRange = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressPrivateMailboxRange;
  }
  this.getOutAddressRouteService = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressRouteService;
  }
  this.getOutAddressStreetName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressStreetName;
  }
  this.getOutAddressStreetSuffix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressStreetSuffix;
  }
  this.getOutAddressSuiteName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressSuiteName;
  }
  this.getOutAddressSuiteNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressSuiteNumber;
  }
  this.getOutAddressTypeCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressTypeCode;
  }
  this.getOutAreaCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAreaCode;
  }
  this.getOutCarrierRoute = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCarrierRoute;
  }
  this.getOutCBSACode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSACode;
  }
  this.getOutCBSADivisionCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSADivisionCode;
  }
  this.getOutCBSADivisionLevel = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSADivisionLevel;
  }
  this.getOutCBSADivisionTitle = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSADivisionTitle;
  }
  this.getOutCBSALevel = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSALevel;
  }
  this.getOutCBSATitle = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCBSATitle;
  }
  this.getOutCensusBlock = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCensusBlock;
  }
  this.getOutCensusKey = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCensusKey;
  }
  this.getOutCensusTract = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCensusTract;
  }
  this.getOutCity = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCity;
  }
  this.getOutCityAbbreviation = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCityAbbreviation;
  }
  this.getOutCompanyName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCompanyName;
  }
  this.getOutCongressionalDistrict = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCongressionalDistrict;
  }
  this.getOutCountryCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountryCode;
  }
  this.getOutCountryName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountryName;
  }
  this.getOutCountyFIPS = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountyFIPS;
  }
  this.getOutCountyName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountyName;
  }
  this.getOutCountySubdivisionCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountySubdivisionCode;
  }
  this.getOutCountySubdivisionName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountySubdivisionName;
  }
  this.getOutDateOfBirth = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDateOfBirth;
  }
  this.getOutDateOfDeath = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDateOfDeath;
  }
  this.getOutDeliveryIndicator = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDeliveryIndicator;
  }
  this.getOutDeliveryPointCheckDigit = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDeliveryPointCheckDigit;
  }
  this.getOutDeliveryPointCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDeliveryPointCode;
  }
  this.getOutDemographicsGender = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDemographicsGender;
  }
  this.getOutDemographicsResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDemographicsResults;
  }
  this.getOutDomainName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDomainName;
  }
  this.getOutElementarySchoolDistrictCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outElementarySchoolDistrictCode;
  }
  this.getOutElementarySchoolDistrictName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outElementarySchoolDistrictName;
  }
  this.getOutEmailAddress = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outEmailAddress;
  }
  this.getOutGender = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outGender;
  }
  this.getOutGender2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outGender2;
  }
  this.getOutHouseholdIncome = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outHouseholdIncome;
  }
  this.getOutLatitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLatitude;
  }
  this.getOutLengthOfResidence = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLengthOfResidence;
  }
  this.getOutLongitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLongitude;
  }
  this.getOutMailboxName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMailboxName;
  }
  this.getOutMaritalStatus = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMaritalStatus;
  }
  this.getOutMelissaAddressKey = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMelissaAddressKey;
  }
  this.getOutNameFirst = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameFirst;
  }
  this.getOutNameFirst2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameFirst2;
  }
  this.getOutNameFull = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameFull;
  }
  this.getOutNameLast = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameLast;
  }
  this.getOutNameLast2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameLast2;
  }
  this.getOutNameMiddle = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameMiddle;
  }
  this.getOutNameMiddle2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameMiddle2;
  }
  this.getOutNamePrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNamePrefix;
  }
  this.getOutNamePrefix2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNamePrefix2;
  }
  this.getOutNameSuffix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameSuffix;
  }
  this.getOutNameSuffix2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNameSuffix2;
  }
  this.getOutNewAreaCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNewAreaCode;
  }
  this.getOutOccupation = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outOccupation;
  }
  this.getOutOwnRent = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outOwnRent;
  }
  this.getOutPhoneCountryCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneCountryCode;
  }
  this.getOutPhoneCountryName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneCountryName;
  }
  this.getOutPhoneExtension = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneExtension;
  }
  this.getOutPhoneNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNumber;
  }
  this.getOutPhonePrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhonePrefix;
  }
  this.getOutPhoneSuffix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneSuffix;
  }
  this.getOutPlaceCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlaceCode;
  }
  this.getOutPlaceName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlaceName;
  }
  this.getOutPlus4 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlus4;
  }
  this.getOutPostalCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPostalCode;
  }
  this.getOutPresenceOfChildren = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPresenceOfChildren;
  }
  this.getOutPrivateMailBox = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPrivateMailBox;
  }
  this.getOutRecordExtras = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outRecordExtras;
  }
  this.getOutRecordID = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outRecordID;
  }
  this.getOutReserved = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outReserved;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutSalutation = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSalutation;
  }
  this.getOutSecondarySchoolDistrictCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSecondarySchoolDistrictCode;
  }
  this.getOutSecondarySchoolDistrictName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSecondarySchoolDistrictName;
  }
  this.getOutState = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outState;
  }
  this.getOutStateDistrictLower = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outStateDistrictLower;
  }
  this.getOutStateDistrictUpper = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outStateDistrictUpper;
  }
  this.getOutStateName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outStateName;
  }
  this.getOutSuite = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSuite;
  }
  this.getOutTopLevelDomain = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTopLevelDomain;
  }
  this.getOutUnifiedSchoolDistrictCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUnifiedSchoolDistrictCode;
  }
  this.getOutUnifiedSchoolDistrictName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUnifiedSchoolDistrictName;
  }
  this.getOutUrbanizationName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUrbanizationName;
  }
  this.getOutUTC = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUTC;
  }
  this.getOutError = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outError;
  }

  //Set property values from responseXml constructor parameter
  _setTransmissionResults(responseXml);
  _setErrorMessage(responseXml);
  _setOutAddressDeliveryInstallation(responseXml);
  _setOutAddressExtras(responseXml);
  _setOutAddressHouseNumber(responseXml);
  _setOutAddressKey(responseXml);
  _setOutAddressLine1(responseXml);
  _setOutAddressLine2(responseXml);
  _setOutAddressLockBox(responseXml);
  _setOutAddressPostDirection(responseXml);
  _setOutAddressPreDirection(responseXml);
  _setOutAddressPrivateMailboxName(responseXml);
  _setOutAddressPrivateMailboxRange(responseXml);
  _setOutAddressRouteService(responseXml);
  _setOutAddressStreetName(responseXml);
  _setOutAddressStreetSuffix(responseXml);
  _setOutAddressSuiteName(responseXml);
  _setOutAddressSuiteNumber(responseXml);
  _setOutAddressTypeCode(responseXml);
  _setOutAreaCode(responseXml);
  _setOutCarrierRoute(responseXml);
  _setOutCBSACode(responseXml);
  _setOutCBSADivisionCode(responseXml);
  _setOutCBSADivisionLevel(responseXml);
  _setOutCBSADivisionTitle(responseXml);
  _setOutCBSALevel(responseXml);
  _setOutCBSATitle(responseXml);
  _setOutCensusBlock(responseXml);
  _setOutCensusKey(responseXml);
  _setOutCensusTract(responseXml);
  _setOutCity(responseXml);
  _setOutCityAbbreviation(responseXml);
  _setOutCompanyName(responseXml);
  _setOutCongressionalDistrict(responseXml);
  _setOutCountryCode(responseXml);
  _setOutCountryName(responseXml);
  _setOutCountyFIPS(responseXml);
  _setOutCountyName(responseXml);
  _setOutCountySubdivisionCode(responseXml);
  _setOutCountySubdivisionName(responseXml);
  _setOutDateOfBirth(responseXml);
  _setOutDateOfDeath(responseXml);
  _setOutDeliveryIndicator(responseXml);
  _setOutDeliveryPointCheckDigit(responseXml);
  _setOutDeliveryPointCode(responseXml);
  _setOutDemographicsGender(responseXml);
  _setOutDemographicsResults(responseXml);
  _setOutDomainName(responseXml);
  _setOutElementarySchoolDistrictCode(responseXml);
  _setOutElementarySchoolDistrictName(responseXml);
  _setOutEmailAddress(responseXml);
  _setOutGender(responseXml);
  _setOutGender2(responseXml);
  _setOutHouseholdIncome(responseXml);
  _setOutLatitude(responseXml);
  _setOutLengthOfResidence(responseXml);
  _setOutLongitude(responseXml);
  _setOutMailboxName(responseXml);
  _setOutMaritalStatus(responseXml);
  _setOutMelissaAddressKey(responseXml);
  _setOutNameFirst(responseXml);
  _setOutNameFirst2(responseXml);
  _setOutNameFull(responseXml);
  _setOutNameLast(responseXml);
  _setOutNameLast2(responseXml);
  _setOutNameMiddle(responseXml);
  _setOutNameMiddle2(responseXml);
  _setOutNamePrefix(responseXml);
  _setOutNamePrefix2(responseXml);
  _setOutNameSuffix(responseXml);
  _setOutNameSuffix2(responseXml);
  _setOutNewAreaCode(responseXml);
  _setOutOccupation(responseXml);
  _setOutOwnRent(responseXml);
  _setOutPhoneCountryCode(responseXml);
  _setOutPhoneCountryName(responseXml);
  _setOutPhoneExtension(responseXml);
  _setOutPhoneNumber(responseXml);
  _setOutPhonePrefix(responseXml);
  _setOutPhoneSuffix(responseXml);
  _setOutPlaceCode(responseXml);
  _setOutPlaceName(responseXml);
  _setOutPlus4(responseXml);
  _setOutPostalCode(responseXml);
  _setOutPresenceOfChildren(responseXml);
  _setOutPrivateMailBox(responseXml);
  _setOutRecordExtras(responseXml);
  _setOutRecordID(responseXml);
  _setOutReserved(responseXml);
  _setOutResults(responseXml);
  _setOutSalutation(responseXml);
  _setOutSecondarySchoolDistrictCode(responseXml);
  _setOutSecondarySchoolDistrictName(responseXml);
  _setOutState(responseXml);
  _setOutStateDistrictLower(responseXml);
  _setOutStateDistrictUpper(responseXml);
  _setOutStateName(responseXml);
  _setOutSuite(responseXml);
  _setOutTopLevelDomain(responseXml);
  _setOutUnifiedSchoolDistrictCode(responseXml);
  _setOutUnifiedSchoolDistrictName(responseXml);
  _setOutUrbanizationName(responseXml);
  _setOutUTC(responseXml);
  _setOutError(responseXml);
 }
 this.md_PersonatorActionResponse.__class = true;
}).call(Sdk)

Sdk.md_PersonatorActionRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_PersonatorActionResponse.prototype = new Sdk.OrganizationResponse();
