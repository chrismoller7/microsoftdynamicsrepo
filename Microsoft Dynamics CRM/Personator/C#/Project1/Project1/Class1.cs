﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace Project1
{
    class Class1
    {
        //public static String INPUTFILE = @"C:\Users\Samuel\Desktop\Customer Data\FW State Level Senate and House Data\file for melissa data";

        static void Main(string[] args)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            EndpointAddress address = new EndpointAddress("https://personator.melissadata.net/v3/SOAP/ContactVerify");

            Personator.ServicemdContactVerifySOAPClient service = new Personator.ServicemdContactVerifySOAPClient(binding, address);

            Personator.Request request = new Personator.Request();
            Personator.Response response = new Personator.Response();
            Personator.RequestRecord reqRecord = new Personator.RequestRecord();

            request.CustomerID = "114718629";
            request.TransmissionReference = "Microsoft Dynamics CRM Personator";
            request.Actions = "Check,Append";
            request.Options = "Address,Always";
            request.Columns = "StateDistrictUpper,StateDistrictLower";

            var reader = new StreamReader(File.OpenRead(@"C:\Users\Samuel\Desktop\Customer Data\FW State Level Senate and House Data\file for melissa data.csv"));
            List<string> listA = new List<string>();
            List<string> listB = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');

                listA.Add(values[0]);
                listB.Add(values[1]);
            }
            Debug.WriteLine(listA[0]);
        }
    }
}
