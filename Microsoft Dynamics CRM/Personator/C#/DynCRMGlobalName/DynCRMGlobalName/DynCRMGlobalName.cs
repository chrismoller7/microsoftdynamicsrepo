﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace DynCRMGlobalName
{
    public class DynCRMGlobalName : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inFullName")]
        [Output("outFullName")]
        public InOutArgument<String> FullName { get; set; }

        [Input("inCompany")]
        [Output("outCompany")]
        public InOutArgument<String> Company { get; set; }

        [Output("outGender")]
        public OutArgument<String> Gender { get; set; }

        [Output("outGender2")]
        public OutArgument<String> Gender2 { get; set; }

        [Output("outNameFirst")]
        public OutArgument<String> NameFirst { get; set; }

        [Output("outNameFirst2")]
        public OutArgument<String> NameFirst2 { get; set; }

        [Output("outNameLast")]
        public OutArgument<String> NameLast { get; set; }

        [Output("outNameLast2")]
        public OutArgument<String> NameLast2 { get; set; }

        [Output("outNameMiddle")]
        public OutArgument<String> NameMiddle { get; set; }

        [Output("outNameMiddle2")]
        public OutArgument<String> NameMiddle2 { get; set; }

        [Output("outNamePrefix")]
        public OutArgument<String> NamePrefix { get; set; }

        [Output("outNamePrefix2")]
        public OutArgument<String> NamePrefix2 { get; set; }

        [Output("outNameSuffix")]
        public OutArgument<String> NameSuffix { get; set; }

        [Output("outNameSuffix2")]
        public OutArgument<String> NameSuffix2 { get; set; }

        [Output("outRecordID")]
        public OutArgument<String> RecordID { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outVersion")]
        public OutArgument<String> Version { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            EndpointAddress address = new EndpointAddress("https://GlobalName.melissadata.net/v3/SOAP/GlobalName");

            GlobalName.GlobalNameSOAP service = new GlobalName.GlobalNameSOAP();

            GlobalName.Request request = new GlobalName.Request();
            GlobalName.Response response = new GlobalName.Response();
            GlobalName.RequestRecord reqRecord = new GlobalName.RequestRecord();

            request.CustomerID = CustomerID.Get(context);
            request.TransmissionReference = "Microsoft Dynamics CRM Global Name";
            request.Options = Options.Get(context);

            request.Records = new GlobalName.RequestRecord[1];
            reqRecord.RecordID = "1";
            reqRecord.FullName = FullName.Get(context);
            reqRecord.Company = Company.Get(context);

            request.Records[0] = reqRecord;

            string errorMessage = "";
            try
            {
                response = service.doGlobalName(request);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact melissa data support. Error Message: " + ex.Message;
            }

            String temp = response.ToString();
            XmlSerializer XmlSer = new XmlSerializer(response.GetType());
            StringWriter textWriter = new StringWriter();
            XmlSer.Serialize(textWriter, response);
            string temp1 = textWriter.ToString();

            String outCompany = "";
            String outGender = "";
            String outGender2 = "";
            String outNameFirst = "";
            String outNameFirst2 = "";
            String outNameLast = "";
            String outNameLast2 = "";
            String outNameMiddle = "";
            String outNameMiddle2 = "";
            String outNamePrefix = "";
            String outNamePrefix2 = "";
            String outNameSuffix = "";
            String outNameSuffix2 = "";
            String outRecordID = "";
            String outResults = "";
            String outTransmissionResults = "";
            String outVersion = response.Version;
            String outError = "";
            outTransmissionResults = response.TransmissionResults;

            if (errorMessage.Length > 0)
            {
                outError = errorMessage;
            }
            else if (outTransmissionResults.Length - 1 > 0)
            {
                outError = outTransmissionResults;
            }
            else
            {
                outCompany = response.Records[0].Company;
                outGender = response.Records[0].Gender;
                outGender2 = response.Records[0].Gender2;
                outNameFirst = response.Records[0].NameFirst;
                outNameFirst2 = response.Records[0].NameFirst2;
                outNameLast = response.Records[0].NameLast;
                outNameLast2 = response.Records[0].NameLast2;
                outNameMiddle = response.Records[0].NameMiddle;
                outNameMiddle2 = response.Records[0].NameMiddle2;
                outNamePrefix = response.Records[0].NamePrefix;
                outNamePrefix2 = response.Records[0].NamePrefix2;
                outNameSuffix = response.Records[0].NameSuffix;
                outNameSuffix2 = response.Records[0].NameSuffix2;
                outRecordID = response.Records[0].RecordID;
                outResults = response.Records[0].Results;
            }

            Version.Set(context, outVersion);
            Company.Set(context, outCompany);
            Gender.Set(context, outGender);
            Gender2.Set(context, outGender2);
            NameFirst.Set(context, outNameFirst);
            NameFirst2.Set(context, outNameFirst2);
            NameLast.Set(context, outNameLast);
            NameLast2.Set(context, outNameLast2);
            NameMiddle.Set(context, outNameMiddle);
            NameMiddle2.Set(context, outNameMiddle2);
            NamePrefix.Set(context, outNamePrefix);
            NamePrefix2.Set(context, outNamePrefix2);
            NameSuffix.Set(context, outNameSuffix);
            NameSuffix2.Set(context, outNameSuffix2);
            RecordID.Set(context, outRecordID);
            Results.Set(context, outResults);
            Error.Set(context, outError);
            TransmissionResults.Set(context, outTransmissionResults);
        }
    }
}
