﻿namespace BusinessCoderRESTSample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Location Type");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Phone");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Stock Ticker");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Web Address");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Employees Estimate");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Sales Estimate");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Plus 4");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Delivery Indicator");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Melissa Address Key");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Melissa Address Key Base");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Address Details", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("SIC Code 1");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("SIC Code 2");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("SIC Code 3");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("NAICS Code 1");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("NAICS Code 2");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("NAICS Code 3");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Business Codes", new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("SIC Description 1");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("SIC Description 2");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("SIC Description 3");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("NAICS Description 1");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("NAICS Description 2");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("NAICS Description 3");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Business Description", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Latitude");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Longitude");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Geo Code", new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("County Name");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("County FIPS");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Census Tract");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Census Block");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("Place Code");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("Place Name");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Census Details", new System.Windows.Forms.TreeNode[] {
            treeNode29,
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33,
            treeNode34});
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LicenseTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.CheckAll = new System.Windows.Forms.Button();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PhoneInput = new System.Windows.Forms.CheckBox();
            this.WebAddressInput = new System.Windows.Forms.CheckBox();
            this.StockTickerInput = new System.Windows.Forms.CheckBox();
            this.MelissaAddressKeyInput = new System.Windows.Forms.CheckBox();
            this.AddressInput = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.MelissaAddressKeyTxt = new System.Windows.Forms.TextBox();
            this.RecordIDTxt = new System.Windows.Forms.TextBox();
            this.TransmissionReferenceTxt = new System.Windows.Forms.TextBox();
            this.CountryTxt = new System.Windows.Forms.TextBox();
            this.PostalTxt = new System.Windows.Forms.TextBox();
            this.StateTxt = new System.Windows.Forms.TextBox();
            this.CityTxt = new System.Windows.Forms.TextBox();
            this.Address2Txt = new System.Windows.Forms.TextBox();
            this.Address1Txt = new System.Windows.Forms.TextBox();
            this.WebAddressTxt = new System.Windows.Forms.TextBox();
            this.StockTickerTxt = new System.Windows.Forms.TextBox();
            this.PhoneNumberTxt = new System.Windows.Forms.TextBox();
            this.CompanyNameTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ReturnDominantBusinessCheck = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.RESTRequestURLTxt = new System.Windows.Forms.TextBox();
            this.RESTRequestURL = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.URLTxt = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(502, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "License:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // LicenseTxt
            // 
            this.LicenseTxt.Location = new System.Drawing.Point(123, 12);
            this.LicenseTxt.Name = "LicenseTxt";
            this.LicenseTxt.Size = new System.Drawing.Size(313, 20);
            this.LicenseTxt.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.Location = new System.Drawing.Point(75, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "URL:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.richTextBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(847, 295);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Output";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(4, 4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(840, 288);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.CheckAll);
            this.tabPage2.Controls.Add(this.SubmitButton);
            this.tabPage2.Controls.Add(this.treeView2);
            this.tabPage2.Controls.Add(this.treeView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(847, 295);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Output Options";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // CheckAll
            // 
            this.CheckAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CheckAll.Location = new System.Drawing.Point(459, 214);
            this.CheckAll.Name = "CheckAll";
            this.CheckAll.Size = new System.Drawing.Size(147, 35);
            this.CheckAll.TabIndex = 4;
            this.CheckAll.Text = "Check All";
            this.CheckAll.UseVisualStyleBackColor = true;
            this.CheckAll.Click += new System.EventHandler(this.CheckAll_Click);
            // 
            // SubmitButton
            // 
            this.SubmitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.SubmitButton.Location = new System.Drawing.Point(660, 214);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(147, 35);
            this.SubmitButton.TabIndex = 3;
            this.SubmitButton.Text = "Submit Request";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // treeView2
            // 
            this.treeView2.CheckBoxes = true;
            this.treeView2.Location = new System.Drawing.Point(424, 6);
            this.treeView2.Name = "treeView2";
            treeNode1.Name = "LocationType";
            treeNode1.Text = "Location Type";
            treeNode2.Name = "Phone";
            treeNode2.Text = "Phone";
            treeNode3.Name = "StockTicker";
            treeNode3.Text = "Stock Ticker";
            treeNode4.Name = "WebAddress";
            treeNode4.Text = "Web Address";
            treeNode5.Name = "EmployeesEstimate";
            treeNode5.Text = "Employees Estimate";
            treeNode6.Name = "SalesEstimate";
            treeNode6.Text = "Sales Estimate";
            this.treeView2.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6});
            this.treeView2.Size = new System.Drawing.Size(415, 174);
            this.treeView2.TabIndex = 2;
            this.treeView2.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView2_AfterSelect);
            // 
            // treeView1
            // 
            this.treeView1.CheckBoxes = true;
            this.treeView1.Location = new System.Drawing.Point(3, 6);
            this.treeView1.Name = "treeView1";
            treeNode7.Name = "Plus4";
            treeNode7.Text = "Plus 4";
            treeNode8.Name = "DeliveryIndicator";
            treeNode8.Text = "Delivery Indicator";
            treeNode9.Name = "MelissaAddressKey";
            treeNode9.Text = "Melissa Address Key";
            treeNode10.Name = "MelissaAddressKeyBase";
            treeNode10.Text = "Melissa Address Key Base";
            treeNode11.Name = "GrpAddressDetails";
            treeNode11.Text = "Address Details";
            treeNode12.Name = "SICCode1";
            treeNode12.Text = "SIC Code 1";
            treeNode13.Name = "SICCode2";
            treeNode13.Text = "SIC Code 2";
            treeNode14.Name = "SICCode3";
            treeNode14.Text = "SIC Code 3";
            treeNode15.Name = "NAICSCode1";
            treeNode15.Text = "NAICS Code 1";
            treeNode16.Name = "NAICSCode2";
            treeNode16.Text = "NAICS Code 2";
            treeNode17.Name = "NAICSCode3";
            treeNode17.Text = "NAICS Code 3";
            treeNode18.Name = "GrpBusinessCodes";
            treeNode18.Text = "Business Codes";
            treeNode19.Name = "SICDescription1";
            treeNode19.Text = "SIC Description 1";
            treeNode20.Name = "SICDescription2";
            treeNode20.Text = "SIC Description 2";
            treeNode21.Name = "SICDescription3";
            treeNode21.Text = "SIC Description 3";
            treeNode22.Name = "NAICSDescription1";
            treeNode22.Text = "NAICS Description 1";
            treeNode23.Name = "NAICSDescription1";
            treeNode23.Text = "NAICS Description 2";
            treeNode24.Name = "NAICSDescription1";
            treeNode24.Text = "NAICS Description 3";
            treeNode25.Name = "GrpBusinessDescription";
            treeNode25.Text = "Business Description";
            treeNode26.Name = "Latitude";
            treeNode26.Text = "Latitude";
            treeNode27.Name = "Longitude";
            treeNode27.Text = "Longitude";
            treeNode28.Name = "GrpGeoCode";
            treeNode28.Text = "Geo Code";
            treeNode29.Name = "CountyName";
            treeNode29.Text = "County Name";
            treeNode30.Name = "CountyFIPS";
            treeNode30.Text = "County FIPS";
            treeNode31.Name = "CensusTract";
            treeNode31.Text = "Census Tract";
            treeNode32.Name = "CensusBlock";
            treeNode32.Text = "Census Block";
            treeNode33.Name = "PlaceCode";
            treeNode33.Text = "Place Code";
            treeNode34.Name = "PlaceName";
            treeNode34.Text = "Place Name";
            treeNode35.Name = "GrpCensusDetails";
            treeNode35.Text = "Census Details";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode18,
            treeNode25,
            treeNode28,
            treeNode35});
            this.treeView1.Size = new System.Drawing.Size(415, 283);
            this.treeView1.TabIndex = 1;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PhoneInput);
            this.tabPage1.Controls.Add(this.WebAddressInput);
            this.tabPage1.Controls.Add(this.StockTickerInput);
            this.tabPage1.Controls.Add(this.MelissaAddressKeyInput);
            this.tabPage1.Controls.Add(this.AddressInput);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.MelissaAddressKeyTxt);
            this.tabPage1.Controls.Add(this.RecordIDTxt);
            this.tabPage1.Controls.Add(this.TransmissionReferenceTxt);
            this.tabPage1.Controls.Add(this.CountryTxt);
            this.tabPage1.Controls.Add(this.PostalTxt);
            this.tabPage1.Controls.Add(this.StateTxt);
            this.tabPage1.Controls.Add(this.CityTxt);
            this.tabPage1.Controls.Add(this.Address2Txt);
            this.tabPage1.Controls.Add(this.Address1Txt);
            this.tabPage1.Controls.Add(this.WebAddressTxt);
            this.tabPage1.Controls.Add(this.StockTickerTxt);
            this.tabPage1.Controls.Add(this.PhoneNumberTxt);
            this.tabPage1.Controls.Add(this.CompanyNameTxt);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.ReturnDominantBusinessCheck);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(847, 295);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PhoneInput
            // 
            this.PhoneInput.AutoSize = true;
            this.PhoneInput.Location = new System.Drawing.Point(685, 252);
            this.PhoneInput.Name = "PhoneInput";
            this.PhoneInput.Size = new System.Drawing.Size(70, 22);
            this.PhoneInput.TabIndex = 38;
            this.PhoneInput.Text = "Phone";
            this.PhoneInput.UseVisualStyleBackColor = true;
            // 
            // WebAddressInput
            // 
            this.WebAddressInput.AutoSize = true;
            this.WebAddressInput.Location = new System.Drawing.Point(563, 251);
            this.WebAddressInput.Name = "WebAddressInput";
            this.WebAddressInput.Size = new System.Drawing.Size(116, 22);
            this.WebAddressInput.TabIndex = 37;
            this.WebAddressInput.Text = "Web Address";
            this.WebAddressInput.UseVisualStyleBackColor = true;
            // 
            // StockTickerInput
            // 
            this.StockTickerInput.AutoSize = true;
            this.StockTickerInput.Location = new System.Drawing.Point(446, 251);
            this.StockTickerInput.Name = "StockTickerInput";
            this.StockTickerInput.Size = new System.Drawing.Size(111, 22);
            this.StockTickerInput.TabIndex = 36;
            this.StockTickerInput.Text = "Stock Ticker";
            this.StockTickerInput.UseVisualStyleBackColor = true;
            // 
            // MelissaAddressKeyInput
            // 
            this.MelissaAddressKeyInput.AutoSize = true;
            this.MelissaAddressKeyInput.Location = new System.Drawing.Point(275, 251);
            this.MelissaAddressKeyInput.Name = "MelissaAddressKeyInput";
            this.MelissaAddressKeyInput.Size = new System.Drawing.Size(165, 22);
            this.MelissaAddressKeyInput.TabIndex = 35;
            this.MelissaAddressKeyInput.Text = "Melissa Address Key";
            this.MelissaAddressKeyInput.UseVisualStyleBackColor = true;
            // 
            // AddressInput
            // 
            this.AddressInput.AutoSize = true;
            this.AddressInput.Location = new System.Drawing.Point(188, 251);
            this.AddressInput.Name = "AddressInput";
            this.AddressInput.Size = new System.Drawing.Size(81, 22);
            this.AddressInput.TabIndex = 34;
            this.AddressInput.Text = "Address";
            this.AddressInput.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(97, 252);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 18);
            this.label17.TabIndex = 30;
            this.label17.Text = "Input Using:";
            // 
            // MelissaAddressKeyTxt
            // 
            this.MelissaAddressKeyTxt.Location = new System.Drawing.Point(595, 206);
            this.MelissaAddressKeyTxt.Name = "MelissaAddressKeyTxt";
            this.MelissaAddressKeyTxt.Size = new System.Drawing.Size(197, 24);
            this.MelissaAddressKeyTxt.TabIndex = 27;
            // 
            // RecordIDTxt
            // 
            this.RecordIDTxt.Location = new System.Drawing.Point(200, 166);
            this.RecordIDTxt.Name = "RecordIDTxt";
            this.RecordIDTxt.Size = new System.Drawing.Size(196, 24);
            this.RecordIDTxt.TabIndex = 23;
            // 
            // TransmissionReferenceTxt
            // 
            this.TransmissionReferenceTxt.Location = new System.Drawing.Point(199, 138);
            this.TransmissionReferenceTxt.Name = "TransmissionReferenceTxt";
            this.TransmissionReferenceTxt.Size = new System.Drawing.Size(197, 24);
            this.TransmissionReferenceTxt.TabIndex = 22;
            // 
            // CountryTxt
            // 
            this.CountryTxt.Location = new System.Drawing.Point(595, 175);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(197, 24);
            this.CountryTxt.TabIndex = 15;
            // 
            // PostalTxt
            // 
            this.PostalTxt.Location = new System.Drawing.Point(595, 145);
            this.PostalTxt.Name = "PostalTxt";
            this.PostalTxt.Size = new System.Drawing.Size(197, 24);
            this.PostalTxt.TabIndex = 14;
            // 
            // StateTxt
            // 
            this.StateTxt.Location = new System.Drawing.Point(595, 115);
            this.StateTxt.Name = "StateTxt";
            this.StateTxt.Size = new System.Drawing.Size(197, 24);
            this.StateTxt.TabIndex = 13;
            // 
            // CityTxt
            // 
            this.CityTxt.Location = new System.Drawing.Point(595, 85);
            this.CityTxt.Name = "CityTxt";
            this.CityTxt.Size = new System.Drawing.Size(197, 24);
            this.CityTxt.TabIndex = 12;
            // 
            // Address2Txt
            // 
            this.Address2Txt.Location = new System.Drawing.Point(595, 55);
            this.Address2Txt.Name = "Address2Txt";
            this.Address2Txt.Size = new System.Drawing.Size(197, 24);
            this.Address2Txt.TabIndex = 11;
            // 
            // Address1Txt
            // 
            this.Address1Txt.Location = new System.Drawing.Point(595, 25);
            this.Address1Txt.Name = "Address1Txt";
            this.Address1Txt.Size = new System.Drawing.Size(197, 24);
            this.Address1Txt.TabIndex = 10;
            // 
            // WebAddressTxt
            // 
            this.WebAddressTxt.Location = new System.Drawing.Point(199, 108);
            this.WebAddressTxt.Name = "WebAddressTxt";
            this.WebAddressTxt.Size = new System.Drawing.Size(197, 24);
            this.WebAddressTxt.TabIndex = 7;
            // 
            // StockTickerTxt
            // 
            this.StockTickerTxt.Location = new System.Drawing.Point(200, 80);
            this.StockTickerTxt.Name = "StockTickerTxt";
            this.StockTickerTxt.Size = new System.Drawing.Size(197, 24);
            this.StockTickerTxt.TabIndex = 5;
            // 
            // PhoneNumberTxt
            // 
            this.PhoneNumberTxt.Location = new System.Drawing.Point(199, 50);
            this.PhoneNumberTxt.Name = "PhoneNumberTxt";
            this.PhoneNumberTxt.Size = new System.Drawing.Size(197, 24);
            this.PhoneNumberTxt.TabIndex = 3;
            // 
            // CompanyNameTxt
            // 
            this.CompanyNameTxt.Location = new System.Drawing.Point(199, 20);
            this.CompanyNameTxt.Name = "CompanyNameTxt";
            this.CompanyNameTxt.Size = new System.Drawing.Size(197, 24);
            this.CompanyNameTxt.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(439, 212);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(150, 18);
            this.label16.TabIndex = 26;
            this.label16.Text = "Melissa Address Key:";
            // 
            // ReturnDominantBusinessCheck
            // 
            this.ReturnDominantBusinessCheck.AutoSize = true;
            this.ReturnDominantBusinessCheck.Location = new System.Drawing.Point(214, 206);
            this.ReturnDominantBusinessCheck.Name = "ReturnDominantBusinessCheck";
            this.ReturnDominantBusinessCheck.Size = new System.Drawing.Size(15, 14);
            this.ReturnDominantBusinessCheck.TabIndex = 25;
            this.ReturnDominantBusinessCheck.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 202);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(189, 18);
            this.label15.TabIndex = 24;
            this.label15.Text = "Return Dominant Business:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(115, 172);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 18);
            this.label14.TabIndex = 21;
            this.label14.Text = "Record ID:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label13.Location = new System.Drawing.Point(19, 144);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(174, 18);
            this.label13.TabIndex = 20;
            this.label13.Text = "Transmission Reference:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label12.Location = new System.Drawing.Point(525, 181);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "Country:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label11.Location = new System.Drawing.Point(535, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 18);
            this.label11.TabIndex = 18;
            this.label11.Text = "Postal:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label10.Location = new System.Drawing.Point(543, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "State:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label9.Location = new System.Drawing.Point(552, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 18);
            this.label9.TabIndex = 16;
            this.label9.Text = "City:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(511, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Address 2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(511, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Address 1:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(92, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "Web Address:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label5.Location = new System.Drawing.Point(97, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Stock Ticker:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label4.Location = new System.Drawing.Point(81, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Phone Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label3.Location = new System.Drawing.Point(73, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Company Name:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tabControl1.Location = new System.Drawing.Point(12, 68);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(855, 323);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.Tag = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.RESTRequestURLTxt);
            this.tabPage4.Controls.Add(this.RESTRequestURL);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(847, 295);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "REST Request";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // RESTRequestURLTxt
            // 
            this.RESTRequestURLTxt.Location = new System.Drawing.Point(157, 17);
            this.RESTRequestURLTxt.Name = "RESTRequestURLTxt";
            this.RESTRequestURLTxt.Size = new System.Drawing.Size(684, 21);
            this.RESTRequestURLTxt.TabIndex = 1;
            // 
            // RESTRequestURL
            // 
            this.RESTRequestURL.AutoSize = true;
            this.RESTRequestURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RESTRequestURL.Location = new System.Drawing.Point(6, 17);
            this.RESTRequestURL.Name = "RESTRequestURL";
            this.RESTRequestURL.Size = new System.Drawing.Size(145, 18);
            this.RESTRequestURL.TabIndex = 0;
            this.RESTRequestURL.Text = "REST Request URL:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.richTextBox2);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(847, 295);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "JSON Raw Output";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(3, 3);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(841, 289);
            this.richTextBox2.TabIndex = 0;
            this.richTextBox2.Text = "";
            // 
            // URLTxt
            // 
            this.URLTxt.FormattingEnabled = true;
            this.URLTxt.Items.AddRange(new object[] {
            "http://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoder",
            "https://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoder",
            "http://etstage1.melissadata.com:30070/WEB/BusinessCoder/doBusinessCoder"});
            this.URLTxt.Location = new System.Drawing.Point(123, 40);
            this.URLTxt.Name = "URLTxt";
            this.URLTxt.Size = new System.Drawing.Size(313, 21);
            this.URLTxt.TabIndex = 6;
            this.URLTxt.SelectedIndexChanged += new System.EventHandler(this.URLTxt_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 403);
            this.Controls.Add(this.URLTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LicenseTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LicenseTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox PhoneInput;
        private System.Windows.Forms.CheckBox WebAddressInput;
        private System.Windows.Forms.CheckBox StockTickerInput;
        private System.Windows.Forms.CheckBox MelissaAddressKeyInput;
        private System.Windows.Forms.CheckBox AddressInput;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox MelissaAddressKeyTxt;
        private System.Windows.Forms.TextBox RecordIDTxt;
        private System.Windows.Forms.TextBox TransmissionReferenceTxt;
        private System.Windows.Forms.TextBox CountryTxt;
        private System.Windows.Forms.TextBox PostalTxt;
        private System.Windows.Forms.TextBox StateTxt;
        private System.Windows.Forms.TextBox CityTxt;
        private System.Windows.Forms.TextBox Address2Txt;
        private System.Windows.Forms.TextBox Address1Txt;
        private System.Windows.Forms.TextBox WebAddressTxt;
        private System.Windows.Forms.TextBox StockTickerTxt;
        private System.Windows.Forms.TextBox PhoneNumberTxt;
        private System.Windows.Forms.TextBox CompanyNameTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox ReturnDominantBusinessCheck;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox RESTRequestURLTxt;
        private System.Windows.Forms.Label RESTRequestURL;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button CheckAll;
        private System.Windows.Forms.ComboBox URLTxt;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}

