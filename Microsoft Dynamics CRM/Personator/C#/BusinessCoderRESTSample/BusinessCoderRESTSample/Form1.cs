﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;
using mdObjectsClient;
using System.IO;

namespace BusinessCoderRESTSample
{
    public partial class Form1 : Form
    {
        HttpWebRequest request;
        HttpWebResponse response;
        System.IO.Stream responseStream;
        System.IO.MemoryStream JSONStream;
        Uri address;
        String tServer;
        StreamReader streamReader;

        private String spaceRemover(String x)
        {
            String y = "";
            for(int i = 0; i < x.Length; i++)
           { 
                if(x.ElementAt(i) == ' ')
                {
                    y += "+";
                }
                else
                {
                    y += x.ElementAt(i);
                }
            }
            return y;
        }

        public Form1()
        {
            InitializeComponent();
            URLTxt.Text = "http://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoderUS";
            TransmissionReferenceTxt.Text = "Business Coder Tester";
            Address1Txt.Text = "22382 Avenida Empresa";
            CityTxt.Text = "Rancho Santa Margarita";
            StateTxt.Text = "CA";
            PostalTxt.Text = "92688";
            AddressInput.Checked = true;
            foreach (TreeNode n in treeView2.Nodes)
            {
                    n.Checked = true;
            }
            ReturnDominantBusinessCheck.Checked = true;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            tServer = URLTxt.Text;
            String auth;
            auth = Uri.EscapeDataString(LicenseTxt.Text);
            tServer += "?id=" + auth; //this sets the license string



            //This portion of the code checks the output column options
            tServer += "&cols=";
            foreach (TreeNode n in treeView2.Nodes)
            {
                if (n.Checked)
                {
                    tServer += n.Name;
                    tServer += ",";
                }
            }
            foreach (TreeNode n in treeView1.Nodes)
            {
                if (n.Checked)
                {
                    tServer += n.Name;
                    tServer += ",";
                }
            }
            //this portion of the code removes an extra , if there is one
            if (tServer.ElementAt(tServer.Length - 1) == ',')
            {
                tServer = tServer.Substring(0, (tServer.Length - 1));
            }

            //This portion of the code handles the options
            if (ReturnDominantBusinessCheck.Checked)
            {
                tServer += "&opt=ReturnDominantBusiness:yes";
            }
            else
            {
                tServer += "&opt=ReturnDominantBusiness:no";
            }

            //This is the Transmission Reference
            tServer += "&t=" + TransmissionReferenceTxt.Text;

            //This portion of the code handles the company name

            if(!object.Equals(CompanyNameTxt.Text, ""))
            {
                tServer += "&comp=" + spaceRemover(CompanyNameTxt.Text);
            }

            //this portion of the code checks which input type it is and adds to the URL accordingly

            //this portion of the code handles the address input
            if (AddressInput.Checked)
            {

                tServer += "&a1=" + spaceRemover(Address1Txt.Text);
                tServer += "&a2=" + spaceRemover(Address2Txt.Text);
                tServer += "&city" + spaceRemover(CityTxt.Text);
                tServer += "&state=" + spaceRemover(StateTxt.Text);
                tServer += "&postal=" + spaceRemover(PostalTxt.Text);
                tServer += "&ctry=" + spaceRemover(CountryTxt.Text);
            }
            else if (MelissaAddressKeyInput.Checked)
            {
                tServer += "&mak=" + MelissaAddressKeyTxt.Text;
            }
            else if (PhoneInput.Checked)
            {
                tServer += "&phone=" + PhoneNumberTxt.Text;
            }
            else if (StockTickerInput.Checked)
            {
                tServer += "&stock=" + StockTickerTxt.Text;
            }
            else if (WebAddressInput.Checked)
            {
                tServer += "&web=" + WebAddressTxt.Text;
            }

            //this portion of the code handles the Record ID
            if (!object.Equals(RecordIDTxt.Text, ""))
            {
                tServer += "&rec=" + RecordIDTxt.Text;
            }

        
            RESTRequestURLTxt.Text = tServer;
            address = new Uri(tServer);

            Debug.WriteLine(tServer);
            // Create the web request
            request = (HttpWebRequest)WebRequest.Create(address);

            // Get response   
            response = (HttpWebResponse)request.GetResponse();
            

            // Get the response stream into a reader
            responseStream = response.GetResponseStream();

            JSONStream = new System.IO.MemoryStream();
            responseStream.CopyTo(JSONStream);
            streamReader = new StreamReader(JSONStream);
            JsonHelper Jhelp = new JsonHelper();
            JSONStream.Position = 0;
            richTextBox2.Text = Jhelp.FormatJson(streamReader.ReadToEnd());

            JSONStream.Position = 0;
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(BusinessCoderResponse));
            BusinessCoderResponse businessCoderData = (BusinessCoderResponse)serializer.ReadObject(JSONStream);

            


            String output = "";
            output += "Transmission Results: " + businessCoderData.TransmissionResults + "\n";
            output += "Transmission Reference: " + businessCoderData.TransmissionReference + "\n";
            output += "Version: " + businessCoderData.Version + "\n";


            

            //This portion of the code fills the output based on the columns that were chosen
            for (int i = 0; i < businessCoderData.Records.Length; i++)
            {
                

                output += "\n";
                output += "Record ID: " + businessCoderData.Records[i].RecordID + "\n";
                output += "Results: " + businessCoderData.Records[i].Results + "\n";
                
                output += "Address Line 1: " + businessCoderData.Records[i].AddressLine1 + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[3].Checked) //Census Block
                    output += "Census Block: " + businessCoderData.Records[i].CensusBlock + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[2].Checked) //Census Tract
                    output += "Census Tract: " + businessCoderData.Records[i].CensusTract + "\n";
                output += "City: " + businessCoderData.Records[i].City + "\n";
                output += "Company Name: " + businessCoderData.Records[i].CompanyName + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[1].Checked) //County FIPS
                    output += "County FIPS: " + businessCoderData.Records[i].CountyFIPS + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[0].Checked) //County Name
                    output += "County Name: " + businessCoderData.Records[i].CountyName + "\n";
                if (treeView1.Nodes[0].Checked || treeView1.Nodes[0].Nodes[1].Checked) //Delivery Indicator
                    output += "Delivery Indicator: " + businessCoderData.Records[i].DeliveryIndicator + "\n";
                if (treeView2.Nodes[4].Checked)//Employee Estimate
                    output += "Employees Estimate: " + businessCoderData.Records[i].EmployeesEstimate + "\n";
                if (treeView1.Nodes[3].Checked || treeView1.Nodes[3].Nodes[0].Checked) //Latitude
                    output += "Latitude: " + businessCoderData.Records[i].Latitude + "\n";
                if(treeView2.Nodes[0].Checked) //Location Type
                    output += "Location Type: " + businessCoderData.Records[i].LocationType + "\n";
                if (treeView1.Nodes[3].Checked || treeView1.Nodes[3].Nodes[1].Checked) //Longitude
                    output += "Longitude: " + businessCoderData.Records[i].Longitude + "\n";
                if (treeView1.Nodes[0].Checked || treeView1.Nodes[0].Nodes[2].Checked) //Melissa Address Key
                    output += "Melissa Address Key: " + businessCoderData.Records[i].MelissaAddressKey + "\n";
                if (treeView1.Nodes[0].Checked || treeView1.Nodes[0].Nodes[3].Checked) //Melissa Address Key Base
                    output += "Melissa Address Key Base: " + businessCoderData.Records[i].MelissaAddressKeyBase + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[3].Checked) //NAICS Code 1
                    output += "NAICS Code 1: " + businessCoderData.Records[i].NAICSCode1 + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[4].Checked) //NAICS Code 2
                    output += "NAICS Code 2: " + businessCoderData.Records[i].NAICSCode2 + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[5].Checked) //NAICS Code 3
                    output += "NAICS Code 3: " + businessCoderData.Records[i].NAICSCode3 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[3].Checked) //NAICS Description 1
                    output += "NAICS Description 1: " + businessCoderData.Records[i].NAICSDescription1 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[4].Checked) //NAICS Description 2
                    output += "NAICS Description 2: " + businessCoderData.Records[i].NAICSDescription2 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[5].Checked) //NAICS Description 3
                    output += "NAICS Description 3: " + businessCoderData.Records[i].NAICSDescription3 + "\n";
                if (treeView2.Nodes[1].Checked) //Phone
                    output += "Phone: " + businessCoderData.Records[i].Phone + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[4].Checked) //Place Code
                    output += "Place Code: " + businessCoderData.Records[i].PlaceCode + "\n";
                if (treeView1.Nodes[4].Checked || treeView1.Nodes[4].Nodes[5].Checked) //Place Name
                    output += "Place Name: " + businessCoderData.Records[i].PlaceName + "\n";
                if(treeView1.Nodes[0].Checked || treeView1.Nodes[0].Nodes[0].Checked) //Plus 4
                    output += "Plus 4: " + businessCoderData.Records[i].Plus4 + "\n";
                output += "Postal Code: " + businessCoderData.Records[i].PostalCode + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[0].Checked) //SIC Code 1
                    output += "SIC Code 1: " + businessCoderData.Records[i].SICCode1 + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[1].Checked) //SIC Code 2
                    output += "SIC Code 2: " + businessCoderData.Records[i].SICCode2 + "\n";
                if (treeView1.Nodes[1].Checked || treeView1.Nodes[1].Nodes[2].Checked) //SIC Code 3
                    output += "SIC Code 3: " + businessCoderData.Records[i].SICCode3 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[0].Checked) //SIC Description 1
                    output += "SIC Description 1: " + businessCoderData.Records[i].SICDescription1 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[1].Checked) //SIC Description 2
                    output += "SIC Description 2: " + businessCoderData.Records[i].SICDescription2 + "\n";
                if (treeView1.Nodes[2].Checked || treeView1.Nodes[2].Nodes[2].Checked) //SIC Description 3
                    output += "SIC Description 3: " + businessCoderData.Records[i].SICDescription3 + "\n";
                if (treeView2.Nodes[5].Checked) //Sales Estimate
                    output += "Sales Estimate: " + businessCoderData.Records[i].SalesEstimate + "\n";
                output += "State: " + businessCoderData.Records[i].State + "\n";
                if (treeView2.Nodes[2].Checked) //Stock Ticker
                    output += "Stock Ticker: " + businessCoderData.Records[i].StockTicker + "\n";
                output += "Suite: " + businessCoderData.Records[i].Suite + "\n";
                if (treeView2.Nodes[3].Checked) //Web Address
                    output += "Web Address: " + businessCoderData.Records[i].WebAddress + "\n";
            }
            
            richTextBox1.Text = output;

        }


       


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CheckAll_Click(object sender, EventArgs e)
        {
            foreach (TreeNode n in treeView2.Nodes)
            {
                n.Checked = true;
            }
            foreach (TreeNode n in treeView1.Nodes)
            {
                n.Checked = true;
            }

        }

        private void URLTxt_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
