﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel.Web;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using mdObjectsClient;
using Newtonsoft.Json;
using System.IO;

namespace BusinessCoderJSONSampleCSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            URLTxt.Text = "http://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoderUS";
            TransmissionReferenceTxt.Text = "Business Coder Tester";
            Address1Txt.Text = "22382 Avenida Empresa";
            CityTxt.Text = "Rancho Santa Margarita";
            StateTxt.Text = "CA";
            PostalTxt.Text = "92688";
            AddressInput.Checked = true;
            foreach (TreeNode n in treeView1.Nodes)
            {
                n.Checked = true;
            }
            ReturnDominantBusinessCheck.Checked = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void CheckAllButton_Click(object sender, EventArgs e)
        {
            foreach (TreeNode n in treeView2.Nodes)
            {
                n.Checked = true;
            }
            foreach (TreeNode n in treeView1.Nodes)
            {
                n.Checked = true;
            }
        }

        private void SubmitRequestButton_Click(object sender, EventArgs e)
        {
            JSONRequestTxt.Clear();
            JSONResponseTxt.Clear();

            String auth;
            auth = Uri.EscapeDataString(LicenseTxt.Text);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URLTxt.Text);
            request.ContentType = "application/json";
            request.Method = "POST";
            string requestjson = "";
            string INDENT = "     ";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                //Transmission Reference
                requestjson += "{\n " + INDENT + "\"t\":\"" + TransmissionReferenceTxt.Text + "\", \n";

                //license string
                requestjson += INDENT + "\"id\":\"" + LicenseTxt.Text + "\",\n";

                //columns
                string colsString = "";
                foreach (TreeNode n in treeView1.Nodes)
                {
                    if (n.Checked)
                    {
                        colsString += n.Name;
                        colsString += ",";
                    }
                }
                foreach (TreeNode n in treeView2.Nodes)
                {
                    if (n.Checked)
                    {
                        colsString += n.Name;
                        colsString += ",";
                    }
                }
                requestjson += INDENT + "\"cols\":\"" + colsString;
                if (requestjson.ElementAt(requestjson.Length - 1) == ',')
                {
                    requestjson = requestjson.Substring(0, (requestjson.Length - 1));
                }
                requestjson += "\",\n";

                //options
                requestjson += INDENT + "\"opt\":\"";
                string optionString = "";
                if (ReturnDominantBusinessCheck.Checked)
                {
                    optionString = "ReturnDominantBusiness:yes";
                }
                else
                {
                    optionString = "ReturnDominantBusiness:no";
                }
                if (ReturnDominantBusinessCheck.Checked)
                {
                    requestjson += "ReturnDominantBusiness:yes";
                }
                else
                {
                    requestjson += "ReturnDominantBusiness:no";
                }
                requestjson += "\",\n";
                
                //process the record
                requestjson += INDENT + "\"Records\":[\n";
                requestjson += INDENT + INDENT + "{\n";
                //record id
                requestjson += INDENT + INDENT + "\"rec\":\"" + RecordIDTxt.Text + "\",\n";
                //company
                requestjson += INDENT + INDENT + "\"comp\":\"" + CompanyNameTxt.Text + "\",\n";
                //address
                if (AddressInput.Checked)
                {
                    requestjson += INDENT + INDENT + "\"a1\":\"" + Address1Txt.Text + "\",\n";
                    requestjson += INDENT + INDENT + "\"a2\":\"" + Address2Txt.Text + "\",\n";
                    requestjson += INDENT + INDENT + "\"city\":\"" + CityTxt.Text + "\",\n";
                    requestjson += INDENT + INDENT + "\"state\":\"" + StateTxt.Text + "\",\n";
                    requestjson += INDENT + INDENT + "\"postal\":\"" + PostalTxt.Text + "\",\n";
                    requestjson += INDENT + INDENT + "\"ctry\":\"" + CountryTxt.Text + "\",\n";
                }
                else
                {
                    requestjson += INDENT + INDENT + "\"a1\":\"\",\n";
                    requestjson += INDENT + INDENT + "\"a2\":\"\",\n";
                    requestjson += INDENT + INDENT + "\"city\":\"\",\n";
                    requestjson += INDENT + INDENT + "\"state\":\"\",\n";
                    requestjson += INDENT + INDENT + "\"postal\":\"\",\n";
                    requestjson += INDENT + INDENT + "\"ctry\":\"\",\n";
                }
                //phone
                if (PhoneInput.Checked)
                {
                    requestjson += INDENT + INDENT + "\"phone\":\"" + PhoneTxt.Text + "\",\n";
                }
                else
                {
                    requestjson += INDENT + INDENT + "\"phone\":\"\",\n";
                }
                //mkey
                if (MelissaAddressKeyInput.Checked)
                {
                    requestjson += INDENT + INDENT + "\"mak\":\"" + MelissaAddressKeyTxt.Text + "\",\n";
                }
                else
                {
                    requestjson += INDENT + INDENT + "\"mak\":\"\",\n";
                }
                //stock
                if (StockTickerInput.Checked)
                {
                    requestjson += INDENT + INDENT + "\"stock\":\"" + StockTickerTxt.Text + "\",\n";
                }
                else
                {
                    requestjson += INDENT + INDENT + "\"stock\":\"\",\n";
                }
                //web
                if (WebAddressInput.Checked)
                {
                    requestjson += INDENT + INDENT + "\"web\":\"" + WebAddressTxt.Text + "\",\n";
                }
                else
                {
                    requestjson += INDENT + INDENT + "\"web\":\"\",\n";
                }

                requestjson += INDENT + INDENT + "}\n";
                requestjson += INDENT + "]\n";
                requestjson += "}\n";

                RequestRecord sending = new RequestRecord();
                sending.rec = RecordIDTxt.Text;
                sending.comp = CompanyNameTxt.Text;
                sending.a1 = Address1Txt.Text;
                sending.a2 = Address2Txt.Text;
                sending.city = CityTxt.Text;
                sending.state = StateTxt.Text;
                sending.postal = PostalTxt.Text;
                sending.ctry = CountryTxt.Text;
                sending.phone = PhoneTxt.Text;
                sending.mak = MelissaAddressKeyTxt.Text;
                sending.stock = StockTickerTxt.Text;
                sending.web = WebAddressTxt.Text;

                RequestSend sendingRequest = new RequestSend();
                sendingRequest.t = TransmissionReferenceTxt.Text;
                sendingRequest.id = LicenseTxt.Text;
                sendingRequest.cols = colsString;
                sendingRequest.opt = optionString;
                sendingRequest.Records = new RequestRecord[1];
                sendingRequest.Records[0] = sending;

                string jsonSerialized = new JavaScriptSerializer().Serialize(sendingRequest);


                streamWriter.Write(jsonSerialized);
            }
            JSONRequestTxt.Text = requestjson;
            Debug.WriteLine(requestjson);
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                JSONResponseTxt.Text = result;
            }

        }
    }
}
