﻿namespace BusinessCoderJSONSampleCSharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Plus4");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Delivery Indicator");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("MelissaAddressKey");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("MelissaAddressKeyBase");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Address Details", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("SIC Code 1");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("SIC Code 2");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("SIC Code 3");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("NAICS Code 1");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("NAICS Code 2");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("NAICS Code 3");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Business Codes", new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("SICDescription1");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("SIC Description 2");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("SIC Description 3");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("NAICS Description 1");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("NAICS Description 2");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("NAICS Description 3");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Business Description", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Latitude");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Longitude");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Geo Code", new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21});
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("County Name");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("County FIPS");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Census Tract");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Census Block");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Place Code");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Place Name");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("Census Details", new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24,
            treeNode25,
            treeNode26,
            treeNode27,
            treeNode28});
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("Location Type");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Phone");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Stock Ticker");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("Web Address");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("Employees Estimate");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Sales Estimate");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.License = new System.Windows.Forms.Label();
            this.LicenseTxt = new System.Windows.Forms.TextBox();
            this.Url = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.PhoneInput = new System.Windows.Forms.CheckBox();
            this.WebAddressInput = new System.Windows.Forms.CheckBox();
            this.StockTickerInput = new System.Windows.Forms.CheckBox();
            this.MelissaAddressKeyInput = new System.Windows.Forms.CheckBox();
            this.AddressInput = new System.Windows.Forms.CheckBox();
            this.Input = new System.Windows.Forms.Label();
            this.Country = new System.Windows.Forms.Label();
            this.Postal = new System.Windows.Forms.Label();
            this.State = new System.Windows.Forms.Label();
            this.City = new System.Windows.Forms.Label();
            this.Address2 = new System.Windows.Forms.Label();
            this.Address1 = new System.Windows.Forms.Label();
            this.Address1Txt = new System.Windows.Forms.TextBox();
            this.Address2Txt = new System.Windows.Forms.TextBox();
            this.CityTxt = new System.Windows.Forms.TextBox();
            this.StateTxt = new System.Windows.Forms.TextBox();
            this.PostalTxt = new System.Windows.Forms.TextBox();
            this.CountryTxt = new System.Windows.Forms.TextBox();
            this.MelissaAddressKeyTxt = new System.Windows.Forms.TextBox();
            this.MelissaAddressKey = new System.Windows.Forms.Label();
            this.ReturnDominantBusinessCheck = new System.Windows.Forms.CheckBox();
            this.ReturnDominantBusiness = new System.Windows.Forms.Label();
            this.RecordIDTxt = new System.Windows.Forms.TextBox();
            this.RecordID = new System.Windows.Forms.Label();
            this.TransmissionReferenceTxt = new System.Windows.Forms.TextBox();
            this.TransmissionReference = new System.Windows.Forms.Label();
            this.WebAddressTxt = new System.Windows.Forms.TextBox();
            this.WebAddress = new System.Windows.Forms.Label();
            this.StockTickerTxt = new System.Windows.Forms.TextBox();
            this.StockTicker = new System.Windows.Forms.Label();
            this.PhoneTxt = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.CompanyNameTxt = new System.Windows.Forms.TextBox();
            this.CompanyName = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SubmitRequestButton = new System.Windows.Forms.Button();
            this.CheckAllButton = new System.Windows.Forms.Button();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.JSONRequestTxt = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.JSONResponseTxt = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.URLTxt = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // License
            // 
            this.License.AutoSize = true;
            this.License.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.License.Location = new System.Drawing.Point(13, 12);
            this.License.Name = "License";
            this.License.Size = new System.Drawing.Size(63, 18);
            this.License.TabIndex = 0;
            this.License.Text = "License:";
            // 
            // LicenseTxt
            // 
            this.LicenseTxt.Location = new System.Drawing.Point(82, 12);
            this.LicenseTxt.Name = "LicenseTxt";
            this.LicenseTxt.Size = new System.Drawing.Size(539, 20);
            this.LicenseTxt.TabIndex = 1;
            // 
            // Url
            // 
            this.Url.AutoSize = true;
            this.Url.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Url.Location = new System.Drawing.Point(13, 42);
            this.Url.Name = "Url";
            this.Url.Size = new System.Drawing.Size(42, 18);
            this.Url.TabIndex = 2;
            this.Url.Text = "URL:";
            this.Url.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 71);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(830, 326);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PhoneInput);
            this.tabPage1.Controls.Add(this.WebAddressInput);
            this.tabPage1.Controls.Add(this.StockTickerInput);
            this.tabPage1.Controls.Add(this.MelissaAddressKeyInput);
            this.tabPage1.Controls.Add(this.AddressInput);
            this.tabPage1.Controls.Add(this.Input);
            this.tabPage1.Controls.Add(this.Country);
            this.tabPage1.Controls.Add(this.Postal);
            this.tabPage1.Controls.Add(this.State);
            this.tabPage1.Controls.Add(this.City);
            this.tabPage1.Controls.Add(this.Address2);
            this.tabPage1.Controls.Add(this.Address1);
            this.tabPage1.Controls.Add(this.Address1Txt);
            this.tabPage1.Controls.Add(this.Address2Txt);
            this.tabPage1.Controls.Add(this.CityTxt);
            this.tabPage1.Controls.Add(this.StateTxt);
            this.tabPage1.Controls.Add(this.PostalTxt);
            this.tabPage1.Controls.Add(this.CountryTxt);
            this.tabPage1.Controls.Add(this.MelissaAddressKeyTxt);
            this.tabPage1.Controls.Add(this.MelissaAddressKey);
            this.tabPage1.Controls.Add(this.ReturnDominantBusinessCheck);
            this.tabPage1.Controls.Add(this.ReturnDominantBusiness);
            this.tabPage1.Controls.Add(this.RecordIDTxt);
            this.tabPage1.Controls.Add(this.RecordID);
            this.tabPage1.Controls.Add(this.TransmissionReferenceTxt);
            this.tabPage1.Controls.Add(this.TransmissionReference);
            this.tabPage1.Controls.Add(this.WebAddressTxt);
            this.tabPage1.Controls.Add(this.WebAddress);
            this.tabPage1.Controls.Add(this.StockTickerTxt);
            this.tabPage1.Controls.Add(this.StockTicker);
            this.tabPage1.Controls.Add(this.PhoneTxt);
            this.tabPage1.Controls.Add(this.Phone);
            this.tabPage1.Controls.Add(this.CompanyNameTxt);
            this.tabPage1.Controls.Add(this.CompanyName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(822, 300);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // PhoneInput
            // 
            this.PhoneInput.AutoSize = true;
            this.PhoneInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.PhoneInput.Location = new System.Drawing.Point(651, 259);
            this.PhoneInput.Name = "PhoneInput";
            this.PhoneInput.Size = new System.Drawing.Size(70, 22);
            this.PhoneInput.TabIndex = 34;
            this.PhoneInput.Text = "Phone";
            this.PhoneInput.UseVisualStyleBackColor = true;
            // 
            // WebAddressInput
            // 
            this.WebAddressInput.AutoSize = true;
            this.WebAddressInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.WebAddressInput.Location = new System.Drawing.Point(529, 259);
            this.WebAddressInput.Name = "WebAddressInput";
            this.WebAddressInput.Size = new System.Drawing.Size(116, 22);
            this.WebAddressInput.TabIndex = 33;
            this.WebAddressInput.Text = "Web Address";
            this.WebAddressInput.UseVisualStyleBackColor = true;
            // 
            // StockTickerInput
            // 
            this.StockTickerInput.AutoSize = true;
            this.StockTickerInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.StockTickerInput.Location = new System.Drawing.Point(412, 262);
            this.StockTickerInput.Name = "StockTickerInput";
            this.StockTickerInput.Size = new System.Drawing.Size(111, 22);
            this.StockTickerInput.TabIndex = 32;
            this.StockTickerInput.Text = "Stock Ticker";
            this.StockTickerInput.UseVisualStyleBackColor = true;
            // 
            // MelissaAddressKeyInput
            // 
            this.MelissaAddressKeyInput.AutoSize = true;
            this.MelissaAddressKeyInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.MelissaAddressKeyInput.Location = new System.Drawing.Point(241, 262);
            this.MelissaAddressKeyInput.Name = "MelissaAddressKeyInput";
            this.MelissaAddressKeyInput.Size = new System.Drawing.Size(165, 22);
            this.MelissaAddressKeyInput.TabIndex = 31;
            this.MelissaAddressKeyInput.Text = "Melissa Address Key";
            this.MelissaAddressKeyInput.UseVisualStyleBackColor = true;
            // 
            // AddressInput
            // 
            this.AddressInput.AutoSize = true;
            this.AddressInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.AddressInput.Location = new System.Drawing.Point(154, 262);
            this.AddressInput.Name = "AddressInput";
            this.AddressInput.Size = new System.Drawing.Size(81, 22);
            this.AddressInput.TabIndex = 30;
            this.AddressInput.Text = "Address";
            this.AddressInput.UseVisualStyleBackColor = true;
            // 
            // Input
            // 
            this.Input.AutoSize = true;
            this.Input.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Input.Location = new System.Drawing.Point(63, 263);
            this.Input.Name = "Input";
            this.Input.Size = new System.Drawing.Size(85, 18);
            this.Input.TabIndex = 28;
            this.Input.Text = "Input Using:";
            // 
            // Country
            // 
            this.Country.AutoSize = true;
            this.Country.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Country.Location = new System.Drawing.Point(541, 181);
            this.Country.Name = "Country";
            this.Country.Size = new System.Drawing.Size(64, 18);
            this.Country.TabIndex = 27;
            this.Country.Text = "Country:";
            // 
            // Postal
            // 
            this.Postal.AutoSize = true;
            this.Postal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Postal.Location = new System.Drawing.Point(551, 151);
            this.Postal.Name = "Postal";
            this.Postal.Size = new System.Drawing.Size(54, 18);
            this.Postal.TabIndex = 26;
            this.Postal.Text = "Postal:";
            // 
            // State
            // 
            this.State.AutoSize = true;
            this.State.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.State.Location = new System.Drawing.Point(559, 121);
            this.State.Name = "State";
            this.State.Size = new System.Drawing.Size(46, 18);
            this.State.TabIndex = 25;
            this.State.Text = "State:";
            // 
            // City
            // 
            this.City.AutoSize = true;
            this.City.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.City.Location = new System.Drawing.Point(568, 91);
            this.City.Name = "City";
            this.City.Size = new System.Drawing.Size(37, 18);
            this.City.TabIndex = 24;
            this.City.Text = "City:";
            // 
            // Address2
            // 
            this.Address2.AutoSize = true;
            this.Address2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Address2.Location = new System.Drawing.Point(527, 61);
            this.Address2.Name = "Address2";
            this.Address2.Size = new System.Drawing.Size(78, 18);
            this.Address2.TabIndex = 23;
            this.Address2.Text = "Address 2:";
            // 
            // Address1
            // 
            this.Address1.AutoSize = true;
            this.Address1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Address1.Location = new System.Drawing.Point(527, 31);
            this.Address1.Name = "Address1";
            this.Address1.Size = new System.Drawing.Size(78, 18);
            this.Address1.TabIndex = 22;
            this.Address1.Text = "Address 1:";
            // 
            // Address1Txt
            // 
            this.Address1Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Address1Txt.Location = new System.Drawing.Point(611, 28);
            this.Address1Txt.Name = "Address1Txt";
            this.Address1Txt.Size = new System.Drawing.Size(200, 24);
            this.Address1Txt.TabIndex = 21;
            // 
            // Address2Txt
            // 
            this.Address2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Address2Txt.Location = new System.Drawing.Point(611, 58);
            this.Address2Txt.Name = "Address2Txt";
            this.Address2Txt.Size = new System.Drawing.Size(200, 24);
            this.Address2Txt.TabIndex = 20;
            // 
            // CityTxt
            // 
            this.CityTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CityTxt.Location = new System.Drawing.Point(611, 88);
            this.CityTxt.Name = "CityTxt";
            this.CityTxt.Size = new System.Drawing.Size(200, 24);
            this.CityTxt.TabIndex = 19;
            // 
            // StateTxt
            // 
            this.StateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.StateTxt.Location = new System.Drawing.Point(611, 118);
            this.StateTxt.Name = "StateTxt";
            this.StateTxt.Size = new System.Drawing.Size(200, 24);
            this.StateTxt.TabIndex = 18;
            // 
            // PostalTxt
            // 
            this.PostalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.PostalTxt.Location = new System.Drawing.Point(611, 148);
            this.PostalTxt.Name = "PostalTxt";
            this.PostalTxt.Size = new System.Drawing.Size(200, 24);
            this.PostalTxt.TabIndex = 17;
            // 
            // CountryTxt
            // 
            this.CountryTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CountryTxt.Location = new System.Drawing.Point(611, 178);
            this.CountryTxt.Name = "CountryTxt";
            this.CountryTxt.Size = new System.Drawing.Size(200, 24);
            this.CountryTxt.TabIndex = 16;
            // 
            // MelissaAddressKeyTxt
            // 
            this.MelissaAddressKeyTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.MelissaAddressKeyTxt.Location = new System.Drawing.Point(611, 208);
            this.MelissaAddressKeyTxt.Name = "MelissaAddressKeyTxt";
            this.MelissaAddressKeyTxt.Size = new System.Drawing.Size(200, 24);
            this.MelissaAddressKeyTxt.TabIndex = 15;
            // 
            // MelissaAddressKey
            // 
            this.MelissaAddressKey.AutoSize = true;
            this.MelissaAddressKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.MelissaAddressKey.Location = new System.Drawing.Point(455, 211);
            this.MelissaAddressKey.Name = "MelissaAddressKey";
            this.MelissaAddressKey.Size = new System.Drawing.Size(150, 18);
            this.MelissaAddressKey.TabIndex = 14;
            this.MelissaAddressKey.Text = "Melissa Address Key:";
            // 
            // ReturnDominantBusinessCheck
            // 
            this.ReturnDominantBusinessCheck.AutoSize = true;
            this.ReturnDominantBusinessCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ReturnDominantBusinessCheck.Location = new System.Drawing.Point(201, 214);
            this.ReturnDominantBusinessCheck.Name = "ReturnDominantBusinessCheck";
            this.ReturnDominantBusinessCheck.Size = new System.Drawing.Size(15, 14);
            this.ReturnDominantBusinessCheck.TabIndex = 13;
            this.ReturnDominantBusinessCheck.UseVisualStyleBackColor = true;
            // 
            // ReturnDominantBusiness
            // 
            this.ReturnDominantBusiness.AutoSize = true;
            this.ReturnDominantBusiness.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ReturnDominantBusiness.Location = new System.Drawing.Point(6, 211);
            this.ReturnDominantBusiness.Name = "ReturnDominantBusiness";
            this.ReturnDominantBusiness.Size = new System.Drawing.Size(189, 18);
            this.ReturnDominantBusiness.TabIndex = 12;
            this.ReturnDominantBusiness.Text = "Return Dominant Business:";
            // 
            // RecordIDTxt
            // 
            this.RecordIDTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RecordIDTxt.Location = new System.Drawing.Point(201, 178);
            this.RecordIDTxt.Name = "RecordIDTxt";
            this.RecordIDTxt.Size = new System.Drawing.Size(200, 24);
            this.RecordIDTxt.TabIndex = 11;
            // 
            // RecordID
            // 
            this.RecordID.AutoSize = true;
            this.RecordID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RecordID.Location = new System.Drawing.Point(120, 181);
            this.RecordID.Name = "RecordID";
            this.RecordID.Size = new System.Drawing.Size(75, 18);
            this.RecordID.TabIndex = 10;
            this.RecordID.Text = "RecordID:";
            // 
            // TransmissionReferenceTxt
            // 
            this.TransmissionReferenceTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.TransmissionReferenceTxt.Location = new System.Drawing.Point(201, 148);
            this.TransmissionReferenceTxt.Name = "TransmissionReferenceTxt";
            this.TransmissionReferenceTxt.Size = new System.Drawing.Size(201, 24);
            this.TransmissionReferenceTxt.TabIndex = 9;
            // 
            // TransmissionReference
            // 
            this.TransmissionReference.AutoSize = true;
            this.TransmissionReference.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.TransmissionReference.Location = new System.Drawing.Point(21, 151);
            this.TransmissionReference.Name = "TransmissionReference";
            this.TransmissionReference.Size = new System.Drawing.Size(174, 18);
            this.TransmissionReference.TabIndex = 8;
            this.TransmissionReference.Text = "Transmission Reference:";
            // 
            // WebAddressTxt
            // 
            this.WebAddressTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.WebAddressTxt.Location = new System.Drawing.Point(201, 118);
            this.WebAddressTxt.Name = "WebAddressTxt";
            this.WebAddressTxt.Size = new System.Drawing.Size(201, 24);
            this.WebAddressTxt.TabIndex = 7;
            // 
            // WebAddress
            // 
            this.WebAddress.AutoSize = true;
            this.WebAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.WebAddress.Location = new System.Drawing.Point(94, 121);
            this.WebAddress.Name = "WebAddress";
            this.WebAddress.Size = new System.Drawing.Size(101, 18);
            this.WebAddress.TabIndex = 6;
            this.WebAddress.Text = "Web Address:";
            // 
            // StockTickerTxt
            // 
            this.StockTickerTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.StockTickerTxt.Location = new System.Drawing.Point(201, 88);
            this.StockTickerTxt.Name = "StockTickerTxt";
            this.StockTickerTxt.Size = new System.Drawing.Size(201, 24);
            this.StockTickerTxt.TabIndex = 5;
            // 
            // StockTicker
            // 
            this.StockTicker.AutoSize = true;
            this.StockTicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.StockTicker.Location = new System.Drawing.Point(99, 91);
            this.StockTicker.Name = "StockTicker";
            this.StockTicker.Size = new System.Drawing.Size(96, 18);
            this.StockTicker.TabIndex = 4;
            this.StockTicker.Text = "Stock Ticker:";
            // 
            // PhoneTxt
            // 
            this.PhoneTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.PhoneTxt.Location = new System.Drawing.Point(201, 58);
            this.PhoneTxt.Name = "PhoneTxt";
            this.PhoneTxt.Size = new System.Drawing.Size(201, 24);
            this.PhoneTxt.TabIndex = 3;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Phone.Location = new System.Drawing.Point(83, 61);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(112, 18);
            this.Phone.TabIndex = 2;
            this.Phone.Text = "Phone Number:";
            // 
            // CompanyNameTxt
            // 
            this.CompanyNameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CompanyNameTxt.Location = new System.Drawing.Point(201, 28);
            this.CompanyNameTxt.Name = "CompanyNameTxt";
            this.CompanyNameTxt.Size = new System.Drawing.Size(201, 24);
            this.CompanyNameTxt.TabIndex = 1;
            // 
            // CompanyName
            // 
            this.CompanyName.AutoSize = true;
            this.CompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CompanyName.Location = new System.Drawing.Point(75, 31);
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Size = new System.Drawing.Size(120, 18);
            this.CompanyName.TabIndex = 0;
            this.CompanyName.Text = "Company Name:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SubmitRequestButton);
            this.tabPage2.Controls.Add(this.CheckAllButton);
            this.tabPage2.Controls.Add(this.treeView2);
            this.tabPage2.Controls.Add(this.treeView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(822, 300);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Output Options";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SubmitRequestButton
            // 
            this.SubmitRequestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.SubmitRequestButton.Location = new System.Drawing.Point(635, 221);
            this.SubmitRequestButton.Name = "SubmitRequestButton";
            this.SubmitRequestButton.Size = new System.Drawing.Size(150, 35);
            this.SubmitRequestButton.TabIndex = 3;
            this.SubmitRequestButton.Text = "Submit Request";
            this.SubmitRequestButton.UseVisualStyleBackColor = true;
            this.SubmitRequestButton.Click += new System.EventHandler(this.SubmitRequestButton_Click);
            // 
            // CheckAllButton
            // 
            this.CheckAllButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CheckAllButton.Location = new System.Drawing.Point(445, 221);
            this.CheckAllButton.Name = "CheckAllButton";
            this.CheckAllButton.Size = new System.Drawing.Size(150, 35);
            this.CheckAllButton.TabIndex = 2;
            this.CheckAllButton.Text = "Check All";
            this.CheckAllButton.UseVisualStyleBackColor = true;
            this.CheckAllButton.Click += new System.EventHandler(this.CheckAllButton_Click);
            // 
            // treeView2
            // 
            this.treeView2.CheckBoxes = true;
            this.treeView2.Location = new System.Drawing.Point(6, 6);
            this.treeView2.Name = "treeView2";
            treeNode1.Name = "Plus4";
            treeNode1.Text = "Plus4";
            treeNode2.Name = "DeliveryIndicator";
            treeNode2.Text = "Delivery Indicator";
            treeNode3.Name = "MelissaAddressKey";
            treeNode3.Text = "MelissaAddressKey";
            treeNode4.Name = "MelissaAddressKeyBase";
            treeNode4.Text = "MelissaAddressKeyBase";
            treeNode5.Name = "GrpAddressDetails";
            treeNode5.Text = "Address Details";
            treeNode6.Name = "SICCode1";
            treeNode6.Text = "SIC Code 1";
            treeNode7.Name = "SICCode2";
            treeNode7.Text = "SIC Code 2";
            treeNode8.Name = "SICCode3";
            treeNode8.Text = "SIC Code 3";
            treeNode9.Name = "NAICSCode1";
            treeNode9.Text = "NAICS Code 1";
            treeNode10.Name = "NAICSCode2";
            treeNode10.Text = "NAICS Code 2";
            treeNode11.Name = "NAICSCode3";
            treeNode11.Text = "NAICS Code 3";
            treeNode12.Name = "GrpBusinessCodes";
            treeNode12.Text = "Business Codes";
            treeNode13.Name = "SICDescription1";
            treeNode13.Text = "SICDescription1";
            treeNode14.Name = "SICDescription2";
            treeNode14.Text = "SIC Description 2";
            treeNode15.Name = "SICDescription3";
            treeNode15.Text = "SIC Description 3";
            treeNode16.Name = "NAICSDescription1";
            treeNode16.Text = "NAICS Description 1";
            treeNode17.Name = "NAICSDescription2";
            treeNode17.Text = "NAICS Description 2";
            treeNode18.Name = "NAICSDescription3";
            treeNode18.Text = "NAICS Description 3";
            treeNode19.Name = "GrpBusinessDescription";
            treeNode19.Text = "Business Description";
            treeNode20.Name = "Latitude";
            treeNode20.Text = "Latitude";
            treeNode21.Name = "Longitude";
            treeNode21.Text = "Longitude";
            treeNode22.Name = "GrpGeoCode";
            treeNode22.Text = "Geo Code";
            treeNode23.Name = "CountyName";
            treeNode23.Text = "County Name";
            treeNode24.Name = "CountyFIPS";
            treeNode24.Text = "County FIPS";
            treeNode25.Name = "CensusTract";
            treeNode25.Text = "Census Tract";
            treeNode26.Name = "CensusBlock";
            treeNode26.Text = "Census Block";
            treeNode27.Name = "PlaceCode";
            treeNode27.Text = "Place Code";
            treeNode28.Name = "PlaceName";
            treeNode28.Text = "Place Name";
            treeNode29.Name = "GrpCensusDetails";
            treeNode29.Text = "Census Details";
            this.treeView2.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode12,
            treeNode19,
            treeNode22,
            treeNode29});
            this.treeView2.Size = new System.Drawing.Size(403, 288);
            this.treeView2.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.CheckBoxes = true;
            this.treeView1.Location = new System.Drawing.Point(415, 6);
            this.treeView1.Name = "treeView1";
            treeNode30.Checked = true;
            treeNode30.Name = "LocationType";
            treeNode30.Text = "Location Type";
            treeNode31.Checked = true;
            treeNode31.Name = "Phone";
            treeNode31.Text = "Phone";
            treeNode32.Checked = true;
            treeNode32.Name = "StockTicker";
            treeNode32.Text = "Stock Ticker";
            treeNode33.Checked = true;
            treeNode33.Name = "WebAddress";
            treeNode33.Text = "Web Address";
            treeNode34.Checked = true;
            treeNode34.Name = "EmployeesEstimate";
            treeNode34.Text = "Employees Estimate";
            treeNode35.Checked = true;
            treeNode35.Name = "SalesEstimate";
            treeNode35.Text = "Sales Estimate";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33,
            treeNode34,
            treeNode35});
            this.treeView1.Size = new System.Drawing.Size(404, 172);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.JSONRequestTxt);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(822, 300);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "JSON Request";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // JSONRequestTxt
            // 
            this.JSONRequestTxt.Location = new System.Drawing.Point(7, 7);
            this.JSONRequestTxt.Name = "JSONRequestTxt";
            this.JSONRequestTxt.Size = new System.Drawing.Size(809, 312);
            this.JSONRequestTxt.TabIndex = 0;
            this.JSONRequestTxt.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.JSONResponseTxt);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(822, 300);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "JSON Raw Output";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // JSONResponseTxt
            // 
            this.JSONResponseTxt.Location = new System.Drawing.Point(4, 4);
            this.JSONResponseTxt.Name = "JSONResponseTxt";
            this.JSONResponseTxt.Size = new System.Drawing.Size(815, 318);
            this.JSONResponseTxt.TabIndex = 0;
            this.JSONResponseTxt.Text = "";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 397);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(854, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(636, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 50);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // URLTxt
            // 
            this.URLTxt.FormattingEnabled = true;
            this.URLTxt.Location = new System.Drawing.Point(82, 40);
            this.URLTxt.Name = "URLTxt";
            this.URLTxt.Size = new System.Drawing.Size(539, 21);
            this.URLTxt.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 419);
            this.Controls.Add(this.URLTxt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Url);
            this.Controls.Add(this.LicenseTxt);
            this.Controls.Add(this.License);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label License;
        private System.Windows.Forms.TextBox LicenseTxt;
        private System.Windows.Forms.Label Url;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label CompanyName;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox JSONRequestTxt;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.RichTextBox JSONResponseTxt;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox RecordIDTxt;
        private System.Windows.Forms.Label RecordID;
        private System.Windows.Forms.TextBox TransmissionReferenceTxt;
        private System.Windows.Forms.Label TransmissionReference;
        private System.Windows.Forms.TextBox WebAddressTxt;
        private System.Windows.Forms.Label WebAddress;
        private System.Windows.Forms.TextBox StockTickerTxt;
        private System.Windows.Forms.Label StockTicker;
        private System.Windows.Forms.TextBox PhoneTxt;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox CompanyNameTxt;
        private System.Windows.Forms.ComboBox URLTxt;
        private System.Windows.Forms.CheckBox ReturnDominantBusinessCheck;
        private System.Windows.Forms.Label ReturnDominantBusiness;
        private System.Windows.Forms.TextBox MelissaAddressKeyTxt;
        private System.Windows.Forms.Label MelissaAddressKey;
        private System.Windows.Forms.CheckBox PhoneInput;
        private System.Windows.Forms.CheckBox WebAddressInput;
        private System.Windows.Forms.CheckBox StockTickerInput;
        private System.Windows.Forms.CheckBox MelissaAddressKeyInput;
        private System.Windows.Forms.CheckBox AddressInput;
        private System.Windows.Forms.Label Input;
        private System.Windows.Forms.Label Country;
        private System.Windows.Forms.Label Postal;
        private System.Windows.Forms.Label State;
        private System.Windows.Forms.Label City;
        private System.Windows.Forms.Label Address2;
        private System.Windows.Forms.Label Address1;
        private System.Windows.Forms.TextBox Address1Txt;
        private System.Windows.Forms.TextBox Address2Txt;
        private System.Windows.Forms.TextBox CityTxt;
        private System.Windows.Forms.TextBox StateTxt;
        private System.Windows.Forms.TextBox PostalTxt;
        private System.Windows.Forms.TextBox CountryTxt;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button SubmitRequestButton;
        private System.Windows.Forms.Button CheckAllButton;
    }
}