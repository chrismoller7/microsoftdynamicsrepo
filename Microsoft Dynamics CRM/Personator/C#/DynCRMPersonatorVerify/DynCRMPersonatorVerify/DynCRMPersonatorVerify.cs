﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;


namespace DynCRMPersonatorVerify
{
    public class DynCRMPersonatorVerify : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inActions")]
        [RequiredArgument]
        public InArgument<String> Actions { get; set; }

        [Input("inColumns")]
        [RequiredArgument]
        public InArgument<String> Columns { get; set; }

        [Output("outAddressDeliveryInstallation")]
        public OutArgument<String> AddressDeliveryInstallation { get; set; }

        [Output("outAddressExtras")]
        public OutArgument<String> AddressExtras { get; set; }

        [Output("outAddressHouseNumber")]
        public OutArgument<String> AddressHouseNumber { get; set; }

        [Output("outAddressKey")]
        public OutArgument<String> AddressKey { get; set; }

        [Input("inAddressLine1")]
        [Output("outAddressLine1")]
        public InOutArgument<String> AddressLine1 { get; set; }

        [Input("inAddressLine2")]
        [Output("outAddressLine2")]
        public InOutArgument<String> AddressLine2 { get; set; }

        [Output("outAddressLockBox")]
        public OutArgument<String> AddressLockBox { get; set; }

        [Output("outAddressPostDirection")]
        public OutArgument<String> AddressPostDirection { get; set; }

        [Output("outAddressPreDirection")]
        public OutArgument<String> AddressPreDirection { get; set; }

        [Output("outAddressPrivateMailboxName")]
        public OutArgument<String> AddressPrivateMailboxName { get; set; }

        [Output("outAddressPrivateMailboxRange")]
        public OutArgument<String> AddressPrivateMailboxRange { get; set; }

        [Output("outAddressRouteService")]
        public OutArgument<String> AddressRouteService { get; set; }

        [Output("outAddressStreetName")]
        public OutArgument<String> AddressStreetName { get; set; }

        [Output("outAddressStreetSuffix")]
        public OutArgument<String> AddressStreetSuffix { get; set; }

        [Output("outAddressSuiteName")]
        public OutArgument<String> AddressSuiteName { get; set; }

        [Output("outAddressSuiteNumber")]
        public OutArgument<String> AddressSuiteNumber { get; set; }

        [Output("outAddressTypeCode")]
        public OutArgument<String> AddressTypeCode { get; set; }

        [Output("outAreaCode")]
        public OutArgument<String> AreaCode { get; set; }

        [Output("outCarrierRoute")]
        public OutArgument<String> CarrierRoute { get; set; }

        [Output("outCBSACode")]
        public OutArgument<String> CBSACode { get; set; }

        [Output("outCBSADivisionCode")]
        public OutArgument<String> CBSADivisionCode { get; set; }

        [Output("outCBSADivisionLevel")]
        public OutArgument<String> CBSADivisionLevel { get; set; }

        [Output("outCBSADivisionTitle")]
        public OutArgument<String> CBSADivisionTitle { get; set; }

        [Output("outCBSALevel")]
        public OutArgument<String> CBSALevel { get; set; }

        [Output("outCBSATitle")]
        public OutArgument<String> CBSATitle { get; set; }

        [Output("outCensusBlock")]
        public OutArgument<String> CensusBlock { get; set; }

        [Output("outCensusKey")]
        public OutArgument<String> CensusKey { get; set; }

        [Output("outCensusTract")]
        public OutArgument<String> CensusTract { get; set; }

        [Input("inCity")]
        [Output("outCity")]
        public InOutArgument<String> City { get; set; }

        [Output("outCityAbbreviation")]
        public OutArgument<String> CityAbbreviation { get; set; }

        [Input("inCompanyName")]
        [Output("outCompanyName")]
        public InOutArgument<String> CompanyName { get; set; }

        [Output("outCongressionalDistrict")]
        public OutArgument<String> CongressionalDistrict { get; set; }

        [Input("inCountry")]
        public InArgument<String> Country { get; set; }

        [Output("outCountryCode")]
        public OutArgument<String> CountryCode { get; set; }

        [Output("outCountryName")]
        public OutArgument<String> CountryName { get; set; }

        [Output("outCountyFIPS")]
        public OutArgument<String> CountyFIPS { get; set; }

        [Output("outCountyName")]
        public OutArgument<String> CountyName { get; set; }

        [Output("outCountySubdivisionCode")]
        public OutArgument<String> CountySubdivisionCode { get; set; }

        [Output("outCountySubdivisionName")]
        public OutArgument<String> CountySubdivisionName { get; set; }

        [Output("outDateOfBirth")]
        public OutArgument<String> DateOfBirth { get; set; }

        [Output("outDateOfDeath")]
        public OutArgument<String> DateOfDeath { get; set; }

        [Output("outDeliveryIndicator")]
        public OutArgument<String> DeliveryIndicator { get; set; }

        [Output("outDeliveryPointCheckDigit")]
        public OutArgument<String> DeliveryPointCheckDigit { get; set; }

        [Output("outDeliveryPointCode")]
        public OutArgument<String> DeliveryPointCode { get; set; }

        [Output("outDemographicsGender")]
        public OutArgument<String> DemographicsGender { get; set; }

        [Output("outDemographicsResults")]
        public OutArgument<String> DemographicsResults { get; set; }

        [Output("outDomainName")]
        public OutArgument<String> DomainName { get; set; }

        [Output("outElementarySchoolDistrictCode")]
        public OutArgument<String> ElementarySchoolDistrictCode { get; set; }

        [Output("outElementarySchoolDistrictName")]
        public OutArgument<String> ElementarySchoolDistrictName { get; set; }

        [Input("inEmailAddress")]
        [Output("outEmailAddress")]
        public InOutArgument<String> EmailAddress { get; set; }

        [Output("outGender")]
        public OutArgument<String> Gender { get; set; }

        [Output("outGender2")]
        public OutArgument<String> Gender2 { get; set; }

        [Input("inFirstName")]
        public InArgument<String> FirstName { get; set; }

        [Input("inFreeForm")]
        public InArgument<String> FreeForm { get; set; }

        [Input("inFullName")]
        public InArgument<String> FullName { get; set; }

        [Output("outHouseholdIncome")]
        public OutArgument<String> HouseholdIncome { get; set; }

        [Input("inLastLine")]
        public InArgument<String> LastLine { get; set; }

        [Input("inLastName")]
        public InArgument<String> LastName { get; set; }

        [Output("outLatitude")]
        public OutArgument<String> Latitude { get; set; }

        [Output("outLengthOfResidence")]
        public OutArgument<String> LengthOfResidence { get; set; }

        [Output("outLongitude")]
        public OutArgument<String> Longitude { get; set; }

        [Output("outMailboxName")]
        public OutArgument<String> MailboxName { get; set; }

        [Output("outMaritalStatus")]
        public OutArgument<String> MaritalStatus { get; set; }

        [Output("outMelissaAddressKey")]
        public OutArgument<String> MelissaAddressKey { get; set; }

        [Output("outNameFirst")]
        public OutArgument<String> NameFirst { get; set; }

        [Output("outNameFirst2")]
        public OutArgument<String> NameFirst2 { get; set; }

        [Output("outNameFull")]
        public OutArgument<String> NameFull { get; set; }

        [Output("outNameLast")]
        public OutArgument<String> NameLast { get; set; }

        [Output("outNameLast2")]
        public OutArgument<String> NameLast2 { get; set; }

        [Output("outNameMiddle")]
        public OutArgument<String> NameMiddle { get; set; }

        [Output("outNameMiddle2")]
        public OutArgument<String> NameMiddle2 { get; set; }

        [Output("outNamePrefix")]
        public OutArgument<String> NamePrefix { get; set; }

        [Output("outNamePrefix2")]
        public OutArgument<String> NamePrefix2 { get; set; }

        [Output("outNameSuffix")]
        public OutArgument<String> NameSuffix { get; set; }

        [Output("outNameSuffix2")]
        public OutArgument<String> NameSuffix2 { get; set; }

        [Output("outNewAreaCode")]
        public OutArgument<String> NewAreaCode { get; set; }

        [Output("outOccupation")]
        public OutArgument<String> Occupation { get; set; }

        [Output("outOwnRent")]
        public OutArgument<String> OwnRent { get; set; }

        [Output("outPhoneCountryCode")]
        public OutArgument<String> PhoneCountryCode { get; set; }

        [Output("outPhoneCountryName")]
        public OutArgument<String> PhoneCountryName { get; set; }

        [Output("outPhoneExtension")]
        public OutArgument<String> PhoneExtension { get; set; }

        [Input("inPhoneNumber")]
        [Output("outPhoneNumber")]
        public InOutArgument<String> PhoneNumber { get; set; }

        [Output("outPhonePrefix")]
        public OutArgument<String> PhonePrefix { get; set; }

        [Output("outPhoneSuffix")]
        public OutArgument<String> PhoneSuffix { get; set; }

        [Output("outPlaceCode")]
        public OutArgument<String> PlaceCode { get; set; }

        [Output("outPlaceName")]
        public OutArgument<String> PlaceName { get; set; }

        [Output("outPlus4")]
        public OutArgument<String> Plus4 { get; set; }

        [Input("inPostalCode")]
        [Output("outPostalCode")]
        public InOutArgument<String> PostalCode { get; set; }

        [Output("outPresenceOfChildren")]
        public OutArgument<String> PresenceOfChildren { get; set; }

        [Output("outPrivateMailBox")]
        public OutArgument<String> PrivateMailBox { get; set; }

        [Output("outRecordExtras")]
        public OutArgument<String> RecordExtras { get; set; }

        [Input("inRecordID")]
        [Output("outRecordID")]
        public InOutArgument<String> RecordID { get; set; }

        [Input("inReserved")]
        [Output("outReserved")]
        public InOutArgument<String> Reserved { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outSalutation")]
        public OutArgument<String> Salutation { get; set; }

        [Output("outSecondarySchoolDistrictCode")]
        public OutArgument<String> SecondarySchoolDistrictCode { get; set; }

        [Output("outSecondarySchoolDistrictName")]
        public OutArgument<String> SecondarySchoolDistrictName { get; set; }

        [Input("inState")]
        [Output("outState")]
        public InOutArgument<String> State { get; set; }

        [Output("outStateDistrictLower")]
        public OutArgument<String> StateDistrictLower { get; set; }

        [Output("outStateDistrictUpper")]
        public OutArgument<String> StateDistrictUpper { get; set; }

        [Output("outStateName")]
        public OutArgument<String> StateName { get; set; }

        [Output("outSuite")]
        public OutArgument<String> Suite { get; set; }

        [Output("outTopLevelDomain")]
        public OutArgument<String> TopLevelDomain { get; set; }

        [Output("outUnifiedSchoolDistrictCode")]
        public OutArgument<String> UnifiedSchoolDistrictCode { get; set; }

        [Output("outUnifiedSchoolDistrictName")]
        public OutArgument<String> UnifiedSchoolDistrictName { get; set; }

        [Output("outUrbanizationName")]
        public OutArgument<String> UrbanizationName { get; set; }

        [Output("outUTC")]
        public OutArgument<String> UTC { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            PersonatorWS.ServicemdContactVerifySOAPClient service = new PersonatorWS.ServicemdContactVerifySOAPClient();
            PersonatorWS.Request request = new PersonatorWS.Request();
            PersonatorWS.Response response = new PersonatorWS.Response();
            PersonatorWS.RequestRecord reqRecord = new PersonatorWS.RequestRecord();

            request.CustomerID = CustomerID.Get(context);
            request.TransmissionReference = "Microsoft Dynamics CRM PersonatorWS";
            request.Actions = Actions.Get(context);
            request.Options = Options.Get(context);
            request.Columns = Columns.Get(context);
            request.Records = new PersonatorWS.RequestRecord[1];

            reqRecord.AddressLine1 = AddressLine1.Get(context);
            reqRecord.AddressLine2 = AddressLine2.Get(context);
            reqRecord.City = City.Get(context);
            reqRecord.CompanyName = CompanyName.Get(context);
            reqRecord.Country = Country.Get(context);
            reqRecord.EmailAddress = EmailAddress.Get(context);
            reqRecord.FirstName = FirstName.Get(context);
            reqRecord.FreeForm = FreeForm.Get(context);
            reqRecord.FullName = FullName.Get(context);
            reqRecord.LastLine = LastLine.Get(context);
            reqRecord.LastName = LastName.Get(context);
            reqRecord.PhoneNumber = PhoneNumber.Get(context);
            reqRecord.PostalCode = PostalCode.Get(context);
            reqRecord.RecordID = RecordID.Get(context);
            reqRecord.Reserved = Reserved.Get(context);
            reqRecord.State = State.Get(context);


            request.Records[0] = reqRecord;

            String errorMessage = "";
            try
            {
                response = service.doContactVerify(request);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact melissa data support. Error Message: " + ex.Message;
            }

            String outAddressDeliveryInstallation = "";
            String outAddressExtras = "";
            String outAddressHouseNumber = "";
            String outAddressKey = "";
            String outAddressLine1 = "";
            String outAddressLine2 = "";
            String outAddressLockBox = "";
            String outAddressPostDirection = "";
            String outAddressPreDirection = "";
            String outAddressPrivateMailboxName = "";
            String outAddressPrivateMailboxRange = "";
            String outAddressRouteService = "";
            String outAddressStreetName = "";
            String outAddressStreetSuffix = "";
            String outAddressSuiteName = "";
            String outAddressSuiteNumber = "";
            String outAddressTypeCode = "";
            String outAreaCode = "";
            String outCarrierRoute = "";
            String outCBSACode = "";
            String outCBSADivisionCode = "";
            String outCBSADivisionLevel = "";
            String outCBSADivisionTitle = "";
            String outCBSALevel = "";
            String outCBSATitle = "";
            String outCensusBlock = "";
            String outCensusKey = "";
            String outCensusTract = "";
            String outCity = "";
            String outCityAbbreviation = "";
            String outCompanyName = "";
            String outCongressionalDistrict = "";
            String outCountryCode = "";
            String outCountryName = "";
            String outCountyFIPS = "";
            String outCountyName = "";
            String outCountySubdivisionCode = "";
            String outCountySubdivisionName = "";
            String outDateOfBirth = "";
            String outDateOfDeath = "";
            String outDeliveryIndicator = "";
            String outDeliveryPointCheckDigit = "";
            String outDeliveryPointCode = "";
            String outDemographicsGender = "";
            String outDemographicsResults = "";
            String outDomainName = "";
            String outElementarySchoolDistrictCode = "";
            String outElementarySchoolDistrictName = "";
            String outEmailAddress = "";
            String outGender = "";
            String outGender2 = "";
            String outHouseholdIncome = "";
            String outLatitude = "";
            String outLengthOfResidence = "";
            String outLongitude = "";
            String outMailboxName = "";
            String outMaritalStatus = "";
            String outMelissaAddressKey = "";
            String outNameFirst = "";
            String outNameFirst2 = "";
            String outNameFull = "";
            String outNameLast = "";
            String outNameLast2 = "";
            String outNameMiddle = "";
            String outNameMiddle2 = "";
            String outNamePrefix = "";
            String outNamePrefix2 = "";
            String outNameSuffix = "";
            String outNameSuffix2 = "";
            String outNewAreaCode = "";
            String outOccupation = "";
            String outOwnRent = "";
            String outPhoneCountryCode = "";
            String outPhoneCountryName = "";
            String outPhoneExtension = "";
            String outPhoneNumber = "";
            String outPhonePrefix = "";
            String outPhoneSuffix = "";
            String outPlaceCode = "";
            String outPlaceName = "";
            String outPlus4 = "";
            String outPostalCode = "";
            String outPresenceOfChildren = "";
            String outPrivateMailBox = "";
            String outRecordExtras = "";
            String outRecordID = "";
            String outReserved = "";
            String outResults = "";
            String outSalutation = "";
            String outSecondarySchoolDistrictCode = "";
            String outSecondarySchoolDistrictName = "";
            String outState = "";
            String outStateDistrictLower = "";
            String outStateDistrictUpper = "";
            String outStateName = "";
            String outSuite = "";
            String outTopLevelDomain = "";
            String outUnifiedSchoolDistrictCode = "";
            String outUnifiedSchoolDistrictName = "";
            String outUrbanizationName = "";
            String outUTC = "";
            String outTransmissionResults = "";
            outTransmissionResults = response.TransmissionResults;
            String outError = "";

            if (errorMessage.Length > 0)
            {
                outError = errorMessage;
            }
            else if (response.TransmissionResults.Length > 0)
            {
                outError = response.TransmissionResults;
            }
            else
            {
                PersonatorWS.ResponseRecord resRecord = new PersonatorWS.ResponseRecord();
                resRecord = response.Records[0];

                outAddressDeliveryInstallation = resRecord.AddressDeliveryInstallation;
                outAddressExtras = resRecord.AddressExtras;
                outAddressHouseNumber = resRecord.AddressHouseNumber;
                outAddressKey = resRecord.AddressKey;
                outAddressLine1 = resRecord.AddressLine1;
                outAddressLine2 = resRecord.AddressLine2;
                outAddressLockBox = resRecord.AddressLockBox;
                outAddressPostDirection = resRecord.AddressPostDirection;
                outAddressPreDirection = resRecord.AddressPreDirection;
                outAddressPrivateMailboxName = resRecord.AddressPrivateMailboxName;
                outAddressPrivateMailboxRange = resRecord.AddressPrivateMailboxRange;
                outAddressRouteService = resRecord.AddressRouteService;
                outAddressStreetName = resRecord.AddressStreetName;
                outAddressStreetSuffix = resRecord.AddressStreetSuffix;
                outAddressSuiteName = resRecord.AddressSuiteName;
                outAddressSuiteNumber = resRecord.AddressSuiteNumber;
                outAddressTypeCode = resRecord.AddressTypeCode;
                outAreaCode = resRecord.AreaCode;
                outCarrierRoute = resRecord.CarrierRoute;
                outCBSACode = resRecord.CBSACode;
                outCBSADivisionCode = resRecord.CBSADivisionCode;
                outCBSADivisionLevel = resRecord.CBSADivisionLevel;
                outCBSADivisionTitle = resRecord.CBSADivisionTitle;
                outCBSALevel = resRecord.CBSALevel;
                outCBSATitle = resRecord.CBSATitle;
                outCensusBlock = resRecord.CensusBlock;
                outCensusKey = resRecord.CensusKey;
                outCensusTract = resRecord.CensusTract;
                outCity = resRecord.City;
                outCityAbbreviation = resRecord.CityAbbreviation;
                outCompanyName = resRecord.CompanyName;
                outCongressionalDistrict = resRecord.CongressionalDistrict;
                outCountryCode = resRecord.CountryName;
                outCountryName = resRecord.CountryName;
                outCountyFIPS = resRecord.CountyFIPS;
                outCountyName = resRecord.CountyName;
                outCountySubdivisionCode = resRecord.CountySubdivisionCode;
                outCountySubdivisionName = resRecord.CountySubdivisionName;
                outDateOfBirth = resRecord.DateOfBirth;
                outDateOfDeath = resRecord.DateOfDeath;
                outDeliveryIndicator = resRecord.DeliveryIndicator;
                outDeliveryPointCheckDigit = resRecord.DeliveryPointCheckDigit;
                outDeliveryPointCode = resRecord.DeliveryPointCode;
                outDemographicsGender = resRecord.DemographicsGender;
                outDemographicsResults = resRecord.DemographicsResults;
                outDomainName = resRecord.DomainName;
                outElementarySchoolDistrictCode = resRecord.ElementarySchoolDistrictCode;
                outElementarySchoolDistrictName = resRecord.ElementarySchoolDistrictName;
                outEmailAddress = resRecord.EmailAddress;
                outGender = resRecord.Gender;
                outGender2 = resRecord.Gender2;
                outHouseholdIncome = resRecord.HouseholdIncome;
                outLatitude = resRecord.Latitude;
                outLengthOfResidence = resRecord.LengthOfResidence;
                outLongitude = resRecord.Longitude;
                outMailboxName = resRecord.MailboxName;
                outMaritalStatus = resRecord.MaritalStatus;
                outMelissaAddressKey = resRecord.MelissaAddressKey;
                outNameFirst = resRecord.NameFirst;
                outNameFirst2 = resRecord.NameFirst2;
                outNameFull = resRecord.NameFull;
                outNameLast = resRecord.NameLast;
                outNameLast2 = resRecord.NameLast2;
                outNameMiddle = resRecord.NameMiddle;
                outNameMiddle2 = resRecord.NameMiddle2;
                outNamePrefix = resRecord.NamePrefix;
                outNamePrefix2 = resRecord.NamePrefix2;
                outNameSuffix = resRecord.NameSuffix;
                outNameSuffix2 = resRecord.NameSuffix2;
                outNewAreaCode = resRecord.NewAreaCode;
                outOccupation = resRecord.Occupation;
                outOwnRent = resRecord.OwnRent;
                outPhoneCountryCode = resRecord.PhoneCountryCode;
                outPhoneCountryName = resRecord.PhoneCountryName;
                outPhoneExtension = resRecord.PhoneExtension;
                outPhoneNumber = resRecord.PhoneNumber;
                outPhonePrefix = resRecord.PhonePrefix;
                outPhoneSuffix = resRecord.PhoneSuffix;
                outPlaceCode = resRecord.PlaceCode;
                outPlaceName = resRecord.PlaceName;
                outPlus4 = resRecord.Plus4;
                outPostalCode = resRecord.PostalCode;
                outPresenceOfChildren = resRecord.PresenceOfChildren;
                outPrivateMailBox = resRecord.PrivateMailBox;
                outRecordExtras = resRecord.RecordExtras;
                outRecordID = resRecord.RecordID;
                outReserved = resRecord.Reserved;
                outResults = resRecord.Results;
                outSalutation = resRecord.Salutation;
                outSecondarySchoolDistrictCode = resRecord.SecondarySchoolDistrictCode;
                outSecondarySchoolDistrictName = resRecord.SecondarySchoolDistrictName;
                outState = resRecord.State;
                outStateDistrictLower = resRecord.StateDistrictLower;
                outStateDistrictUpper = resRecord.StateDistrictUpper;
                outStateName = resRecord.StateName;
                outSuite = resRecord.Suite;
                outTopLevelDomain = resRecord.TopLevelDomain;
                outUnifiedSchoolDistrictCode = resRecord.UnifiedSchoolDistrictCode;
                outUnifiedSchoolDistrictName = resRecord.UnifiedSchoolDistrictName;
                outUrbanizationName = resRecord.UrbanizationName;
                outUTC = resRecord.UrbanizationName;
            }


            AddressDeliveryInstallation.Set(context, outAddressDeliveryInstallation);
            AddressExtras.Set(context, outAddressExtras);
            AddressHouseNumber.Set(context, outAddressHouseNumber);
            AddressKey.Set(context, outAddressKey);
            AddressLine1.Set(context, outAddressLine1);
            AddressLine2.Set(context, outAddressLine2);
            AddressLockBox.Set(context, outAddressLockBox);
            AddressPostDirection.Set(context, outAddressPostDirection);
            AddressPreDirection.Set(context, outAddressPreDirection);
            AddressPrivateMailboxName.Set(context, outAddressPrivateMailboxName);
            AddressPrivateMailboxRange.Set(context, outAddressPrivateMailboxRange);
            AddressRouteService.Set(context, outAddressRouteService);
            AddressStreetName.Set(context, outAddressStreetName);
            AddressStreetSuffix.Set(context, outAddressStreetSuffix);
            AddressSuiteName.Set(context, outAddressSuiteName);
            AddressSuiteNumber.Set(context, outAddressSuiteNumber);
            AddressTypeCode.Set(context, outAddressTypeCode);
            AreaCode.Set(context, outAreaCode);
            CarrierRoute.Set(context, outCarrierRoute);
            CBSACode.Set(context, outCBSACode);
            CBSADivisionCode.Set(context, outCBSADivisionCode);
            CBSADivisionLevel.Set(context, outCBSADivisionLevel);
            CBSADivisionTitle.Set(context, outCBSADivisionTitle);
            CBSALevel.Set(context, outCBSALevel);
            CBSATitle.Set(context, outCBSATitle);
            CensusBlock.Set(context, outCensusBlock);
            CensusKey.Set(context, outCensusKey);
            CensusTract.Set(context, outCensusTract);
            City.Set(context, outCity);
            CityAbbreviation.Set(context, outCityAbbreviation);
            CompanyName.Set(context, outCompanyName);
            CongressionalDistrict.Set(context, outCongressionalDistrict);
            CountryCode.Set(context, outCountryName);
            CountryName.Set(context, outCountryName);
            CountyFIPS.Set(context, outCountyFIPS);
            CountyName.Set(context, outCountyName);
            CountySubdivisionCode.Set(context, outCountySubdivisionCode);
            CountySubdivisionName.Set(context, outCountySubdivisionName);
            DateOfBirth.Set(context, outDateOfBirth);
            DateOfDeath.Set(context, outDateOfDeath);
            DeliveryIndicator.Set(context, outDeliveryIndicator);
            DeliveryPointCheckDigit.Set(context, outDeliveryPointCheckDigit);
            DeliveryPointCode.Set(context, outDeliveryPointCode);
            DemographicsGender.Set(context, outDemographicsGender);
            DemographicsResults.Set(context, outDemographicsResults);
            DomainName.Set(context, outDomainName);
            ElementarySchoolDistrictCode.Set(context, outElementarySchoolDistrictCode);
            ElementarySchoolDistrictName.Set(context, outElementarySchoolDistrictName);
            EmailAddress.Set(context, outEmailAddress);
            Gender.Set(context, outGender);
            Gender2.Set(context, outGender2);
            HouseholdIncome.Set(context, outHouseholdIncome);
            Latitude.Set(context, outLatitude);
            LengthOfResidence.Set(context, outLengthOfResidence);
            Longitude.Set(context, outLongitude);
            MailboxName.Set(context, outMailboxName);
            MaritalStatus.Set(context, outMaritalStatus);
            MelissaAddressKey.Set(context, outMelissaAddressKey);
            NameFirst.Set(context, outNameFirst);
            NameFirst2.Set(context, outNameFirst2);
            NameFull.Set(context, outNameFull);
            NameLast.Set(context, outNameLast);
            NameLast2.Set(context, outNameLast2);
            NameMiddle.Set(context, outNameMiddle);
            NameMiddle2.Set(context, outNameMiddle2);
            NamePrefix.Set(context, outNamePrefix);
            NamePrefix2.Set(context, outNamePrefix2);
            NameSuffix.Set(context, outNameSuffix);
            NameSuffix2.Set(context, outNameSuffix2);
            NewAreaCode.Set(context, outNewAreaCode);
            Occupation.Set(context, outOccupation);
            OwnRent.Set(context, outOwnRent);
            PhoneCountryCode.Set(context, outPhoneCountryCode);
            PhoneCountryName.Set(context, outPhoneCountryName);
            PhoneExtension.Set(context, outPhoneExtension);
            PhoneNumber.Set(context, outPhoneNumber);
            PhonePrefix.Set(context, outPhonePrefix);
            PhoneSuffix.Set(context, outPhoneSuffix);
            PlaceCode.Set(context, outPlaceCode);
            PlaceName.Set(context, outPlaceName);
            Plus4.Set(context, outPlus4);
            PostalCode.Set(context, outPostalCode);
            PresenceOfChildren.Set(context, outPresenceOfChildren);
            PrivateMailBox.Set(context, outPrivateMailBox);
            RecordExtras.Set(context, outRecordExtras);
            RecordID.Set(context, outRecordID);
            Reserved.Set(context, outReserved);
            Results.Set(context, outResults);
            Salutation.Set(context, outSalutation);
            SecondarySchoolDistrictCode.Set(context, outSecondarySchoolDistrictCode);
            SecondarySchoolDistrictName.Set(context, outSecondarySchoolDistrictName);
            State.Set(context, outState);
            StateDistrictLower.Set(context, outStateDistrictLower);
            StateDistrictUpper.Set(context, outStateDistrictUpper);
            StateName.Set(context, outStateName);
            Suite.Set(context, outSuite);
            TopLevelDomain.Set(context, outTopLevelDomain);
            UnifiedSchoolDistrictCode.Set(context, outUnifiedSchoolDistrictCode);
            UnifiedSchoolDistrictName.Set(context, outUnifiedSchoolDistrictName);
            UrbanizationName.Set(context, outUrbanizationName);
            UTC.Set(context, outUrbanizationName);
            Error.Set(context, outError);
            TransmissionResults.Set(context, outTransmissionResults);
        }
    }
}
