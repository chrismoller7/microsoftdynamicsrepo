﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace DynCRMGlobalEmail
{
    public class DynCRMGlobalEmail : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inEmail")]
        [Output("outEmail")]
        public InOutArgument<String> Email { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        [Output("outDomainName")]
        public OutArgument<String> DomainName { get; set; }

        [Output("outMailBoxName")]
        public OutArgument<String> MailBoxName { get; set; }
        
        [Output("outRecordID")]
        public OutArgument<String> RecordID { get; set; }
        
        [Output("outResults")]
        public OutArgument<String> Results { get; set; }
        
        [Output("outTopLevelDomain")]
        public OutArgument<String> TopLevelDomain { get; set; }

        [Output("outTopLevelDomainName")]
        public OutArgument<String> TopLevelDomainName { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            EndpointAddress address = new EndpointAddress("https://globalemail.melissadata.net/v3/SOAP/globalemail");

            globalemail.GlobalEmailSOAP service = new globalemail.GlobalEmailSOAP();
            service.Url = "https://globalemail.melissadata.net/v3/SOAP/globalemail";

            globalemail.Request request = new globalemail.Request();
            globalemail.Response response = new globalemail.Response();
            globalemail.RequestRecord reqRecord = new globalemail.RequestRecord();

            request.CustomerID = CustomerID.Get(context);
            request.TransmissionReference = "Microsoft Dynamics CRM Global Email";
            request.Options = Options.Get(context);

            request.Records = new globalemail.RequestRecord[1];
            reqRecord.RecordID = "1";
            reqRecord.Email = Email.Get(context);

            request.Records[0] = reqRecord;
            string errorMessage = "";
            try
            {
                response = service.doGlobalEmail(request);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact melissa data support. Error Message: " + ex.Message;
            }

            String outDomainName = "";
            String outEmailAddress = "";
            String outMailboxName = "";
            String outRecordID = "";
            String outResults = "";
            String outTopLevelDomain = "";
            String outTopLevelDomainName = "";
            String outTransmissionResults = "";
            outTransmissionResults = response.TransmissionResults;
            String outError = errorMessage;

            if (errorMessage.Length > 0)
            {
                outError = errorMessage;
            }
            else if (outTransmissionResults.Length - 1 > 0)
            {
                outError = outTransmissionResults;
            }
            else
            {
                outDomainName = response.Records[0].DomainName;
                outEmailAddress = response.Records[0].EmailAddress;
                outMailboxName = response.Records[0].MailboxName;
                outRecordID = response.Records[0].RecordID;
                outResults = response.Records[0].Results;
                outTopLevelDomain = response.Records[0].TopLevelDomain;
                outTopLevelDomainName = response.Records[0].TopLevelDomainName;
            }

            DomainName.Set(context, outDomainName);
            Email.Set(context, outEmailAddress);
            MailBoxName.Set(context, outMailboxName);
            RecordID.Set(context, outRecordID);
            Results.Set(context, outResults);
            TopLevelDomain.Set(context, outTopLevelDomain);
            TopLevelDomainName.Set(context, outTopLevelDomainName);
            Error.Set(context, outError);
            TransmissionResults.Set(context, outTransmissionResults);
        }
    }
}
