﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace DynCRMGlobalPhone
{
    public class DynCRMGlobalPhone : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inPhoneNumber")]
        [Output("outPhoneNumber")]
        public InOutArgument<String> PhoneNumber { get; set; }

        [Input("inCountry")]
        [Output("outCountry")]
        public InOutArgument<String> Country { get; set; }

        [Input("inCountryOfOrigin")]
        public InArgument<String> CountryOfOrigin { get; set; }

        [Output("outAdministrativeArea")]
        public OutArgument<String> AdministrativeArea { get; set; }

        [Output("outDST")]
        public OutArgument<String> DST { get; set; }

        [Output("outLanguage")]
        public OutArgument<String> Language { get; set; }

        [Output("outLatitude")]
        public OutArgument<String> Latitude { get; set; }

        [Output("outLocality")]
        public OutArgument<String> Locality { get; set; }

        [Output("outLongitude")]
        public OutArgument<String> Longitude { get; set; }

        [Output("outPhoneCountryDialingCode")]
        public OutArgument<String> PhoneCountryDialingCode { get; set; }

        [Output("outPhoneInternationalPrefix")]
        public OutArgument<String> PhoneInternationalPrefix { get; set; }

        [Output("outPhoneNationalDestinationCode")]
        public OutArgument<String> PhoneNationalDestinationCode { get; set; }

        [Output("outPhoneNationPrefix")]
        public OutArgument<String> PhoneNationPrefix { get; set; }

        [Output("outPhoneSubscriberNumber")]
        public OutArgument<String> PhoneSubscriberNumber { get; set; }

        [Output("outRecordID")]
        public OutArgument<String> RecordID { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outUTC")]
        public OutArgument<String> UTC { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        [Output("outVersion")]
        public OutArgument<String> Version { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            EndpointAddress address = new EndpointAddress("https://globalphone.melissadata.net/V3/SOAP/GlobalPhone");
            
            GlobalWS.GlobalPhoneSOAP service = new GlobalWS.GlobalPhoneSOAP();

            GlobalWS.Request request = new GlobalWS.Request();
            GlobalWS.Response response = new GlobalWS.Response();
            GlobalWS.RequestRecord reqRecord = new GlobalWS.RequestRecord();

            request.CustomerID = CustomerID.Get(context);
            request.TransmissionReference = "Microsoft Dynamics CRM Global Phone";
            request.Options = Options.Get(context);

            request.Records = new GlobalWS.RequestRecord[1];
            reqRecord.RecordID = "1";
            reqRecord.PhoneNumber = PhoneNumber.Get(context);
            reqRecord.Country = Country.Get(context);
            reqRecord.CountryOfOrigin = CountryOfOrigin.Get(context);

            request.Records[0] = reqRecord;

            string errorMessage = "";
            try
            {
                response = service.doGlobalPhone(request);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact melissa data support. Error Message: " + ex.Message;
            }

            String temp = response.ToString();
            XmlSerializer XmlSer = new XmlSerializer(response.GetType());
            StringWriter textWriter = new StringWriter();
            XmlSer.Serialize(textWriter, response);
            string temp1 = textWriter.ToString();

            String outAdministrativeArea = "";
            String outCountryName = "";
            String outDST = "";
            String outLanguage = "";
            String outLatitude = "";
            String outLocality = "";
            String outLongitude = "";
            String outPhoneCountryDialingCode = "";
            String outPhoneInternationalPrefix = "";
            String outPhoneNationalDestinationCode = "";
            String outPhoneNationPrefix = "";
            String outPhoneNumber = "";
            String outPhoneSubscriberNumber = "";
            String outRecordID = "";
            String outResults = "";
            String outUTC = "";
            String outTransmissionResults = "";
            outTransmissionResults = response.TransmissionResults;
            String outError = errorMessage;
            String outVersion = response.Version;

            if (errorMessage.Length > 0)
            {
                outError = errorMessage;
            }
            else if (outTransmissionResults.Length - 1 > 0)
            {
                outError = outTransmissionResults;
            }
            else
            {
                outAdministrativeArea = response.Records[0].AdministrativeArea;
                outCountryName = response.Records[0].CountryName;
                outDST = response.Records[0].DST;
                outLanguage = response.Records[0].Language;
                outLatitude = response.Records[0].Latitude;
                outLocality = response.Records[0].Locality;
                outLongitude = response.Records[0].Longitude;
                outPhoneCountryDialingCode = response.Records[0].PhoneCountryDialingCode;
                outPhoneInternationalPrefix = response.Records[0].PhoneInternationalPrefix;
                outPhoneNationalDestinationCode = response.Records[0].PhoneNationalDestinationCode;
                outPhoneNationPrefix = response.Records[0].PhoneNationPrefix;
                outPhoneNumber = response.Records[0].PhoneNumber;
                outPhoneSubscriberNumber = response.Records[0].PhoneSubscriberNumber;
                outRecordID = response.Records[0].RecordID;
                outResults = response.Records[0].Results;
                outUTC = response.Records[0].UTC;
            }

            Version.Set(context, outVersion);
            AdministrativeArea.Set(context, outAdministrativeArea);
            Country.Set(context, outCountryName);
            DST.Set(context, outDST);
            Language.Set(context, outLanguage);
            Latitude.Set(context, outLatitude);
            Locality.Set(context, outLocality);
            Longitude.Set(context, outLongitude);
            PhoneCountryDialingCode.Set(context, outPhoneCountryDialingCode);
            PhoneInternationalPrefix.Set(context, outPhoneInternationalPrefix);
            PhoneNationalDestinationCode.Set(context, outPhoneNationalDestinationCode);
            PhoneNationPrefix.Set(context, outPhoneNationPrefix);
            PhoneNumber.Set(context, outPhoneNumber);
            PhoneSubscriberNumber.Set(context, outPhoneSubscriberNumber);
            RecordID.Set(context, outRecordID);
            Results.Set(context, outResults);
            UTC.Set(context, outUTC);
            Error.Set(context, outError);
            TransmissionResults.Set(context, outTransmissionResults);
        }
    }
}
