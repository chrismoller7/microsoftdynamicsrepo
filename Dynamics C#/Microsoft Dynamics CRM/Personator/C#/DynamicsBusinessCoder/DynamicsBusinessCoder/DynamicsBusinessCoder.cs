﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace DynamicsBusinessCoder
{
    public class DynamicsBusinessCoder : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inColumns")]
        [RequiredArgument]
        public InArgument<String> Columns { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inCompanyName")]
        [Output("outCompanyName")]
        public InOutArgument<String> CompanyName { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            HttpWebRequest request;
            HttpWebResponse response;
            System.IO.Stream responseStream;
            System.IO.MemoryStream JSONStream;
            Uri address;
            String tServer;
            StreamReader streamReader;

            tServer = "https://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoderUS";
            String auth;
            auth = Uri.EscapeDataString(CustomerID.Get(context));
            tServer += "?lic=" + auth; //this sets the license string

            tServer += "&cols=";
            tServer += Columns.Get(context);

            tServer += "&opt=";
            tServer += Options.Get(context);

            tServer += "&t=" + "Microsoft Dynamics CRM Business Coder";

            if (!object.Equals(CompanyName.Get(context), ""))
            {
                tServer += "&comp=" + spaceRemover(CompanyName.Get(context));
            }

        }

        private String spaceRemover(String x)
        {
            String y = "";
            for (int i = 0; i < x.Length; i++)
            {
                if (x.ElementAt(i) == ' ')
                {
                    y += "+";
                }
                else
                {
                    y += x.ElementAt(i);
                }
            }
            return y;
        }
    }
}
