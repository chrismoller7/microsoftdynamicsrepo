﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessCoderJSONSampleCSharp
{
    class RequestSend
    {
        public string t { get; set; }
        public string id { get; set; }
        public string cols { get; set; }
        public string opt { get; set; }
        public RequestRecord[] Records { get; set; }
    }
}
