﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessCoderJSONSampleCSharp
{
    class RequestRecord
    {
        public string rec { get; set; }
        public string comp { get; set; }
        public string a1 { get; set; }
        public string a2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal { get; set; }
        public string ctry { get; set; }
        public string phone { get; set; }
        public string mak { get; set; }
        public string stock { get; set; }
        public string web { get; set; }
    }
}
