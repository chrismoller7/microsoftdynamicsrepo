namespace AddressObjectSample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblBuild;
        private System.Windows.Forms.Label lblDatabaseDate;
        private System.Windows.Forms.Label lblExpiration;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.Label lblInitError;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblParsePlus4;
        private System.Windows.Forms.Label lblParseZip;
        private System.Windows.Forms.Label lblParseGarbage;
        private System.Windows.Forms.Label lblParseSuitenumber;
        private System.Windows.Forms.Label lblParseSuffix;
        private System.Windows.Forms.Label lblParseSuitename;
        private System.Windows.Forms.Label lblParsePredir;
        private System.Windows.Forms.Label lblParseStreet;
        private System.Windows.Forms.Label lblParseState;
        private System.Windows.Forms.Label lblParseCity;
        private System.Windows.Forms.Label lblParseRange;
        private System.Windows.Forms.TextBox txtParseAddress;
        private System.Windows.Forms.Button btnReparse;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.Label lblParsePostdir;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btnFindStreet;
        private System.Windows.Forms.TextBox txtStreetStreet;
        private System.Windows.Forms.TextBox txtStreetZip;
        private System.Windows.Forms.Label lblStreetInitError;
        private System.Windows.Forms.Label lblStreetDatabaseDate;
        private System.Windows.Forms.Label lblStreetBuild;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Label State;
        private System.Windows.Forms.TextBox txtZipState;
        private System.Windows.Forms.TextBox txtZipCity;
        private System.Windows.Forms.Label Zip;
        private System.Windows.Forms.TextBox txtZipZip;
        private System.Windows.Forms.Button btnCityInState;
        private System.Windows.Forms.Button btnZipInCity;
        private System.Windows.Forms.Button btnFindZip;
        private System.Windows.Forms.TextBox txtZipResult;
        private System.Windows.Forms.Label lblZipDatabase;
        private System.Windows.Forms.Label lblZipBuild;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label Address2;
        private System.Windows.Forms.TextBox txtAddress2Out;
        private System.Windows.Forms.TextBox txtAddressOut;
        private System.Windows.Forms.TextBox txtStateOut;
        private System.Windows.Forms.TextBox txtCityOut;
        private System.Windows.Forms.TextBox txtZipOut;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnLastLine;
        private System.Windows.Forms.TextBox txtPlus4;
        private System.Windows.Forms.TextBox txtOther;
        private System.Windows.Forms.TextBox txtResultCodes;
        private System.Windows.Forms.Label lblSuite;
        private System.Windows.Forms.TextBox txtSuite;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtLastLine;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtCompanyOut;
        private System.Windows.Forms.Label lblZipErrorString;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtCompanyOut = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.lblSuite = new System.Windows.Forms.Label();
            this.txtSuite = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtResultCodes = new System.Windows.Forms.TextBox();
            this.txtOther = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPlus4 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtZipOut = new System.Windows.Forms.TextBox();
            this.txtStateOut = new System.Windows.Forms.TextBox();
            this.txtCityOut = new System.Windows.Forms.TextBox();
            this.txtAddress2Out = new System.Windows.Forms.TextBox();
            this.txtAddressOut = new System.Windows.Forms.TextBox();
            this.Address2 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.btnVerify = new System.Windows.Forms.Button();
            this.lblInitError = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblExpiration = new System.Windows.Forms.Label();
            this.lblDatabaseDate = new System.Windows.Forms.Label();
            this.lblBuild = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.txtState = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label30 = new System.Windows.Forms.Label();
            this.txtLastLine = new System.Windows.Forms.TextBox();
            this.lblParsePostdir = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btnLastLine = new System.Windows.Forms.Button();
            this.lblParsePlus4 = new System.Windows.Forms.Label();
            this.lblParseZip = new System.Windows.Forms.Label();
            this.lblParseGarbage = new System.Windows.Forms.Label();
            this.lblParseSuitenumber = new System.Windows.Forms.Label();
            this.lblParseSuffix = new System.Windows.Forms.Label();
            this.lblParseSuitename = new System.Windows.Forms.Label();
            this.lblParsePredir = new System.Windows.Forms.Label();
            this.lblParseStreet = new System.Windows.Forms.Label();
            this.lblParseState = new System.Windows.Forms.Label();
            this.lblParseCity = new System.Windows.Forms.Label();
            this.lblParseRange = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnReparse = new System.Windows.Forms.Button();
            this.btnParse = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.txtParseAddress = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnFindStreet = new System.Windows.Forms.Button();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStreetStreet = new System.Windows.Forms.TextBox();
            this.txtStreetZip = new System.Windows.Forms.TextBox();
            this.lblStreetInitError = new System.Windows.Forms.Label();
            this.lblStreetDatabaseDate = new System.Windows.Forms.Label();
            this.lblStreetBuild = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lblZipErrorString = new System.Windows.Forms.Label();
            this.btnFindZip = new System.Windows.Forms.Button();
            this.btnZipInCity = new System.Windows.Forms.Button();
            this.btnCityInState = new System.Windows.Forms.Button();
            this.txtZipResult = new System.Windows.Forms.TextBox();
            this.Zip = new System.Windows.Forms.Label();
            this.txtZipZip = new System.Windows.Forms.TextBox();
            this.lblZipDatabase = new System.Windows.Forms.Label();
            this.lblZipBuild = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.State = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtZipState = new System.Windows.Forms.TextBox();
            this.txtZipCity = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(8, 64);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(824, 383);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtLastName);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.txtCompanyOut);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.txtCompany);
            this.tabPage1.Controls.Add(this.lblSuite);
            this.tabPage1.Controls.Add(this.txtSuite);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtResultCodes);
            this.tabPage1.Controls.Add(this.txtOther);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtPlus4);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtZipOut);
            this.tabPage1.Controls.Add(this.txtStateOut);
            this.tabPage1.Controls.Add(this.txtCityOut);
            this.tabPage1.Controls.Add(this.txtAddress2Out);
            this.tabPage1.Controls.Add(this.txtAddressOut);
            this.tabPage1.Controls.Add(this.Address2);
            this.tabPage1.Controls.Add(this.txtAddress2);
            this.tabPage1.Controls.Add(this.btnVerify);
            this.tabPage1.Controls.Add(this.lblInitError);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.lblExpiration);
            this.tabPage1.Controls.Add(this.lblDatabaseDate);
            this.tabPage1.Controls.Add(this.lblBuild);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtCity);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtZip);
            this.tabPage1.Controls.Add(this.txtState);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(816, 357);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Address Check";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(72, 218);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(152, 20);
            this.txtLastName.TabIndex = 71;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(8, 221);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 13);
            this.label39.TabIndex = 70;
            this.label39.Text = "Last Name";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(352, 16);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(56, 16);
            this.label36.TabIndex = 69;
            this.label36.Text = "Company";
            // 
            // txtCompanyOut
            // 
            this.txtCompanyOut.Location = new System.Drawing.Point(408, 8);
            this.txtCompanyOut.Name = "txtCompanyOut";
            this.txtCompanyOut.Size = new System.Drawing.Size(280, 20);
            this.txtCompanyOut.TabIndex = 68;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(8, 96);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 16);
            this.label35.TabIndex = 67;
            this.label35.Text = "Company";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(72, 96);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(248, 20);
            this.txtCompany.TabIndex = 66;
            // 
            // lblSuite
            // 
            this.lblSuite.Location = new System.Drawing.Point(352, 88);
            this.lblSuite.Name = "lblSuite";
            this.lblSuite.Size = new System.Drawing.Size(48, 16);
            this.lblSuite.TabIndex = 65;
            this.lblSuite.Text = "Suite";
            // 
            // txtSuite
            // 
            this.txtSuite.Location = new System.Drawing.Point(408, 80);
            this.txtSuite.Name = "txtSuite";
            this.txtSuite.Size = new System.Drawing.Size(200, 20);
            this.txtSuite.TabIndex = 64;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(352, 160);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 63;
            this.label19.Text = "Results";
            // 
            // txtResultCodes
            // 
            this.txtResultCodes.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtResultCodes.Location = new System.Drawing.Point(408, 154);
            this.txtResultCodes.Multiline = true;
            this.txtResultCodes.Name = "txtResultCodes";
            this.txtResultCodes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultCodes.Size = new System.Drawing.Size(280, 45);
            this.txtResultCodes.TabIndex = 62;
            // 
            // txtOther
            // 
            this.txtOther.Location = new System.Drawing.Point(408, 221);
            this.txtOther.Multiline = true;
            this.txtOther.Name = "txtOther";
            this.txtOther.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOther.Size = new System.Drawing.Size(280, 128);
            this.txtOther.TabIndex = 59;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(586, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 16);
            this.label17.TabIndex = 58;
            this.label17.Text = "Plus4";
            // 
            // txtPlus4
            // 
            this.txtPlus4.Location = new System.Drawing.Point(632, 128);
            this.txtPlus4.Name = "txtPlus4";
            this.txtPlus4.Size = new System.Drawing.Size(56, 20);
            this.txtPlus4.TabIndex = 57;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(486, 136);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 16);
            this.label16.TabIndex = 56;
            this.label16.Text = "Zip";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(352, 202);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 55;
            this.label13.Text = "Other Data";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(352, 136);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.TabIndex = 54;
            this.label11.Text = "State";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(352, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 53;
            this.label12.Text = "City";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(352, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 48;
            this.label8.Text = "Address";
            // 
            // txtZipOut
            // 
            this.txtZipOut.Location = new System.Drawing.Point(516, 128);
            this.txtZipOut.Name = "txtZipOut";
            this.txtZipOut.Size = new System.Drawing.Size(64, 20);
            this.txtZipOut.TabIndex = 47;
            // 
            // txtStateOut
            // 
            this.txtStateOut.Location = new System.Drawing.Point(408, 128);
            this.txtStateOut.Name = "txtStateOut";
            this.txtStateOut.Size = new System.Drawing.Size(40, 20);
            this.txtStateOut.TabIndex = 46;
            // 
            // txtCityOut
            // 
            this.txtCityOut.Location = new System.Drawing.Point(408, 104);
            this.txtCityOut.Name = "txtCityOut";
            this.txtCityOut.Size = new System.Drawing.Size(200, 20);
            this.txtCityOut.TabIndex = 45;
            // 
            // txtAddress2Out
            // 
            this.txtAddress2Out.Location = new System.Drawing.Point(408, 56);
            this.txtAddress2Out.Name = "txtAddress2Out";
            this.txtAddress2Out.Size = new System.Drawing.Size(280, 20);
            this.txtAddress2Out.TabIndex = 44;
            // 
            // txtAddressOut
            // 
            this.txtAddressOut.Location = new System.Drawing.Point(408, 32);
            this.txtAddressOut.Name = "txtAddressOut";
            this.txtAddressOut.Size = new System.Drawing.Size(280, 20);
            this.txtAddressOut.TabIndex = 43;
            // 
            // Address2
            // 
            this.Address2.Location = new System.Drawing.Point(352, 64);
            this.Address2.Name = "Address2";
            this.Address2.Size = new System.Drawing.Size(56, 16);
            this.Address2.TabIndex = 42;
            this.Address2.Text = "Address2";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(72, 144);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(248, 20);
            this.txtAddress2.TabIndex = 41;
            // 
            // btnVerify
            // 
            this.btnVerify.Location = new System.Drawing.Point(72, 256);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(128, 32);
            this.btnVerify.TabIndex = 40;
            this.btnVerify.Text = "Verify Address";
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // lblInitError
            // 
            this.lblInitError.ForeColor = System.Drawing.Color.Blue;
            this.lblInitError.Location = new System.Drawing.Point(8, 56);
            this.lblInitError.Name = "lblInitError";
            this.lblInitError.Size = new System.Drawing.Size(320, 13);
            this.lblInitError.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(8, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 16);
            this.label15.TabIndex = 28;
            this.label15.Text = "Initialize Error String";
            // 
            // lblExpiration
            // 
            this.lblExpiration.ForeColor = System.Drawing.Color.Blue;
            this.lblExpiration.Location = new System.Drawing.Point(240, 24);
            this.lblExpiration.Name = "lblExpiration";
            this.lblExpiration.Size = new System.Drawing.Size(88, 16);
            this.lblExpiration.TabIndex = 25;
            // 
            // lblDatabaseDate
            // 
            this.lblDatabaseDate.ForeColor = System.Drawing.Color.Blue;
            this.lblDatabaseDate.Location = new System.Drawing.Point(144, 24);
            this.lblDatabaseDate.Name = "lblDatabaseDate";
            this.lblDatabaseDate.Size = new System.Drawing.Size(80, 13);
            this.lblDatabaseDate.TabIndex = 24;
            // 
            // lblBuild
            // 
            this.lblBuild.ForeColor = System.Drawing.Color.Blue;
            this.lblBuild.Location = new System.Drawing.Point(8, 24);
            this.lblBuild.Name = "lblBuild";
            this.lblBuild.Size = new System.Drawing.Size(112, 16);
            this.lblBuild.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Build";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(144, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Database Date";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(240, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Expiration Date";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(72, 168);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(168, 20);
            this.txtCity.TabIndex = 1;
            this.txtCity.Text = "Las Vegas";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(112, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Zip";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 24);
            this.label2.TabIndex = 8;
            this.label2.Text = "City";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "State";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Address";
            // 
            // txtZip
            // 
            this.txtZip.Location = new System.Drawing.Point(144, 192);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(80, 20);
            this.txtZip.TabIndex = 3;
            this.txtZip.Text = "92688";
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(72, 192);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(32, 20);
            this.txtState.TabIndex = 2;
            this.txtState.Text = "NV";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(72, 120);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(248, 20);
            this.txtAddress.TabIndex = 0;
            this.txtAddress.Text = "201 10th St";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtLastLine);
            this.tabPage2.Controls.Add(this.lblParsePostdir);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.btnLastLine);
            this.tabPage2.Controls.Add(this.lblParsePlus4);
            this.tabPage2.Controls.Add(this.lblParseZip);
            this.tabPage2.Controls.Add(this.lblParseGarbage);
            this.tabPage2.Controls.Add(this.lblParseSuitenumber);
            this.tabPage2.Controls.Add(this.lblParseSuffix);
            this.tabPage2.Controls.Add(this.lblParseSuitename);
            this.tabPage2.Controls.Add(this.lblParsePredir);
            this.tabPage2.Controls.Add(this.lblParseStreet);
            this.tabPage2.Controls.Add(this.lblParseState);
            this.tabPage2.Controls.Add(this.lblParseCity);
            this.tabPage2.Controls.Add(this.lblParseRange);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.btnReparse);
            this.tabPage2.Controls.Add(this.btnParse);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.txtParseAddress);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(816, 357);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Parse";
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(456, 24);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 16);
            this.label30.TabIndex = 101;
            this.label30.Text = "Last Line";
            // 
            // txtLastLine
            // 
            this.txtLastLine.Location = new System.Drawing.Point(520, 24);
            this.txtLastLine.Name = "txtLastLine";
            this.txtLastLine.Size = new System.Drawing.Size(280, 20);
            this.txtLastLine.TabIndex = 100;
            this.txtLastLine.Text = "Rancho Santa Margarita, CA 92688";
            // 
            // lblParsePostdir
            // 
            this.lblParsePostdir.ForeColor = System.Drawing.Color.Blue;
            this.lblParsePostdir.Location = new System.Drawing.Point(88, 112);
            this.lblParsePostdir.Name = "lblParsePostdir";
            this.lblParsePostdir.Size = new System.Drawing.Size(72, 16);
            this.lblParsePostdir.TabIndex = 99;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(16, 112);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 16);
            this.label23.TabIndex = 98;
            this.label23.Text = "PostDir";
            // 
            // btnLastLine
            // 
            this.btnLastLine.Location = new System.Drawing.Point(496, 192);
            this.btnLastLine.Name = "btnLastLine";
            this.btnLastLine.Size = new System.Drawing.Size(96, 24);
            this.btnLastLine.TabIndex = 97;
            this.btnLastLine.Text = "Parse LastLine";
            this.btnLastLine.Click += new System.EventHandler(this.btnLastLine_Click);
            // 
            // lblParsePlus4
            // 
            this.lblParsePlus4.ForeColor = System.Drawing.Color.Blue;
            this.lblParsePlus4.Location = new System.Drawing.Point(528, 136);
            this.lblParsePlus4.Name = "lblParsePlus4";
            this.lblParsePlus4.Size = new System.Drawing.Size(232, 16);
            this.lblParsePlus4.TabIndex = 96;
            // 
            // lblParseZip
            // 
            this.lblParseZip.ForeColor = System.Drawing.Color.Blue;
            this.lblParseZip.Location = new System.Drawing.Point(528, 112);
            this.lblParseZip.Name = "lblParseZip";
            this.lblParseZip.Size = new System.Drawing.Size(232, 16);
            this.lblParseZip.TabIndex = 95;
            // 
            // lblParseGarbage
            // 
            this.lblParseGarbage.ForeColor = System.Drawing.Color.Blue;
            this.lblParseGarbage.Location = new System.Drawing.Point(264, 136);
            this.lblParseGarbage.Name = "lblParseGarbage";
            this.lblParseGarbage.Size = new System.Drawing.Size(184, 16);
            this.lblParseGarbage.TabIndex = 94;
            // 
            // lblParseSuitenumber
            // 
            this.lblParseSuitenumber.ForeColor = System.Drawing.Color.Blue;
            this.lblParseSuitenumber.Location = new System.Drawing.Point(264, 112);
            this.lblParseSuitenumber.Name = "lblParseSuitenumber";
            this.lblParseSuitenumber.Size = new System.Drawing.Size(184, 16);
            this.lblParseSuitenumber.TabIndex = 92;
            // 
            // lblParseSuffix
            // 
            this.lblParseSuffix.ForeColor = System.Drawing.Color.Blue;
            this.lblParseSuffix.Location = new System.Drawing.Point(88, 136);
            this.lblParseSuffix.Name = "lblParseSuffix";
            this.lblParseSuffix.Size = new System.Drawing.Size(72, 16);
            this.lblParseSuffix.TabIndex = 91;
            // 
            // lblParseSuitename
            // 
            this.lblParseSuitename.ForeColor = System.Drawing.Color.Blue;
            this.lblParseSuitename.Location = new System.Drawing.Point(264, 88);
            this.lblParseSuitename.Name = "lblParseSuitename";
            this.lblParseSuitename.Size = new System.Drawing.Size(184, 16);
            this.lblParseSuitename.TabIndex = 90;
            // 
            // lblParsePredir
            // 
            this.lblParsePredir.ForeColor = System.Drawing.Color.Blue;
            this.lblParsePredir.Location = new System.Drawing.Point(88, 88);
            this.lblParsePredir.Name = "lblParsePredir";
            this.lblParsePredir.Size = new System.Drawing.Size(72, 16);
            this.lblParsePredir.TabIndex = 89;
            // 
            // lblParseStreet
            // 
            this.lblParseStreet.ForeColor = System.Drawing.Color.Blue;
            this.lblParseStreet.Location = new System.Drawing.Point(264, 64);
            this.lblParseStreet.Name = "lblParseStreet";
            this.lblParseStreet.Size = new System.Drawing.Size(184, 16);
            this.lblParseStreet.TabIndex = 88;
            // 
            // lblParseState
            // 
            this.lblParseState.ForeColor = System.Drawing.Color.Blue;
            this.lblParseState.Location = new System.Drawing.Point(528, 64);
            this.lblParseState.Name = "lblParseState";
            this.lblParseState.Size = new System.Drawing.Size(232, 16);
            this.lblParseState.TabIndex = 87;
            // 
            // lblParseCity
            // 
            this.lblParseCity.ForeColor = System.Drawing.Color.Blue;
            this.lblParseCity.Location = new System.Drawing.Point(528, 88);
            this.lblParseCity.Name = "lblParseCity";
            this.lblParseCity.Size = new System.Drawing.Size(232, 16);
            this.lblParseCity.TabIndex = 86;
            // 
            // lblParseRange
            // 
            this.lblParseRange.ForeColor = System.Drawing.Color.Blue;
            this.lblParseRange.Location = new System.Drawing.Point(88, 64);
            this.lblParseRange.Name = "lblParseRange";
            this.lblParseRange.Size = new System.Drawing.Size(72, 16);
            this.lblParseRange.TabIndex = 85;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(176, 112);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 16);
            this.label22.TabIndex = 83;
            this.label22.Text = "Suite Number";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(464, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 16);
            this.label21.TabIndex = 82;
            this.label21.Text = "State";
            // 
            // btnReparse
            // 
            this.btnReparse.Location = new System.Drawing.Point(376, 192);
            this.btnReparse.Name = "btnReparse";
            this.btnReparse.Size = new System.Drawing.Size(96, 24);
            this.btnReparse.TabIndex = 80;
            this.btnReparse.Text = "Reparse";
            this.btnReparse.Click += new System.EventHandler(this.btnReparse_Click);
            // 
            // btnParse
            // 
            this.btnParse.Location = new System.Drawing.Point(256, 192);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(96, 24);
            this.btnParse.TabIndex = 79;
            this.btnParse.Text = "Parse";
            this.btnParse.Click += new System.EventHandler(this.btnParse_Click);
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(464, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 16);
            this.label25.TabIndex = 73;
            this.label25.Text = "City";
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(176, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 16);
            this.label26.TabIndex = 72;
            this.label26.Text = "Street";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(176, 136);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 16);
            this.label27.TabIndex = 71;
            this.label27.Text = "Garbage";
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(176, 88);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(72, 16);
            this.label28.TabIndex = 70;
            this.label28.Text = "Suite Name";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(464, 136);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(40, 16);
            this.label37.TabIndex = 60;
            this.label37.Text = "Plus4";
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(16, 88);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(40, 16);
            this.label41.TabIndex = 56;
            this.label41.Text = "PreDir";
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(464, 112);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 16);
            this.label42.TabIndex = 55;
            this.label42.Text = "Zip";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(16, 136);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 16);
            this.label43.TabIndex = 54;
            this.label43.Text = "Suffix";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(16, 64);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(64, 16);
            this.label44.TabIndex = 53;
            this.label44.Text = "Range";
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(16, 27);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(48, 16);
            this.label52.TabIndex = 45;
            this.label52.Text = "Address";
            // 
            // txtParseAddress
            // 
            this.txtParseAddress.Location = new System.Drawing.Point(72, 24);
            this.txtParseAddress.Name = "txtParseAddress";
            this.txtParseAddress.Size = new System.Drawing.Size(336, 20);
            this.txtParseAddress.TabIndex = 41;
            this.txtParseAddress.Text = "22382 Avenida Empresa";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnFindStreet);
            this.tabPage3.Controls.Add(this.txtResults);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.txtStreetStreet);
            this.tabPage3.Controls.Add(this.txtStreetZip);
            this.tabPage3.Controls.Add(this.lblStreetInitError);
            this.tabPage3.Controls.Add(this.lblStreetDatabaseDate);
            this.tabPage3.Controls.Add(this.lblStreetBuild);
            this.tabPage3.Controls.Add(this.label57);
            this.tabPage3.Controls.Add(this.label59);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(816, 357);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Street";
            // 
            // btnFindStreet
            // 
            this.btnFindStreet.Location = new System.Drawing.Point(624, 40);
            this.btnFindStreet.Name = "btnFindStreet";
            this.btnFindStreet.Size = new System.Drawing.Size(176, 40);
            this.btnFindStreet.TabIndex = 84;
            this.btnFindStreet.Text = "Find Street";
            this.btnFindStreet.Click += new System.EventHandler(this.btnFindStreet_Click);
            // 
            // txtResults
            // 
            this.txtResults.Location = new System.Drawing.Point(8, 96);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResults.Size = new System.Drawing.Size(792, 216);
            this.txtResults.TabIndex = 82;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(200, 64);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(48, 16);
            this.label31.TabIndex = 81;
            this.label31.Text = "Street";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(480, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 80;
            this.label14.Text = "Zip";
            // 
            // txtStreetStreet
            // 
            this.txtStreetStreet.Location = new System.Drawing.Point(256, 56);
            this.txtStreetStreet.Name = "txtStreetStreet";
            this.txtStreetStreet.Size = new System.Drawing.Size(192, 20);
            this.txtStreetStreet.TabIndex = 79;
            this.txtStreetStreet.Text = "Main";
            // 
            // txtStreetZip
            // 
            this.txtStreetZip.Location = new System.Drawing.Point(536, 56);
            this.txtStreetZip.Name = "txtStreetZip";
            this.txtStreetZip.Size = new System.Drawing.Size(64, 20);
            this.txtStreetZip.TabIndex = 78;
            this.txtStreetZip.Text = "89101";
            // 
            // lblStreetInitError
            // 
            this.lblStreetInitError.ForeColor = System.Drawing.Color.Blue;
            this.lblStreetInitError.Location = new System.Drawing.Point(208, 24);
            this.lblStreetInitError.Name = "lblStreetInitError";
            this.lblStreetInitError.Size = new System.Drawing.Size(264, 16);
            this.lblStreetInitError.TabIndex = 77;
            // 
            // lblStreetDatabaseDate
            // 
            this.lblStreetDatabaseDate.ForeColor = System.Drawing.Color.Blue;
            this.lblStreetDatabaseDate.Location = new System.Drawing.Point(112, 24);
            this.lblStreetDatabaseDate.Name = "lblStreetDatabaseDate";
            this.lblStreetDatabaseDate.Size = new System.Drawing.Size(80, 13);
            this.lblStreetDatabaseDate.TabIndex = 69;
            // 
            // lblStreetBuild
            // 
            this.lblStreetBuild.ForeColor = System.Drawing.Color.Blue;
            this.lblStreetBuild.Location = new System.Drawing.Point(8, 24);
            this.lblStreetBuild.Name = "lblStreetBuild";
            this.lblStreetBuild.Size = new System.Drawing.Size(88, 16);
            this.lblStreetBuild.TabIndex = 68;
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(8, 8);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(48, 16);
            this.label57.TabIndex = 67;
            this.label57.Text = "Build";
            // 
            // label59
            // 
            this.label59.Location = new System.Drawing.Point(104, 8);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(88, 16);
            this.label59.TabIndex = 66;
            this.label59.Text = "Database Date";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(200, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(128, 16);
            this.label20.TabIndex = 58;
            this.label20.Text = "Initialize Error String";
            // 
            // label24
            // 
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(200, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(264, 16);
            this.label24.TabIndex = 77;
            // 
            // label29
            // 
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(104, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(80, 13);
            this.label29.TabIndex = 69;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lblZipErrorString);
            this.tabPage4.Controls.Add(this.btnFindZip);
            this.tabPage4.Controls.Add(this.btnZipInCity);
            this.tabPage4.Controls.Add(this.btnCityInState);
            this.tabPage4.Controls.Add(this.txtZipResult);
            this.tabPage4.Controls.Add(this.Zip);
            this.tabPage4.Controls.Add(this.txtZipZip);
            this.tabPage4.Controls.Add(this.lblZipDatabase);
            this.tabPage4.Controls.Add(this.lblZipBuild);
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.label34);
            this.tabPage4.Controls.Add(this.label38);
            this.tabPage4.Controls.Add(this.State);
            this.tabPage4.Controls.Add(this.label68);
            this.tabPage4.Controls.Add(this.txtZipState);
            this.tabPage4.Controls.Add(this.txtZipCity);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(816, 357);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Zip";
            // 
            // lblZipErrorString
            // 
            this.lblZipErrorString.ForeColor = System.Drawing.Color.Blue;
            this.lblZipErrorString.Location = new System.Drawing.Point(480, 56);
            this.lblZipErrorString.Name = "lblZipErrorString";
            this.lblZipErrorString.Size = new System.Drawing.Size(304, 24);
            this.lblZipErrorString.TabIndex = 128;
            // 
            // btnFindZip
            // 
            this.btnFindZip.Location = new System.Drawing.Point(576, 240);
            this.btnFindZip.Name = "btnFindZip";
            this.btnFindZip.Size = new System.Drawing.Size(96, 32);
            this.btnFindZip.TabIndex = 127;
            this.btnFindZip.Text = "Find Zip";
            this.btnFindZip.Click += new System.EventHandler(this.btnFindZip_Click);
            // 
            // btnZipInCity
            // 
            this.btnZipInCity.Location = new System.Drawing.Point(360, 240);
            this.btnZipInCity.Name = "btnZipInCity";
            this.btnZipInCity.Size = new System.Drawing.Size(96, 32);
            this.btnZipInCity.TabIndex = 126;
            this.btnZipInCity.Text = "Zip in City";
            this.btnZipInCity.Click += new System.EventHandler(this.btnZipInCity_Click);
            // 
            // btnCityInState
            // 
            this.btnCityInState.Location = new System.Drawing.Point(160, 240);
            this.btnCityInState.Name = "btnCityInState";
            this.btnCityInState.Size = new System.Drawing.Size(96, 32);
            this.btnCityInState.TabIndex = 125;
            this.btnCityInState.Text = "City in State";
            this.btnCityInState.Click += new System.EventHandler(this.btnCityInState_Click);
            // 
            // txtZipResult
            // 
            this.txtZipResult.Location = new System.Drawing.Point(12, 88);
            this.txtZipResult.Multiline = true;
            this.txtZipResult.Name = "txtZipResult";
            this.txtZipResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtZipResult.Size = new System.Drawing.Size(780, 144);
            this.txtZipResult.TabIndex = 124;
            // 
            // Zip
            // 
            this.Zip.Location = new System.Drawing.Point(144, 48);
            this.Zip.Name = "Zip";
            this.Zip.Size = new System.Drawing.Size(40, 16);
            this.Zip.TabIndex = 123;
            this.Zip.Text = "Zip";
            // 
            // txtZipZip
            // 
            this.txtZipZip.Location = new System.Drawing.Point(200, 48);
            this.txtZipZip.Name = "txtZipZip";
            this.txtZipZip.Size = new System.Drawing.Size(48, 20);
            this.txtZipZip.TabIndex = 122;
            this.txtZipZip.Text = "89101";
            // 
            // lblZipDatabase
            // 
            this.lblZipDatabase.ForeColor = System.Drawing.Color.Blue;
            this.lblZipDatabase.Location = new System.Drawing.Point(696, 24);
            this.lblZipDatabase.Name = "lblZipDatabase";
            this.lblZipDatabase.Size = new System.Drawing.Size(96, 13);
            this.lblZipDatabase.TabIndex = 121;
            // 
            // lblZipBuild
            // 
            this.lblZipBuild.ForeColor = System.Drawing.Color.Blue;
            this.lblZipBuild.Location = new System.Drawing.Point(480, 24);
            this.lblZipBuild.Name = "lblZipBuild";
            this.lblZipBuild.Size = new System.Drawing.Size(120, 16);
            this.lblZipBuild.TabIndex = 120;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(480, 8);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 16);
            this.label33.TabIndex = 119;
            this.label33.Text = "Build";
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(696, 8);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(88, 16);
            this.label34.TabIndex = 118;
            this.label34.Text = "Database Date";
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(480, 40);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(128, 16);
            this.label38.TabIndex = 117;
            this.label38.Text = "Initialize Error String";
            // 
            // State
            // 
            this.State.Location = new System.Drawing.Point(16, 48);
            this.State.Name = "State";
            this.State.Size = new System.Drawing.Size(40, 16);
            this.State.TabIndex = 98;
            this.State.Text = "State";
            // 
            // label68
            // 
            this.label68.Location = new System.Drawing.Point(16, 16);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(48, 16);
            this.label68.TabIndex = 97;
            this.label68.Text = "City";
            // 
            // txtZipState
            // 
            this.txtZipState.Location = new System.Drawing.Point(72, 48);
            this.txtZipState.Name = "txtZipState";
            this.txtZipState.Size = new System.Drawing.Size(48, 20);
            this.txtZipState.TabIndex = 96;
            this.txtZipState.Text = "NV";
            // 
            // txtZipCity
            // 
            this.txtZipCity.Location = new System.Drawing.Point(72, 16);
            this.txtZipCity.Name = "txtZipCity";
            this.txtZipCity.Size = new System.Drawing.Size(240, 20);
            this.txtZipCity.TabIndex = 95;
            this.txtZipCity.Text = "Las Vegas";
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.DarkBlue;
            this.label32.Location = new System.Drawing.Point(256, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(328, 23);
            this.label32.TabIndex = 8;
            this.label32.Text = "Melissa Data Address Check Demo";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(840, 452);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Melissa Data\'s C# Address Object Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label19;
    }
}

