
//********************************************************************************
//If you should have any comments, suggestions or improvements to these samples, 
//we welcome you to contact us at SampleCode@melissadata.com also please visit our 
//developerís bulletin board at forum.melissadata.com.
//********************************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace AddressObjectSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // ********************** DATA FILE PATH  ***********************
        //  File location path is set to the default Data File location. 
        //  Change this value if you installed the data files to a       
        //  different folder.                                            
        //  The Data Files Directory must contain the following files:   
        //  mdAddr.dat, mdAddr.lic, mdAddr.nat, and mdAddr.str
        // **************************************************************
        public string DataPathLocation = @"C:\Program Files\Melissa Data\DQT\Data";

        // ********************** LICENSE STRINGS ***********************
        //       To unlock the full function ality of Address Object,   
        //   please call a sales representative at 1-800-MELISSA ext. 3 
        //           (1-800-635-4772 x3) for a license string.          
        //        Without a valid license string, AddressCheck will     
        //                  only verify Nevada addresses.               
        //              REPLACE "DEMO" with LICENSE STRING                    

        //   SetLicenseString will also check for a valid license in the 
        //   MDADDR_LICENSE(Environment) variable. This allows you to  
        //   modify the license without recompiling the project
        // **************************************************************
        public string LicenseString = "DEMO";

        //Address Object Interfaces
        MelissaData.mdAddr addrobj = new MelissaData.mdAddr();
        MelissaData.mdParse parseObj = new MelissaData.mdParse();
        MelissaData.mdStreet streetObj = new MelissaData.mdStreet();
        MelissaData.mdZip zipObj = new MelissaData.mdZip();

         
        

        #region Initialize Objects
        private void Form1_Load(object sender, System.EventArgs e)
        {
            //****Initialize Address Object Address Check Interface****//
            addrobj.SetLicenseString(LicenseString);

            //Set Path To Data Files
            addrobj.SetPathToUSFiles(DataPathLocation);

            //CASS required add-ons for highest level of validation. Should be used by non-demo users.
            //addrobj.SetPathToDPVDataFiles(DataPathLocation);			
            //addrobj.SetPathToLACSLinkDataFiles(DataPathLocation); 
            //addrobj.SetPathToSuiteLinkDataFiles(DataPathLocation);

            //addrobj.SetPathToRBDIFiles(DataPathLocation);             //Delivery Indicator Add-on (Residence or Business)
            //addrobj.SetPathToCanadaFiles(DataPathLocation);           //Canadian Address Add-on
            //addrobj.SetPathToSuiteFinderDataFiles(DataPathLocation);  //AddressPlus Add-on (appends residential suites)

            if (addrobj.InitializeDataFiles() != 0)
            {
                //Initialization failed
                lblInitError.Text = addrobj.GetInitializeErrorString();
                btnVerify.Enabled = false;
            }
            else
            {
                //Initialized
                btnVerify.Enabled = true;
                lblBuild.Text = addrobj.GetBuildNumber();
                lblDatabaseDate.Text = addrobj.GetDatabaseDate();
                lblExpiration.Text = addrobj.GetExpirationDate();
            }



            //****Initialize Address Object Parse  Interface****//
            parseObj.Initialize(DataPathLocation);


            //****Initialize Address Object Street Data Interface****//
            streetObj.SetLicenseString(LicenseString);
            if (streetObj.Initialize(DataPathLocation, DataPathLocation, "") != 0)
            {
                // Initialize failed
                lblStreetInitError.Text = streetObj.GetInitializeErrorString();
                btnFindStreet.Enabled = false;
            }
            else
            {
                btnFindStreet.Enabled = true;
                lblStreetBuild.Text = streetObj.GetBuildNumber();
                lblStreetDatabaseDate.Text = streetObj.GetDatabaseDate();
                lblStreetInitError.Text = streetObj.GetInitializeErrorString();
            }




            //****Initialize Address Object Zip Data Interface****//
            zipObj.SetLicenseString(LicenseString);
            if (zipObj.Initialize(DataPathLocation, DataPathLocation, "") != 0)
            {
                // Initialize failed
                lblZipErrorString.Text = zipObj.GetInitializeErrorString();
                btnCityInState.Enabled = false;
                btnZipInCity.Enabled = false;
                btnFindZip.Enabled = false;
            }
            else
            {
                // Initialize successful
                lblZipBuild.Text = zipObj.GetBuildNumber();
                lblZipDatabase.Text = zipObj.GetDatabaseDate();
                lblZipErrorString.Text = zipObj.GetInitializeErrorString();
                btnCityInState.Enabled = true;
                btnZipInCity.Enabled = true;
                btnFindZip.Enabled = true;
            }
        }
        #endregion

        #region Address Check
        private void btnVerify_Click(object sender, System.EventArgs e)
        {
            //clear any previous results
            AddressClear();

            //set input address
            addrobj.ClearProperties();
            addrobj.SetCompany(txtCompany.Text);
            addrobj.SetAddress(txtAddress.Text);
            addrobj.SetAddress2(txtAddress2.Text);
            addrobj.SetCity(txtCity.Text);
            addrobj.SetState(txtState.Text);
            addrobj.SetZip(txtZip.Text);
            addrobj.SetLastName(txtLastName.Text);

            //verify address
            addrobj.VerifyAddress();

            //set results
            txtCompanyOut.Text = addrobj.GetCompany();
            txtAddressOut.Text = addrobj.GetAddress();
            txtAddress2Out.Text = addrobj.GetAddress2();
            txtSuite.Text = addrobj.GetSuite();
            txtCityOut.Text = addrobj.GetCity();
            txtStateOut.Text = addrobj.GetState();
            txtZipOut.Text = addrobj.GetZip();
            txtPlus4.Text = addrobj.GetPlus4();

            // Get the Result Codes
            String ResultsString = addrobj.GetResults();
            String ResultsOut = "";
            //if (ResultsString.Contains("AS01") || ResultsString.Contains("AS02"))
            if(ResultsString.Contains("AS"))
            {
                // Address was verified
                if (ResultsString.Contains("AS01"))
                    ResultsOut += "AS01: Full Address Matched to Postal Database and is deliverable\r\n";
                if (ResultsString.Contains("AS02"))
                    ResultsOut += "AS02: Address Matched to Postal Database, but suite missing or invalid\r\n";
                if (ResultsString.Contains("AS03"))
                    ResultsOut += "AS03: Valid Physical Address, not Serviced by the USPS \r\n";
                if (ResultsString.Contains("AS09"))
                    ResultsOut += "AS09: Foreign Postal Code Detected \r\n";
                if (ResultsString.Contains("AS10"))
                    ResultsOut += "AS10: Address Matched to CMRA\r\n";
                if (ResultsString.Contains("AS13"))
                    ResultsOut += "AS13: Address has been Updated by LACSLink \r\n";
                if (ResultsString.Contains("AS14"))
                    ResultsOut += "AS14: Suite Appended by SuiteLink \r\n";
                if (ResultsString.Contains("AS15"))
                    ResultsOut += "AS15: Suite Appended by SuiteFinder \r\n";
                if (ResultsString.Contains("AS16"))
                    ResultsOut += "AS16: Address is vacant.\r\n";
                if (ResultsString.Contains("AS17"))
                    ResultsOut += "AS17: Alternate delivery.\r\n";
                if (ResultsString.Contains("AS18"))
                    ResultsOut += "AS18: Artificially created adresses detected,DPV processing terminated at this point\r\n";
                if (ResultsString.Contains("AS20"))
                    ResultsOut += "AS20: Address Deliverable by USPS only \r\n";
                if (ResultsString.IndexOf("AS21") != -1)
                    ResultsOut += "AS21: Alternate Address Suggestion Found\r\n";
                if (ResultsString.IndexOf("AS22") != -1)
                    ResultsOut += "AS22: No Alternate Address Suggestion Found\r\n";
                if (ResultsString.IndexOf("AS23") != -1)
                    ResultsOut += "AS23: Extraneous information found \r\n";   
                 
                txtResultCodes.ForeColor = Color.Green;
            }
            if (ResultsString.Contains("AE"))
            {
                // there was an error in verifying the address
                if (ResultsString.Contains("AE01"))
                    ResultsOut += "AE01: Zip Code Error \r\n";
                if (ResultsString.Contains("AE02"))
                    ResultsOut += "AE02: Unknown Street Error \r\n";
                if (ResultsString.Contains("AE03"))
                    ResultsOut += "AE03: Component Mismatch Error \r\n";
                if (ResultsString.Contains("AE04"))
                    ResultsOut += "AE04: Non-Deliverable Address Error \r\n";
                if (ResultsString.Contains("AE05"))
                    ResultsOut += "AE05: Multiple Match Error \r\n";
                if (ResultsString.Contains("AE06"))
                    ResultsOut += "AE06: Early Warning System Error \r\n";
                if (ResultsString.Contains("AE07"))
                    ResultsOut += "AE07: Missing Minimum Address Input \r\n";
                if (ResultsString.Contains("AE08"))
                    ResultsOut += "AE08: Suite Range Invalid Error\r\n";
                if (ResultsString.Contains("AE09"))
                    ResultsOut += "AE09: Suite Range Missing Error \r\n";
                if (ResultsString.Contains("AE10"))
                    ResultsOut += "AE10: Primary Range Invalid Error \r\n";
                if (ResultsString.Contains("AE11"))
                    ResultsOut += "AE11: Primary Range Missing Error \r\n";
                if (ResultsString.Contains("AE12"))
                    ResultsOut += "AE12: PO, HC, or RR Box Number Invalid \r\n";
                if (ResultsString.Contains("AE13"))
                    ResultsOut += "AE13: PO, HC, or RR Box Number Missing \r\n";
                if (ResultsString.Contains("AE14"))
                    ResultsOut += "AE14: CMRA Secondary Missing Error\r\n";

                // program can not attempt address lookup
                if (ResultsString.Contains("AE15"))
                    ResultsOut += "AE15: Demo Mode \r\n";
                if (ResultsString.Contains("AE16"))
                    ResultsOut += "AE16: Expired Database\r\n";

                if (ResultsString.IndexOf("AE17") != -1)
                    ResultsOut += "AE17: Unnecessary Suite Error \r\n";
                if (ResultsString.IndexOf("AE19") != -1)
                    ResultsOut += "AE19: Max time for FindSuggestion exceeded \r\n";
                if (ResultsString.IndexOf("AE20") != -1)
                    ResultsOut += "AE20: FindSuggestion cannot be used\r\n";

                txtResultCodes.ForeColor = Color.Red;
            }

            // a change was made to the address
            if (ResultsString.IndexOf("AC01") != -1)
                ResultsOut += "AC01: ZIP Code Change \r\n";
            if (ResultsString.IndexOf("AC02") != -1)
                ResultsOut += "AC02: State Change\r\n";
            if (ResultsString.IndexOf("AC03") != -1)
                ResultsOut += "AC03: City Change \r\n";
            if (ResultsString.IndexOf("AC04") != -1)
                ResultsOut += "AC04: Base/Alternate Change \r\n";
            if (ResultsString.IndexOf("AC05") != -1)
                ResultsOut += "AC05: Alias Name Change \r\n";
            if (ResultsString.IndexOf("AC06") != -1)
                ResultsOut += "AC06: Address1/Address2 Swap\r\n";
            if (ResultsString.IndexOf("AC07") != -1)
                ResultsOut += "AC07: Address1/Company Swap \r\n";
            if (ResultsString.IndexOf("AC08") != -1)
                ResultsOut += "AC08: Plus4 Change\r\n";
            if (ResultsString.IndexOf("AC09") != -1)
                ResultsOut += "AC09: Urbanization Change \r\n";
            if (ResultsString.IndexOf("AC10") != -1)
                ResultsOut += "AC10: Street Name Change \r\n";
            if (ResultsString.IndexOf("AC11") != -1)
                ResultsOut += "AC11: Street Suffix Change\r\n";
            if (ResultsString.IndexOf("AC12") != -1)
                ResultsOut += "AC12: Street Directional Change \r\n";
            if (ResultsString.IndexOf("AC13") != -1)
                ResultsOut += "AC13: Suite Name Change\r\n";

            txtResultCodes.Text = ResultsOut;

            string other = "";
            other += "Carrier Route: " + addrobj.GetCarrierRoute() + "\r\n";
            other += "Delivery Point Code: " + addrobj.GetDeliveryPointCode() + "\r\n";
            other += "Delivery Point CheckDigit: " + addrobj.GetDeliveryPointCheckDigit() + "\r\n";
            other += "DPV Footnotes: " + addrobj.GetDPVFootnotes() + "\r\n";
            other += "\r\n";
            other += "Address Type Code: " + addrobj.GetAddressTypeCode() + "\r\n";
            other += "Address Type String: " + addrobj.GetAddressTypeString() + "\r\n";
            other += "City Abbreviation: " + addrobj.GetCityAbbreviation() + "\r\n";
            other += "County Name: " + addrobj.GetCountyName() + "\r\n";
            other += "County Fips: " + addrobj.GetCountyFips() + "\r\n";
            other += "Country Code: " + addrobj.GetCountryCode() + "\r\n";
            other += "Congressional District: " + addrobj.GetCongressionalDistrict() + "\r\n";
            other += "Time Zone: " + addrobj.GetTimeZone() + "\r\n";
            other += "Time Zone Code: " + addrobj.GetTimeZoneCode() + "\r\n";
            other += "Urbanization: " + addrobj.GetUrbanization() + "\r\n";
            other += "Zip Type: " + addrobj.GetZipType();
            other += "\r\n";
            other += "MSA: " + addrobj.GetMsa() + "\r\n";
            other += "PMSA: " + addrobj.GetPmsa() + "\r\n";
            other += "Private Mailbox: " + addrobj.GetPrivateMailbox() + "\r\n";
            other += "\r\n";
            other += "Parsed Address Range: " + addrobj.GetParsedAddressRange() + "\r\n";
            other += "Parsed Pre Direction: " + addrobj.GetParsedPreDirection() + "\r\n";
            other += "Parsed Street Name: " + addrobj.GetParsedStreetName() + "\r\n";
            other += "Parsed Suffix: " + addrobj.GetParsedSuffix() + "\r\n";
            other += "Parsed Post Direction: " + addrobj.GetParsedPostDirection() + "\r\n";
            other += "Parsed Suite Name: " + addrobj.GetParsedSuiteName() + "\r\n";
            other += "Parsed Suite Range: " + addrobj.GetParsedSuiteRange() + "\r\n";
            other += "Parsed Garbage: " + addrobj.GetParsedGarbage() + "\r\n";
            other += "\r\n";
            other += "ELot Order: " + addrobj.GetELotOrder() + "\r\n";
            other += "ELot Number: " + addrobj.GetELotNumber() + "\r\n";
            other += "LACS: " + addrobj.GetLACS() + "\r\n";
            other += "LACSLinkIndicator: " + addrobj.GetLACSLinkIndicator() + "\r\n";
            other += "LACSLinkReturnCode: " + addrobj.GetLACSLinkReturnCode() + "\r\n";
            other += "EWS: " + addrobj.GetEWSFlag() + "\r\n";
            other += "RBDI: " + addrobj.GetRBDI() + "\r\n";
            other += "Suite Link Return Code: " + addrobj.GetSuiteLinkReturnCode() + "\r\n";
            
            txtOther.Text = other;
        }

        private void AddressClear()
        {
            txtAddressOut.Text = "";
            txtAddress2Out.Text = "";
            txtCityOut.Text = "";
            txtStateOut.Text = "";
            txtZipOut.Text = "";
            txtPlus4.Text = "";
            txtOther.Text = "";
        }
        #endregion

        #region Parse
        bool parseFlag = false;  //This is a flag for trying to reparse without initially calling parse
                                 //True if can reparse, false if not.
        private void btnParse_Click(object sender, System.EventArgs e)
        {
            parseObj.Parse(txtParseAddress.Text); 
            
                // parse successful
                lblParseGarbage.Text = parseObj.GetGarbage();
                lblParseSuitenumber.Text = parseObj.GetSuiteNumber();
                lblParseSuffix.Text = parseObj.GetSuffix();
                lblParseSuitename.Text = parseObj.GetSuiteName();
                lblParsePredir.Text = parseObj.GetPreDirection();
                lblParseStreet.Text = parseObj.GetStreetName();
                lblParseRange.Text = parseObj.GetRange();
                lblParsePostdir.Text = parseObj.GetPostDirection();

                parseFlag = true; //Can now reparse
            
            
        }

        private void btnReparse_Click(object sender, System.EventArgs e)
        {
            if (parseFlag == true)
            {
                if (parseObj.ParseNext() == true)
                {
                    //reparses avaliable
                    lblParseGarbage.Text = parseObj.GetGarbage();
                    lblParseSuitenumber.Text = parseObj.GetSuiteNumber();
                    lblParseSuffix.Text = parseObj.GetSuffix();
                    lblParseSuitename.Text = parseObj.GetSuiteName();
                    lblParsePredir.Text = parseObj.GetPreDirection();
                    lblParseStreet.Text = parseObj.GetStreetName();
                    lblParseRange.Text = parseObj.GetRange();
                    lblParsePostdir.Text = parseObj.GetPostDirection();
                }
            else parseFlag = false;
            }
        }

        private void btnLastLine_Click(object sender, System.EventArgs e)
        {
            parseObj.LastLineParse(txtLastLine.Text);
            lblParseState.Text = parseObj.GetState();
            lblParseCity.Text = parseObj.GetCity();
            lblParsePlus4.Text = parseObj.GetPlus4();
            lblParseZip.Text = parseObj.GetZip();
        }
        #endregion

        #region StreetData
        private void btnFindStreet_Click(object sender, System.EventArgs e)
        {
            txtResults.Text = "";

            if (streetObj.FindStreet(txtStreetStreet.Text, txtStreetZip.Text, false) == true)
            {
                string output = "";
                output += streetObj.GetPrimaryRangeLow() + "-";
                output += streetObj.GetPrimaryRangeHigh() + "      ";
                output += streetObj.GetPrimaryRangeOddEven() + "   ";
                output += streetObj.GetPreDirection() + "   ";
                output += streetObj.GetStreetName() + "   ";
                output += streetObj.GetSuffix() + "   ";
                output += streetObj.GetPostDirection() + "   ";
                if (streetObj.GetSuiteName() == "" && streetObj.GetSuiteRangeLow() == "")
                    output += "   ";
                else
                {
                    output += streetObj.GetSuiteName().Trim() + "  ";
                    output += streetObj.GetSuiteRangeLow().Trim() + "-";
                    output += streetObj.GetSuiteRangeHigh().Trim() + "  ";
                }
                output += streetObj.GetZip() + "   ";
                output += streetObj.GetPlus4Low() + "-";
                output += streetObj.GetPlus4High() + "\r\n";

                txtResults.Text += output;
            }
            else
            {
                txtResults.Text = "No Street Data found";
                return;
            }

            //loop through all streets
            while (streetObj.FindStreetNext() == true)
            {
                string output = "";
                output += streetObj.GetPrimaryRangeLow() + "-";
                output += streetObj.GetPrimaryRangeHigh() + "      ";
                output += streetObj.GetPrimaryRangeOddEven() + "   ";
                output += streetObj.GetPreDirection() + "   ";
                output += streetObj.GetStreetName() + "   ";
                output += streetObj.GetSuffix() + "   ";
                output += streetObj.GetPostDirection() + "   ";
                if (streetObj.GetSuiteName() == "" && streetObj.GetSuiteRangeLow() == "")
                    output += "   ";
                else
                {
                    output += streetObj.GetSuiteName().Trim() + "  ";
                    output += streetObj.GetSuiteRangeLow().Trim() + "-";
                    output += streetObj.GetSuiteRangeHigh().Trim() + "  ";
                }
                output += streetObj.GetZip() + "   ";
                output += streetObj.GetPlus4Low() + "-";
                output += streetObj.GetPlus4High() + "\r\n";

                txtResults.Text += output;
            }

        }
        #endregion

        #region Zip Data
        private void btnCityInState_Click(object sender, System.EventArgs e)
        {
            txtZipResult.Text = "";

            //call FindCityInState
            if (zipObj.FindCityInState(txtZipCity.Text, txtZipState.Text) == true)
            {
                //search successful
                txtZipResult.Text += zipObj.GetCity() + " - " + zipObj.GetState() + "\r\n";
            }
            //loop through all found cities
            while (zipObj.FindCityInStateNext() == true)
            {
                txtZipResult.Text += zipObj.GetCity() + " - " + zipObj.GetState() + "\r\n";
            }
        }

        private void btnZipInCity_Click(object sender, System.EventArgs e)
        {
            txtZipResult.Text = "";

            //call FindZipinCity
            if (zipObj.FindZipInCity(txtZipCity.Text, txtZipState.Text) == true)
            {
                //search successful
                txtZipResult.Text += zipObj.GetZip() + " - " + zipObj.GetCity() + " - " + zipObj.GetState() + " - " + zipObj.GetCountyFips() + " - " + zipObj.GetCountyName() + " - " + zipObj.GetLatitude() + " - " + zipObj.GetLongitude() + " - " + zipObj.GetLastLineIndicator() + "\r\n";
            }
            //loop through all found zips
            while (zipObj.FindZipInCityNext() == true)   //. != 0)
            {
                txtZipResult.Text += zipObj.GetZip() + " - " + zipObj.GetCity() + " - " + zipObj.GetState() + " - " + zipObj.GetCountyFips() + " - " + zipObj.GetCountyName() + " - " + zipObj.GetLatitude() + " - " + zipObj.GetLongitude() + " - " + zipObj.GetLastLineIndicator() + "\r\n";
            }
        }

        private void btnFindZip_Click(object sender, System.EventArgs e)
        {
            txtZipResult.Text = "";

            //call FindZipinCity
            if (zipObj.FindZip(txtZipZip.Text, false) == true)
            {
                //search successful
                txtZipResult.Text += zipObj.GetZip() + " - " + zipObj.GetCity() + " - " + zipObj.GetState() + " - " + zipObj.GetCountyFips() + " - " + zipObj.GetCountyName() + " - " + zipObj.GetLatitude() + " - " + zipObj.GetLongitude() + " - " + zipObj.GetLastLineIndicator() + "\r\n";
            }
            //loop through all found zips
            while (zipObj.FindZipNext() == true)
            {
                txtZipResult.Text += zipObj.GetZip() + " - " + zipObj.GetCity() + " - " + zipObj.GetState() + " - " + zipObj.GetCountyFips() + " - " + zipObj.GetCountyName() + " - " + zipObj.GetLatitude() + " - " + zipObj.GetLongitude() + " - " + zipObj.GetLastLineIndicator() + "\r\n";
            }
        }
        #endregion
    }
}