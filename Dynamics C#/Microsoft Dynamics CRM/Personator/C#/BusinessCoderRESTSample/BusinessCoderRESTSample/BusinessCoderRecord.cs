﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace BusinessCoderRESTSample
{
    [DataContract]
    class BusinessCoderRecord
    {
        [DataMember(Name = "RecordID")]
        public string RecordID { get; set; }

        [DataMember(Name = "Results")]
        public string Results { get; set; }

        [DataMember(Name = "LocationType")]
        public string LocationType { get; set; }

        [DataMember(Name = "Phone")]
        public string Phone { get; set; }

        [DataMember(Name = "StockTicker")]
        public string StockTicker { get; set; }

        [DataMember(Name = "WebAddress")]
        public string WebAddress { get; set; }

        [DataMember(Name = "EmployeesEstimate")]
        public string EmployeesEstimate { get; set; }

        [DataMember(Name = "SalesEstimate")]
        public string SalesEstimate { get; set; }

        [DataMember(Name = "CompanyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "Suite")]
        public string Suite { get; set; }

        [DataMember(Name = "City")]
        public string City { get; set; }

        [DataMember(Name = "State")]
        public string State { get; set; }

        [DataMember(Name = "PostalCode")]
        public string PostalCode { get; set; }

        [DataMember(Name = "Plus4")]
        public string Plus4 { get; set; }
    
        [DataMember(Name = "DeliveryIndicator")]
        public string DeliveryIndicator { get; set; }

        [DataMember(Name = "MelissaAddressKey")]
        public string MelissaAddressKey { get; set; }

        [DataMember(Name = "MelissaAddressKeyBase")]
        public string MelissaAddressKeyBase { get; set; }

        [DataMember(Name = "SICCode1")]
        public string SICCode1 { get; set; }

        [DataMember(Name = "SICCode2")]
        public string SICCode2 { get; set; }

        [DataMember(Name = "SICCode3")]
        public string SICCode3 { get; set; }

        [DataMember(Name = "NAICSCode1")]
        public string NAICSCode1 { get; set; }

        [DataMember(Name = "NAICSCode2")]
        public string NAICSCode2 { get; set; }

        [DataMember(Name = "NAICSCode3")]
        public string NAICSCode3 { get; set; }

        [DataMember(Name = "SICDescription1")]
        public string SICDescription1 { get; set; }

        [DataMember(Name = "SICDescription2")]
        public string SICDescription2 { get; set; }

        [DataMember(Name = "SICDescription3")]
        public string SICDescription3 { get; set; }

        [DataMember(Name = "NAICSDescription1")]
        public string NAICSDescription1 { get; set; }

        [DataMember(Name = "NAICSDescription2")]
        public string NAICSDescription2 { get; set; }

        [DataMember(Name = "NAICSDescription3")]
        public string NAICSDescription3 { get; set; }

        [DataMember(Name = "Latitude")]
        public string Latitude { get; set; }

        [DataMember(Name = "Longitude")]
        public string Longitude { get; set; }

        [DataMember(Name = "CountyName")]
        public string CountyName { get; set; }

        [DataMember(Name = "CountyFIPS")]
        public string CountyFIPS { get; set; }

        [DataMember(Name = "CensusTract")]
        public string CensusTract { get; set; }

        [DataMember(Name = "CensusBlock")]
        public string CensusBlock { get; set; }

        [DataMember(Name = "PlaceCode")]
        public string PlaceCode { get; set; }

        [DataMember(Name = "PlaceName")]
        public string PlaceName { get; set; }

    }
}
