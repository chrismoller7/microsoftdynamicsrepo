"use strict";
(function() {
    this.new_PersonatorActionRequest = function(n, t, i, r, u, f, e, o, s, h, c, l, a, v, y, p, w, b) {
        function wt(n) {
            if (typeof n == "string") d = n;
            else throw new Error("Sdk.new_PersonatorActionRequest CustomerID property is required and must be a String.");
        }

        function bt(n) {
            if (typeof n == "string") g = n;
            else throw new Error("Sdk.new_PersonatorActionRequest Options property is required and must be a String.");
        }

        function kt(n) {
            if (n == null || typeof n == "string") nt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InAddressLine1 property must be a String or null.");
        }

        function dt(n) {
            if (n == null || typeof n == "string") tt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InAddressLine2 property must be a String or null.");
        }

        function gt(n) {
            if (n == null || typeof n == "string") it = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InCity property must be a String or null.");
        }

        function ni(n) {
            if (n == null || typeof n == "string") rt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InCompanyName property must be a String or null.");
        }

        function ti(n) {
            if (n == null || typeof n == "string") ut = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InCountry property must be a String or null.");
        }

        function ii(n) {
            if (n == null || typeof n == "string") ft = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InEmailAddress property must be a String or null.");
        }

        function ri(n) {
            if (n == null || typeof n == "string") et = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InFirstName property must be a String or null.");
        }

        function ui(n) {
            if (n == null || typeof n == "string") ot = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InFreeForm property must be a String or null.");
        }

        function fi(n) {
            if (n == null || typeof n == "string") st = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InFullName property must be a String or null.");
        }

        function ei(n) {
            if (n == null || typeof n == "string") ht = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InLastLine property must be a String or null.");
        }

        function oi(n) {
            if (n == null || typeof n == "string") ct = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InLastName property must be a String or null.");
        }

        function si(n) {
            if (n == null || typeof n == "string") lt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InPhoneNumber property must be a String or null.");
        }

        function hi(n) {
            if (n == null || typeof n == "string") at = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InPostalCode property must be a String or null.");
        }

        function ci(n) {
            if (n == null || typeof n == "string") vt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InRecordID property must be a String or null.");
        }

        function li(n) {
            if (n == null || typeof n == "string") yt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InReserved property must be a String or null.");
        }

        function ai(n) {
            if (n == null || typeof n == "string") pt = n;
            else throw new Error("Sdk.new_PersonatorActionRequest InState property must be a String or null.");
        }

        function k() {
            return ["<d:request>", "<a:Parameters>", "<a:KeyValuePairOfstringanyType>", "<b:key>CustomerID<\/b:key>", d == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', d, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>Options<\/b:key>", g == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', g, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inAddressLine1<\/b:key>", nt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', nt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inAddressLine2<\/b:key>", tt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', tt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inCity<\/b:key>", it == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', it, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inCompanyName<\/b:key>", rt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', rt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inCountry<\/b:key>", ut == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', ut, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inEmailAddress<\/b:key>", ft == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', ft, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inFirstName<\/b:key>", et == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', et, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inFreeForm<\/b:key>", ot == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', ot, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inFullName<\/b:key>", st == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', st, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inLastLine<\/b:key>", ht == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', ht, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inLastName<\/b:key>", ct == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', ct, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inPhoneNumber<\/b:key>", lt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', lt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inPostalCode<\/b:key>", at == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', at, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inRecordID<\/b:key>", vt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', vt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inReserved<\/b:key>", yt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', yt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<a:KeyValuePairOfstringanyType>", "<b:key>inState<\/b:key>", pt == null ? '<b:value i:nil="true" />' : ['<b:value i:type="c:string">', pt, "<\/b:value>"].join(""), "<\/a:KeyValuePairOfstringanyType>", "<\/a:Parameters>", '<a:RequestId i:nil="true" />', "<a:RequestName>new_PersonatorAction<\/a:RequestName>", "<\/d:request>"].join("")
        }
        if (!(this instanceof Sdk.new_PersonatorActionRequest)) return new Sdk.new_PersonatorActionRequest(n, t, i, r, u, f, e, o, s, h, c, l, a, v, y, p, w, b);
        Sdk.OrganizationRequest.call(this);
        var d = null,
            g = null,
            nt = null,
            tt = null,
            it = null,
            rt = null,
            ut = null,
            ft = null,
            et = null,
            ot = null,
            st = null,
            ht = null,
            ct = null,
            lt = null,
            at = null,
            vt = null,
            yt = null,
            pt = null;
        typeof n != "undefined" && wt(n);
        typeof t != "undefined" && bt(t);
        typeof i != "undefined" && kt(i);
        typeof r != "undefined" && dt(r);
        typeof u != "undefined" && gt(u);
        typeof f != "undefined" && ni(f);
        typeof e != "undefined" && ti(e);
        typeof o != "undefined" && ii(o);
        typeof s != "undefined" && ri(s);
        typeof h != "undefined" && ui(h);
        typeof c != "undefined" && fi(c);
        typeof l != "undefined" && ei(l);
        typeof a != "undefined" && oi(a);
        typeof v != "undefined" && si(v);
        typeof y != "undefined" && hi(y);
        typeof p != "undefined" && ci(p);
        typeof w != "undefined" && li(w);
        typeof b != "undefined" && ai(b);
        this.setResponseType(Sdk.new_PersonatorActionResponse);
        this.setRequestXml(k());
        this.setCustomerID = function(n) {
            wt(n);
            this.setRequestXml(k())
        };
        this.setOptions = function(n) {
            bt(n);
            this.setRequestXml(k())
        };
        this.setInAddressLine1 = function(n) {
            kt(n);
            this.setRequestXml(k())
        };
        this.setInAddressLine2 = function(n) {
            dt(n);
            this.setRequestXml(k())
        };
        this.setInCity = function(n) {
            gt(n);
            this.setRequestXml(k())
        };
        this.setInCompanyName = function(n) {
            ni(n);
            this.setRequestXml(k())
        };
        this.setInCountry = function(n) {
            ti(n);
            this.setRequestXml(k())
        };
        this.setInEmailAddress = function(n) {
            ii(n);
            this.setRequestXml(k())
        };
        this.setInFirstName = function(n) {
            ri(n);
            this.setRequestXml(k())
        };
        this.setInFreeForm = function(n) {
            ui(n);
            this.setRequestXml(k())
        };
        this.setInFullName = function(n) {
            fi(n);
            this.setRequestXml(k())
        };
        this.setInLastLine = function(n) {
            ei(n);
            this.setRequestXml(k())
        };
        this.setInLastName = function(n) {
            oi(n);
            this.setRequestXml(k())
        };
        this.setInPhoneNumber = function(n) {
            si(n);
            this.setRequestXml(k())
        };
        this.setInPostalCode = function(n) {
            hi(n);
            this.setRequestXml(k())
        };
        this.setInRecordID = function(n) {
            ci(n);
            this.setRequestXml(k())
        };
        this.setInReserved = function(n) {
            li(n);
            this.setRequestXml(k())
        };
        this.setInState = function(n) {
            ai(n);
            this.setRequestXml(k())
        }
    };
    this.new_PersonatorActionRequest.__class = !0;
    this.new_PersonatorActionResponse = function(n) {
        function gu(n) {
            var i = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='ErrorMessage']/b:value");
            Sdk.Xml.isNodeNull(i) || (t = Sdk.Xml.getNodeText(i))
        }

        function nf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressDeliveryInstallation']/b:value");
            Sdk.Xml.isNodeNull(t) || (i = Sdk.Xml.getNodeText(t))
        }

        function tf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressExtras']/b:value");
            Sdk.Xml.isNodeNull(t) || (r = Sdk.Xml.getNodeText(t))
        }

        function rf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressHouseNumber']/b:value");
            Sdk.Xml.isNodeNull(t) || (u = Sdk.Xml.getNodeText(t))
        }

        function uf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressKey']/b:value");
            Sdk.Xml.isNodeNull(t) || (f = Sdk.Xml.getNodeText(t))
        }

        function ff(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine1']/b:value");
            Sdk.Xml.isNodeNull(t) || (e = Sdk.Xml.getNodeText(t))
        }

        function ef(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine2']/b:value");
            Sdk.Xml.isNodeNull(t) || (o = Sdk.Xml.getNodeText(t))
        }

        function of(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressLockBox']/b:value");
            Sdk.Xml.isNodeNull(t) || (s = Sdk.Xml.getNodeText(t))
        }

        function sf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressPostDirection']/b:value");
            Sdk.Xml.isNodeNull(t) || (h = Sdk.Xml.getNodeText(t))
        }

        function hf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressPreDirection']/b:value");
            Sdk.Xml.isNodeNull(t) || (c = Sdk.Xml.getNodeText(t))
        }

        function cf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressPrivateMailboxName']/b:value");
            Sdk.Xml.isNodeNull(t) || (l = Sdk.Xml.getNodeText(t))
        }

        function lf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressPrivateMailboxRange']/b:value");
            Sdk.Xml.isNodeNull(t) || (a = Sdk.Xml.getNodeText(t))
        }

        function af(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressRouteService']/b:value");
            Sdk.Xml.isNodeNull(t) || (v = Sdk.Xml.getNodeText(t))
        }

        function vf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressStreetName']/b:value");
            Sdk.Xml.isNodeNull(t) || (y = Sdk.Xml.getNodeText(t))
        }

        function yf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressStreetSuffix']/b:value");
            Sdk.Xml.isNodeNull(t) || (p = Sdk.Xml.getNodeText(t))
        }

        function pf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressSuiteName']/b:value");
            Sdk.Xml.isNodeNull(t) || (w = Sdk.Xml.getNodeText(t))
        }

        function wf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressSuiteNumber']/b:value");
            Sdk.Xml.isNodeNull(t) || (b = Sdk.Xml.getNodeText(t))
        }

        function bf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAddressTypeCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (k = Sdk.Xml.getNodeText(t))
        }

        function kf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outAreaCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (d = Sdk.Xml.getNodeText(t))
        }

        function df(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCarrierRoute']/b:value");
            Sdk.Xml.isNodeNull(t) || (g = Sdk.Xml.getNodeText(t))
        }

        function gf(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSACode']/b:value");
            Sdk.Xml.isNodeNull(t) || (nt = Sdk.Xml.getNodeText(t))
        }

        function ne(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (tt = Sdk.Xml.getNodeText(t))
        }

        function te(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionLevel']/b:value");
            Sdk.Xml.isNodeNull(t) || (it = Sdk.Xml.getNodeText(t))
        }

        function ie(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSADivisionTitle']/b:value");
            Sdk.Xml.isNodeNull(t) || (rt = Sdk.Xml.getNodeText(t))
        }

        function re(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSALevel']/b:value");
            Sdk.Xml.isNodeNull(t) || (ut = Sdk.Xml.getNodeText(t))
        }

        function ue(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCBSATitle']/b:value");
            Sdk.Xml.isNodeNull(t) || (ft = Sdk.Xml.getNodeText(t))
        }

        function fe(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCensusBlock']/b:value");
            Sdk.Xml.isNodeNull(t) || (et = Sdk.Xml.getNodeText(t))
        }

        function ee(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCensusKey']/b:value");
            Sdk.Xml.isNodeNull(t) || (ot = Sdk.Xml.getNodeText(t))
        }

        function oe(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCensusTract']/b:value");
            Sdk.Xml.isNodeNull(t) || (st = Sdk.Xml.getNodeText(t))
        }

        function se(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCity']/b:value");
            Sdk.Xml.isNodeNull(t) || (ht = Sdk.Xml.getNodeText(t))
        }

        function he(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCityAbbreviation']/b:value");
            Sdk.Xml.isNodeNull(t) || (ct = Sdk.Xml.getNodeText(t))
        }

        function ce(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCompanyName']/b:value");
            Sdk.Xml.isNodeNull(t) || (lt = Sdk.Xml.getNodeText(t))
        }

        function le(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCongressionalDistrict']/b:value");
            Sdk.Xml.isNodeNull(t) || (at = Sdk.Xml.getNodeText(t))
        }

        function ae(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountryCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (vt = Sdk.Xml.getNodeText(t))
        }

        function ve(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountryName']/b:value");
            Sdk.Xml.isNodeNull(t) || (yt = Sdk.Xml.getNodeText(t))
        }

        function ye(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountyFIPS']/b:value");
            Sdk.Xml.isNodeNull(t) || (pt = Sdk.Xml.getNodeText(t))
        }

        function pe(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountyName']/b:value");
            Sdk.Xml.isNodeNull(t) || (wt = Sdk.Xml.getNodeText(t))
        }

        function we(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountySubdivisionCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (bt = Sdk.Xml.getNodeText(t))
        }

        function be(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outCountySubdivisionName']/b:value");
            Sdk.Xml.isNodeNull(t) || (kt = Sdk.Xml.getNodeText(t))
        }

        function ke(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDateOfBirth']/b:value");
            Sdk.Xml.isNodeNull(t) || (dt = Sdk.Xml.getNodeText(t))
        }

        function de(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDateOfDeath']/b:value");
            Sdk.Xml.isNodeNull(t) || (gt = Sdk.Xml.getNodeText(t))
        }

        function ge(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryIndicator']/b:value");
            Sdk.Xml.isNodeNull(t) || (ni = Sdk.Xml.getNodeText(t))
        }

        function no(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryPointCheckDigit']/b:value");
            Sdk.Xml.isNodeNull(t) || (ti = Sdk.Xml.getNodeText(t))
        }

        function to(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryPointCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (ii = Sdk.Xml.getNodeText(t))
        }

        function io(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDemographicsGender']/b:value");
            Sdk.Xml.isNodeNull(t) || (ri = Sdk.Xml.getNodeText(t))
        }

        function ro(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDemographicsResults']/b:value");
            Sdk.Xml.isNodeNull(t) || (ui = Sdk.Xml.getNodeText(t))
        }

        function uo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outDomainName']/b:value");
            Sdk.Xml.isNodeNull(t) || (fi = Sdk.Xml.getNodeText(t))
        }

        function fo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outElementarySchoolDistrictCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (ei = Sdk.Xml.getNodeText(t))
        }

        function eo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outElementarySchoolDistrictName']/b:value");
            Sdk.Xml.isNodeNull(t) || (oi = Sdk.Xml.getNodeText(t))
        }

        function oo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outEmailAddress']/b:value");
            Sdk.Xml.isNodeNull(t) || (si = Sdk.Xml.getNodeText(t))
        }

        function so(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outGender']/b:value");
            Sdk.Xml.isNodeNull(t) || (hi = Sdk.Xml.getNodeText(t))
        }

        function ho(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outGender2']/b:value");
            Sdk.Xml.isNodeNull(t) || (ci = Sdk.Xml.getNodeText(t))
        }

        function co(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outHouseholdIncome']/b:value");
            Sdk.Xml.isNodeNull(t) || (li = Sdk.Xml.getNodeText(t))
        }

        function lo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
            Sdk.Xml.isNodeNull(t) || (ai = Sdk.Xml.getNodeText(t))
        }

        function ao(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outLengthOfResidence']/b:value");
            Sdk.Xml.isNodeNull(t) || (vi = Sdk.Xml.getNodeText(t))
        }

        function vo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
            Sdk.Xml.isNodeNull(t) || (yi = Sdk.Xml.getNodeText(t))
        }

        function yo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outMailboxName']/b:value");
            Sdk.Xml.isNodeNull(t) || (pi = Sdk.Xml.getNodeText(t))
        }

        function po(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outMaritalStatus']/b:value");
            Sdk.Xml.isNodeNull(t) || (wi = Sdk.Xml.getNodeText(t))
        }

        function wo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outMelissaAddressKey']/b:value");
            Sdk.Xml.isNodeNull(t) || (bi = Sdk.Xml.getNodeText(t))
        }

        function bo(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst']/b:value");
            Sdk.Xml.isNodeNull(t) || (ki = Sdk.Xml.getNodeText(t))
        }

        function ko(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameFirst2']/b:value");
            Sdk.Xml.isNodeNull(t) || (di = Sdk.Xml.getNodeText(t))
        }

        function go(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameFull']/b:value");
            Sdk.Xml.isNodeNull(t) || (gi = Sdk.Xml.getNodeText(t))
        }

        function ns(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameLast']/b:value");
            Sdk.Xml.isNodeNull(t) || (nr = Sdk.Xml.getNodeText(t))
        }

        function ts(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameLast2']/b:value");
            Sdk.Xml.isNodeNull(t) || (tr = Sdk.Xml.getNodeText(t))
        }

        function is(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle']/b:value");
            Sdk.Xml.isNodeNull(t) || (ir = Sdk.Xml.getNodeText(t))
        }

        function rs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameMiddle2']/b:value");
            Sdk.Xml.isNodeNull(t) || (rr = Sdk.Xml.getNodeText(t))
        }

        function us(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix']/b:value");
            Sdk.Xml.isNodeNull(t) || (ur = Sdk.Xml.getNodeText(t))
        }

        function fs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNamePrefix2']/b:value");
            Sdk.Xml.isNodeNull(t) || (fr = Sdk.Xml.getNodeText(t))
        }

        function es(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix']/b:value");
            Sdk.Xml.isNodeNull(t) || (er = Sdk.Xml.getNodeText(t))
        }

        function os(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNameSuffix2']/b:value");
            Sdk.Xml.isNodeNull(t) || (or = Sdk.Xml.getNodeText(t))
        }

        function ss(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outNewAreaCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (sr = Sdk.Xml.getNodeText(t))
        }

        function hs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outOccupation']/b:value");
            Sdk.Xml.isNodeNull(t) || (hr = Sdk.Xml.getNodeText(t))
        }

        function cs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outOwnRent']/b:value");
            Sdk.Xml.isNodeNull(t) || (cr = Sdk.Xml.getNodeText(t))
        }

        function ls(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (lr = Sdk.Xml.getNodeText(t))
        }

        function as(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryName']/b:value");
            Sdk.Xml.isNodeNull(t) || (ar = Sdk.Xml.getNodeText(t))
        }

        function vs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhoneExtension']/b:value");
            Sdk.Xml.isNodeNull(t) || (vr = Sdk.Xml.getNodeText(t))
        }

        function ys(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNumber']/b:value");
            Sdk.Xml.isNodeNull(t) || (yr = Sdk.Xml.getNodeText(t))
        }

        function ps(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhonePrefix']/b:value");
            Sdk.Xml.isNodeNull(t) || (pr = Sdk.Xml.getNodeText(t))
        }

        function ws(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPhoneSuffix']/b:value");
            Sdk.Xml.isNodeNull(t) || (wr = Sdk.Xml.getNodeText(t))
        }

        function bs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPlaceCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (br = Sdk.Xml.getNodeText(t))
        }

        function ks(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPlaceName']/b:value");
            Sdk.Xml.isNodeNull(t) || (kr = Sdk.Xml.getNodeText(t))
        }

        function ds(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPlus4']/b:value");
            Sdk.Xml.isNodeNull(t) || (dr = Sdk.Xml.getNodeText(t))
        }

        function gs(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPostalCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (gr = Sdk.Xml.getNodeText(t))
        }

        function nh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPresenceOfChildren']/b:value");
            Sdk.Xml.isNodeNull(t) || (nu = Sdk.Xml.getNodeText(t))
        }

        function th(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outPrivateMailBox']/b:value");
            Sdk.Xml.isNodeNull(t) || (tu = Sdk.Xml.getNodeText(t))
        }

        function ih(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outRecordExtras']/b:value");
            Sdk.Xml.isNodeNull(t) || (iu = Sdk.Xml.getNodeText(t))
        }

        function rh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outRecordID']/b:value");
            Sdk.Xml.isNodeNull(t) || (ru = Sdk.Xml.getNodeText(t))
        }

        function uh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outReserved']/b:value");
            Sdk.Xml.isNodeNull(t) || (uu = Sdk.Xml.getNodeText(t))
        }

        function fh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
            Sdk.Xml.isNodeNull(t) || (fu = Sdk.Xml.getNodeText(t))
        }

        function eh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outSalutation']/b:value");
            Sdk.Xml.isNodeNull(t) || (eu = Sdk.Xml.getNodeText(t))
        }

        function oh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outSecondarySchoolDistrictCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (ou = Sdk.Xml.getNodeText(t))
        }

        function sh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outSecondarySchoolDistrictName']/b:value");
            Sdk.Xml.isNodeNull(t) || (su = Sdk.Xml.getNodeText(t))
        }

        function hh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outState']/b:value");
            Sdk.Xml.isNodeNull(t) || (hu = Sdk.Xml.getNodeText(t))
        }

        function ch(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outStateDistrictLower']/b:value");
            Sdk.Xml.isNodeNull(t) || (cu = Sdk.Xml.getNodeText(t))
        }

        function lh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outStateDistrictUpper']/b:value");
            Sdk.Xml.isNodeNull(t) || (lu = Sdk.Xml.getNodeText(t))
        }

        function ah(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outStateName']/b:value");
            Sdk.Xml.isNodeNull(t) || (au = Sdk.Xml.getNodeText(t))
        }

        function vh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outSuite']/b:value");
            Sdk.Xml.isNodeNull(t) || (vu = Sdk.Xml.getNodeText(t))
        }

        function yh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outTopLevelDomain']/b:value");
            Sdk.Xml.isNodeNull(t) || (yu = Sdk.Xml.getNodeText(t))
        }

        function ph(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outUnifiedSchoolDistrictCode']/b:value");
            Sdk.Xml.isNodeNull(t) || (pu = Sdk.Xml.getNodeText(t))
        }

        function wh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outUnifiedSchoolDistrictName']/b:value");
            Sdk.Xml.isNodeNull(t) || (wu = Sdk.Xml.getNodeText(t))
        }

        function bh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outUrbanizationName']/b:value");
            Sdk.Xml.isNodeNull(t) || (bu = Sdk.Xml.getNodeText(t))
        }

        function kh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outUTC']/b:value");
            Sdk.Xml.isNodeNull(t) || (ku = Sdk.Xml.getNodeText(t))
        }

        function dh(n) {
            var t = Sdk.Xml.selectSingleNode(n, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
            Sdk.Xml.isNodeNull(t) || (du = Sdk.Xml.getNodeText(t))
        }
        if (!(this instanceof Sdk.new_PersonatorActionResponse)) return new Sdk.new_PersonatorActionResponse(n);
        Sdk.OrganizationResponse.call(this);
        var t = null,
            i = null,
            r = null,
            u = null,
            f = null,
            e = null,
            o = null,
            s = null,
            h = null,
            c = null,
            l = null,
            a = null,
            v = null,
            y = null,
            p = null,
            w = null,
            b = null,
            k = null,
            d = null,
            g = null,
            nt = null,
            tt = null,
            it = null,
            rt = null,
            ut = null,
            ft = null,
            et = null,
            ot = null,
            st = null,
            ht = null,
            ct = null,
            lt = null,
            at = null,
            vt = null,
            yt = null,
            pt = null,
            wt = null,
            bt = null,
            kt = null,
            dt = null,
            gt = null,
            ni = null,
            ti = null,
            ii = null,
            ri = null,
            ui = null,
            fi = null,
            ei = null,
            oi = null,
            si = null,
            hi = null,
            ci = null,
            li = null,
            ai = null,
            vi = null,
            yi = null,
            pi = null,
            wi = null,
            bi = null,
            ki = null,
            di = null,
            gi = null,
            nr = null,
            tr = null,
            ir = null,
            rr = null,
            ur = null,
            fr = null,
            er = null,
            or = null,
            sr = null,
            hr = null,
            cr = null,
            lr = null,
            ar = null,
            vr = null,
            yr = null,
            pr = null,
            wr = null,
            br = null,
            kr = null,
            dr = null,
            gr = null,
            nu = null,
            tu = null,
            iu = null,
            ru = null,
            uu = null,
            fu = null,
            eu = null,
            ou = null,
            su = null,
            hu = null,
            cu = null,
            lu = null,
            au = null,
            vu = null,
            yu = null,
            pu = null,
            wu = null,
            bu = null,
            ku = null,
            du = null;
        this.getErrorMessage = function() {
            return t
        };
        this.getOutAddressDeliveryInstallation = function() {
            return i
        };
        this.getOutAddressExtras = function() {
            return r
        };
        this.getOutAddressHouseNumber = function() {
            return u
        };
        this.getOutAddressKey = function() {
            return f
        };
        this.getOutAddressLine1 = function() {
            return e
        };
        this.getOutAddressLine2 = function() {
            return o
        };
        this.getOutAddressLockBox = function() {
            return s
        };
        this.getOutAddressPostDirection = function() {
            return h
        };
        this.getOutAddressPreDirection = function() {
            return c
        };
        this.getOutAddressPrivateMailboxName = function() {
            return l
        };
        this.getOutAddressPrivateMailboxRange = function() {
            return a
        };
        this.getOutAddressRouteService = function() {
            return v
        };
        this.getOutAddressStreetName = function() {
            return y
        };
        this.getOutAddressStreetSuffix = function() {
            return p
        };
        this.getOutAddressSuiteName = function() {
            return w
        };
        this.getOutAddressSuiteNumber = function() {
            return b
        };
        this.getOutAddressTypeCode = function() {
            return k
        };
        this.getOutAreaCode = function() {
            return d
        };
        this.getOutCarrierRoute = function() {
            return g
        };
        this.getOutCBSACode = function() {
            return nt
        };
        this.getOutCBSADivisionCode = function() {
            return tt
        };
        this.getOutCBSADivisionLevel = function() {
            return it
        };
        this.getOutCBSADivisionTitle = function() {
            return rt
        };
        this.getOutCBSALevel = function() {
            return ut
        };
        this.getOutCBSATitle = function() {
            return ft
        };
        this.getOutCensusBlock = function() {
            return et
        };
        this.getOutCensusKey = function() {
            return ot
        };
        this.getOutCensusTract = function() {
            return st
        };
        this.getOutCity = function() {
            return ht
        };
        this.getOutCityAbbreviation = function() {
            return ct
        };
        this.getOutCompanyName = function() {
            return lt
        };
        this.getOutCongressionalDistrict = function() {
            return at
        };
        this.getOutCountryCode = function() {
            return vt
        };
        this.getOutCountryName = function() {
            return yt
        };
        this.getOutCountyFIPS = function() {
            return pt
        };
        this.getOutCountyName = function() {
            return wt
        };
        this.getOutCountySubdivisionCode = function() {
            return bt
        };
        this.getOutCountySubdivisionName = function() {
            return kt
        };
        this.getOutDateOfBirth = function() {
            return dt
        };
        this.getOutDateOfDeath = function() {
            return gt
        };
        this.getOutDeliveryIndicator = function() {
            return ni
        };
        this.getOutDeliveryPointCheckDigit = function() {
            return ti
        };
        this.getOutDeliveryPointCode = function() {
            return ii
        };
        this.getOutDemographicsGender = function() {
            return ri
        };
        this.getOutDemographicsResults = function() {
            return ui
        };
        this.getOutDomainName = function() {
            return fi
        };
        this.getOutElementarySchoolDistrictCode = function() {
            return ei
        };
        this.getOutElementarySchoolDistrictName = function() {
            return oi
        };
        this.getOutEmailAddress = function() {
            return si
        };
        this.getOutGender = function() {
            return hi
        };
        this.getOutGender2 = function() {
            return ci
        };
        this.getOutHouseholdIncome = function() {
            return li
        };
        this.getOutLatitude = function() {
            return ai
        };
        this.getOutLengthOfResidence = function() {
            return vi
        };
        this.getOutLongitude = function() {
            return yi
        };
        this.getOutMailboxName = function() {
            return pi
        };
        this.getOutMaritalStatus = function() {
            return wi
        };
        this.getOutMelissaAddressKey = function() {
            return bi
        };
        this.getOutNameFirst = function() {
            return ki
        };
        this.getOutNameFirst2 = function() {
            return di
        };
        this.getOutNameFull = function() {
            return gi
        };
        this.getOutNameLast = function() {
            return nr
        };
        this.getOutNameLast2 = function() {
            return tr
        };
        this.getOutNameMiddle = function() {
            return ir
        };
        this.getOutNameMiddle2 = function() {
            return rr
        };
        this.getOutNamePrefix = function() {
            return ur
        };
        this.getOutNamePrefix2 = function() {
            return fr
        };
        this.getOutNameSuffix = function() {
            return er
        };
        this.getOutNameSuffix2 = function() {
            return or
        };
        this.getOutNewAreaCode = function() {
            return sr
        };
        this.getOutOccupation = function() {
            return hr
        };
        this.getOutOwnRent = function() {
            return cr
        };
        this.getOutPhoneCountryCode = function() {
            return lr
        };
        this.getOutPhoneCountryName = function() {
            return ar
        };
        this.getOutPhoneExtension = function() {
            return vr
        };
        this.getOutPhoneNumber = function() {
            return yr
        };
        this.getOutPhonePrefix = function() {
            return pr
        };
        this.getOutPhoneSuffix = function() {
            return wr
        };
        this.getOutPlaceCode = function() {
            return br
        };
        this.getOutPlaceName = function() {
            return kr
        };
        this.getOutPlus4 = function() {
            return dr
        };
        this.getOutPostalCode = function() {
            return gr
        };
        this.getOutPresenceOfChildren = function() {
            return nu
        };
        this.getOutPrivateMailBox = function() {
            return tu
        };
        this.getOutRecordExtras = function() {
            return iu
        };
        this.getOutRecordID = function() {
            return ru
        };
        this.getOutReserved = function() {
            return uu
        };
        this.getOutResults = function() {
            return fu
        };
        this.getOutSalutation = function() {
            return eu
        };
        this.getOutSecondarySchoolDistrictCode = function() {
            return ou
        };
        this.getOutSecondarySchoolDistrictName = function() {
            return su
        };
        this.getOutState = function() {
            return hu
        };
        this.getOutStateDistrictLower = function() {
            return cu
        };
        this.getOutStateDistrictUpper = function() {
            return lu
        };
        this.getOutStateName = function() {
            return au
        };
        this.getOutSuite = function() {
            return vu
        };
        this.getOutTopLevelDomain = function() {
            return yu
        };
        this.getOutUnifiedSchoolDistrictCode = function() {
            return pu
        };
        this.getOutUnifiedSchoolDistrictName = function() {
            return wu
        };
        this.getOutUrbanizationName = function() {
            return bu
        };
        this.getOutUTC = function() {
            return ku
        };
        this.getOutError = function() {
            return du
        };
        gu(n);
        nf(n);
        tf(n);
        rf(n);
        uf(n);
        ff(n);
        ef(n);
        of(n);
        sf(n);
        hf(n);
        cf(n);
        lf(n);
        af(n);
        vf(n);
        yf(n);
        pf(n);
        wf(n);
        bf(n);
        kf(n);
        df(n);
        gf(n);
        ne(n);
        te(n);
        ie(n);
        re(n);
        ue(n);
        fe(n);
        ee(n);
        oe(n);
        se(n);
        he(n);
        ce(n);
        le(n);
        ae(n);
        ve(n);
        ye(n);
        pe(n);
        we(n);
        be(n);
        ke(n);
        de(n);
        ge(n);
        no(n);
        to(n);
        io(n);
        ro(n);
        uo(n);
        fo(n);
        eo(n);
        oo(n);
        so(n);
        ho(n);
        co(n);
        lo(n);
        ao(n);
        vo(n);
        yo(n);
        po(n);
        wo(n);
        bo(n);
        ko(n);
        go(n);
        ns(n);
        ts(n);
        is(n);
        rs(n);
        us(n);
        fs(n);
        es(n);
        os(n);
        ss(n);
        hs(n);
        cs(n);
        ls(n);
        as(n);
        vs(n);
        ys(n);
        ps(n);
        ws(n);
        bs(n);
        ks(n);
        ds(n);
        gs(n);
        nh(n);
        th(n);
        ih(n);
        rh(n);
        uh(n);
        fh(n);
        eh(n);
        oh(n);
        sh(n);
        hh(n);
        ch(n);
        lh(n);
        ah(n);
        vh(n);
        yh(n);
        ph(n);
        wh(n);
        bh(n);
        kh(n);
        dh(n)
    };
    this.new_PersonatorActionResponse.__class = !0
}).call(Sdk);
Sdk.new_PersonatorActionRequest.prototype = new Sdk.OrganizationRequest;
Sdk.new_PersonatorActionResponse.prototype = new Sdk.OrganizationResponse