"use strict";
(function () {
this.new_ValidateAddressRequest = function (
addressLine1,
addressLine2,
city,
state,
zip,
country,
target
)
{
///<summary>
/// 
///</summary>
///<param name="addressLine1"  type="String">
/// [Add Description]
///</param>
///<param name="addressLine2"  type="String">
/// [Add Description]
///</param>
///<param name="city"  type="String">
/// [Add Description]
///</param>
///<param name="state"  type="String">
/// [Add Description]
///</param>
///<param name="zip"  type="String">
/// [Add Description]
///</param>
///<param name="country"  type="String">
/// [Add Description]
///</param>
///<param name="target"  type="Sdk.EntityReference">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.new_ValidateAddressRequest)) {
return new Sdk.new_ValidateAddressRequest(addressLine1, addressLine2, city, state, zip, country, target);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _AddressLine1 = null;
var _AddressLine2 = null;
var _City = null;
var _State = null;
var _Zip = null;
var _Country = null;
var _Target = null;

// internal validation functions

function _setValidAddressLine1(value) {
 if (typeof value == "string") {
  _AddressLine1 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest AddressLine1 property is required and must be a String.")
 }
}

function _setValidAddressLine2(value) {
 if (typeof value == "string") {
  _AddressLine2 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest AddressLine2 property is required and must be a String.")
 }
}

function _setValidCity(value) {
 if (typeof value == "string") {
  _City = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest City property is required and must be a String.")
 }
}

function _setValidState(value) {
 if (typeof value == "string") {
  _State = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest State property is required and must be a String.")
 }
}

function _setValidZip(value) {
 if (typeof value == "string") {
  _Zip = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest Zip property is required and must be a String.")
 }
}

function _setValidCountry(value) {
 if (typeof value == "string") {
  _Country = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest Country property is required and must be a String.")
 }
}

function _setValidTarget(value) {
 if (value instanceof Sdk.EntityReference) {
  _Target = value;
 }
 else {
  throw new Error("Sdk.new_ValidateAddressRequest Target property is required and must be a Sdk.EntityReference.")
 }
}

//Set internal properties from constructor parameters
  if (typeof addressLine1 != "undefined") {
   _setValidAddressLine1(addressLine1);
  }
  if (typeof addressLine2 != "undefined") {
   _setValidAddressLine2(addressLine2);
  }
  if (typeof city != "undefined") {
   _setValidCity(city);
  }
  if (typeof state != "undefined") {
   _setValidState(state);
  }
  if (typeof zip != "undefined") {
   _setValidZip(zip);
  }
  if (typeof country != "undefined") {
   _setValidCountry(country);
  }
  if (typeof target != "undefined") {
   _setValidTarget(target);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>AddressLine1</b:key>",
           (_AddressLine1 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _AddressLine1, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>AddressLine2</b:key>",
           (_AddressLine2 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _AddressLine2, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>City</b:key>",
           (_City == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _City, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>State</b:key>",
           (_State == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _State, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Zip</b:key>",
           (_Zip == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Zip, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Country</b:key>",
           (_Country == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Country, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Target</b:key>",
           (_Target == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"a:EntityReference\">", _Target.toValueXml(), "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>new_ValidateAddress</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.new_ValidateAddressResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setAddressLine1 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidAddressLine1(value);
   this.setRequestXml(getRequestXml());
  }

  this.setAddressLine2 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidAddressLine2(value);
   this.setRequestXml(getRequestXml());
  }

  this.setCity = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidCity(value);
   this.setRequestXml(getRequestXml());
  }

  this.setState = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidState(value);
   this.setRequestXml(getRequestXml());
  }

  this.setZip = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidZip(value);
   this.setRequestXml(getRequestXml());
  }

  this.setCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidCountry(value);
   this.setRequestXml(getRequestXml());
  }

  this.setTarget = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="Sdk.EntityReference">
  /// [Add Description]
  ///</param>
   _setValidTarget(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.new_ValidateAddressRequest.__class = true;

this.new_ValidateAddressResponse = function (responseXml) {
  ///<summary>
  /// Response to new_ValidateAddressRequest
  ///</summary>
  if (!(this instanceof Sdk.new_ValidateAddressResponse)) {
   return new Sdk.new_ValidateAddressResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outAddressLine1 = null;
  var _outAddressLine2 = null;
  var _outCity = null;
  var _outState = null;
  var _outZip = null;
  var _outCountry = null;

  // Internal property setter functions

  function _setOutAddressLine1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutAddressLine1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutAddressLine2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCity(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutCity']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCity = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutState(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutState']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outState = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutZip(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutZip']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outZip = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountry(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='OutCountry']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountry = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutAddressLine1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine1;
  }
  this.getOutAddressLine2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine2;
  }
  this.getOutCity = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCity;
  }
  this.getOutState = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outState;
  }
  this.getOutZip = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outZip;
  }
  this.getOutCountry = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountry;
  }

  //Set property values from responseXml constructor parameter
  _setOutAddressLine1(responseXml);
  _setOutAddressLine2(responseXml);
  _setOutCity(responseXml);
  _setOutState(responseXml);
  _setOutZip(responseXml);
  _setOutCountry(responseXml);
 }
 this.new_ValidateAddressResponse.__class = true;
}).call(Sdk)

Sdk.new_ValidateAddressRequest.prototype = new Sdk.OrganizationRequest();
Sdk.new_ValidateAddressResponse.prototype = new Sdk.OrganizationResponse();
