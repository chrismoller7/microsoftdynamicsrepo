"use strict";
(function () {
this.md_BusinessVerifyRequest = function (
inInputOption,
inLicense,
inColumns,
inOptions,
inCompanyName,
inPhoneNumber,
inAddress1,
inAddress2,
inCity,
inState,
inPostal,
inCountry,
inMAK,
inStockTicker,
inWebAddress,
inTransmissionReference,
inRecordID
)
{
///<summary>
/// 
///</summary>
///<param name="inInputOption"  type="String">
/// [Add Description]
///</param>
///<param name="inLicense" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inColumns" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inOptions" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCompanyName" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inPhoneNumber" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddress1" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddress2" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCity" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inState" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inPostal" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCountry" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inMAK" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inStockTicker" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inWebAddress" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inTransmissionReference" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inRecordID" optional="true" type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_BusinessVerifyRequest)) {
return new Sdk.md_BusinessVerifyRequest(inInputOption, inLicense, inColumns, inOptions, inCompanyName, inPhoneNumber, inAddress1, inAddress2, inCity, inState, inPostal, inCountry, inMAK, inStockTicker, inWebAddress, inTransmissionReference, inRecordID);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _inInputOption = null;
var _inLicense = null;
var _inColumns = null;
var _inOptions = null;
var _inCompanyName = null;
var _inPhoneNumber = null;
var _inAddress1 = null;
var _inAddress2 = null;
var _inCity = null;
var _inState = null;
var _inPostal = null;
var _inCountry = null;
var _inMAK = null;
var _inStockTicker = null;
var _inWebAddress = null;
var _inTransmissionReference = null;
var _inRecordID = null;

// internal validation functions

function _setValidInInputOption(value) {
 if (typeof value == "string") {
  _inInputOption = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InInputOption property is required and must be a String.")
 }
}

function _setValidInLicense(value) {
 if (value == null || typeof value == "string") {
  _inLicense = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InLicense property must be a String or null.")
 }
}

function _setValidInColumns(value) {
 if (value == null || typeof value == "string") {
  _inColumns = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InColumns property must be a String or null.")
 }
}

function _setValidInOptions(value) {
 if (value == null || typeof value == "string") {
  _inOptions = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InOptions property must be a String or null.")
 }
}

function _setValidInCompanyName(value) {
 if (value == null || typeof value == "string") {
  _inCompanyName = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InCompanyName property must be a String or null.")
 }
}

function _setValidInPhoneNumber(value) {
 if (value == null || typeof value == "string") {
  _inPhoneNumber = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InPhoneNumber property must be a String or null.")
 }
}

function _setValidInAddress1(value) {
 if (value == null || typeof value == "string") {
  _inAddress1 = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InAddress1 property must be a String or null.")
 }
}

function _setValidInAddress2(value) {
 if (value == null || typeof value == "string") {
  _inAddress2 = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InAddress2 property must be a String or null.")
 }
}

function _setValidInCity(value) {
 if (value == null || typeof value == "string") {
  _inCity = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InCity property must be a String or null.")
 }
}

function _setValidInState(value) {
 if (value == null || typeof value == "string") {
  _inState = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InState property must be a String or null.")
 }
}

function _setValidInPostal(value) {
 if (value == null || typeof value == "string") {
  _inPostal = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InPostal property must be a String or null.")
 }
}

function _setValidInCountry(value) {
 if (value == null || typeof value == "string") {
  _inCountry = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InCountry property must be a String or null.")
 }
}

function _setValidInMAK(value) {
 if (value == null || typeof value == "string") {
  _inMAK = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InMAK property must be a String or null.")
 }
}

function _setValidInStockTicker(value) {
 if (value == null || typeof value == "string") {
  _inStockTicker = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InStockTicker property must be a String or null.")
 }
}

function _setValidInWebAddress(value) {
 if (value == null || typeof value == "string") {
  _inWebAddress = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InWebAddress property must be a String or null.")
 }
}

function _setValidInTransmissionReference(value) {
 if (value == null || typeof value == "string") {
  _inTransmissionReference = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InTransmissionReference property must be a String or null.")
 }
}

function _setValidInRecordID(value) {
 if (value == null || typeof value == "string") {
  _inRecordID = value;
 }
 else {
  throw new Error("Sdk.md_BusinessVerifyRequest InRecordID property must be a String or null.")
 }
}

//Set internal properties from constructor parameters
  if (typeof inInputOption != "undefined") {
   _setValidInInputOption(inInputOption);
  }
  if (typeof inLicense != "undefined") {
   _setValidInLicense(inLicense);
  }
  if (typeof inColumns != "undefined") {
   _setValidInColumns(inColumns);
  }
  if (typeof inOptions != "undefined") {
   _setValidInOptions(inOptions);
  }
  if (typeof inCompanyName != "undefined") {
   _setValidInCompanyName(inCompanyName);
  }
  if (typeof inPhoneNumber != "undefined") {
   _setValidInPhoneNumber(inPhoneNumber);
  }
  if (typeof inAddress1 != "undefined") {
   _setValidInAddress1(inAddress1);
  }
  if (typeof inAddress2 != "undefined") {
   _setValidInAddress2(inAddress2);
  }
  if (typeof inCity != "undefined") {
   _setValidInCity(inCity);
  }
  if (typeof inState != "undefined") {
   _setValidInState(inState);
  }
  if (typeof inPostal != "undefined") {
   _setValidInPostal(inPostal);
  }
  if (typeof inCountry != "undefined") {
   _setValidInCountry(inCountry);
  }
  if (typeof inMAK != "undefined") {
   _setValidInMAK(inMAK);
  }
  if (typeof inStockTicker != "undefined") {
   _setValidInStockTicker(inStockTicker);
  }
  if (typeof inWebAddress != "undefined") {
   _setValidInWebAddress(inWebAddress);
  }
  if (typeof inTransmissionReference != "undefined") {
   _setValidInTransmissionReference(inTransmissionReference);
  }
  if (typeof inRecordID != "undefined") {
   _setValidInRecordID(inRecordID);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inInputOption</b:key>",
           (_inInputOption == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inInputOption, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inLicense</b:key>",
           (_inLicense == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inLicense, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inColumns</b:key>",
           (_inColumns == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inColumns, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inOptions</b:key>",
           (_inOptions == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inOptions, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCompanyName</b:key>",
           (_inCompanyName == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCompanyName, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPhoneNumber</b:key>",
           (_inPhoneNumber == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPhoneNumber, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddress1</b:key>",
           (_inAddress1 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddress1, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddress2</b:key>",
           (_inAddress2 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddress2, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCity</b:key>",
           (_inCity == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCity, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inState</b:key>",
           (_inState == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inState, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPostal</b:key>",
           (_inPostal == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPostal, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountry</b:key>",
           (_inCountry == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountry, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inMAK</b:key>",
           (_inMAK == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inMAK, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inStockTicker</b:key>",
           (_inStockTicker == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inStockTicker, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inWebAddress</b:key>",
           (_inWebAddress == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inWebAddress, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inTransmissionReference</b:key>",
           (_inTransmissionReference == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inTransmissionReference, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inRecordID</b:key>",
           (_inRecordID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inRecordID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_BusinessVerify</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_BusinessVerifyResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setInInputOption = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInInputOption(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInLicense = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInLicense(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInColumns = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInColumns(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCompanyName = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCompanyName(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPhoneNumber = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPhoneNumber(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddress1 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddress1(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddress2 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddress2(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCity = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCity(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInState = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInState(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPostal = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPostal(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountry(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInMAK = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInMAK(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInStockTicker = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInStockTicker(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInWebAddress = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInWebAddress(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInTransmissionReference = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInTransmissionReference(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInRecordID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInRecordID(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_BusinessVerifyRequest.__class = true;

this.md_BusinessVerifyResponse = function (responseXml) {
  ///<summary>
  /// Response to md_BusinessVerifyRequest
  ///</summary>
  if (!(this instanceof Sdk.md_BusinessVerifyResponse)) {
   return new Sdk.md_BusinessVerifyResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outContacts = null;
  var _outProcessedResults = null;
  var _outResponseURL = null;
  var _outLocationType = null;
  var _outPhone = null;
  var _outStockTicker = null;
  var _outWebAddress = null;
  var _outEmployeesEstimate = null;
  var _outSalesEstimate = null;
  var _outCompanyName = null;
  var _outAddressLine1 = null;
  var _outSuite = null;
  var _outCity = null;
  var _outState = null;
  var _outPostalCode = null;
  var _outPlus4 = null;
  var _outDeliveryIndicator = null;
  var _outMelissaAddressKey = null;
  var _outMelissaAddressKeyBase = null;
  var _outCountryName = null;
  var _outCountryCode = null;
  var _outSICCode1 = null;
  var _outSICCode2 = null;
  var _outSICCode3 = null;
  var _outNAICSCode1 = null;
  var _outNAICSCode2 = null;
  var _outNAICSCode3 = null;
  var _outSICDescription1 = null;
  var _outSICDescription2 = null;
  var _outSICDescription3 = null;
  var _outNAICSDescription1 = null;
  var _outNAICSDescription2 = null;
  var _outNAICSDescription3 = null;
  var _outLatitude = null;
  var _outLongitude = null;
  var _outCountyName = null;
  var _outCountyFIPS = null;
  var _outCensusTract = null;
  var _outCensusBlock = null;
  var _outPlaceCode = null;
  var _outPlaceName = null;

  // Internal property setter functions

  function _setOutContacts(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outContacts']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outContacts = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutProcessedResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outProcessedResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outProcessedResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResponseURL(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResponseURL']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResponseURL = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLocationType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLocationType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLocationType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhone(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhone']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhone = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutStockTicker(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outStockTicker']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outStockTicker = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutWebAddress(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outWebAddress']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outWebAddress = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutEmployeesEstimate(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outEmployeesEstimate']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outEmployeesEstimate = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSalesEstimate(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSalesEstimate']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSalesEstimate = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCompanyName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCompanyName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCompanyName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSuite(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSuite']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSuite = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCity(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCity']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCity = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutState(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outState']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outState = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPostalCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPostalCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPostalCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlus4(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlus4']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlus4 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDeliveryIndicator(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDeliveryIndicator']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDeliveryIndicator = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMelissaAddressKey(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMelissaAddressKey']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMelissaAddressKey = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMelissaAddressKeyBase(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMelissaAddressKeyBase']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMelissaAddressKeyBase = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountryName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountryName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountryName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountryCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountryCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountryCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICCode1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICCode1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICCode1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICCode2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICCode2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICCode2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICCode3(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICCode3']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICCode3 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSCode1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSCode1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSCode1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSCode2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSCode2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSCode2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSCode3(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSCode3']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSCode3 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICDescription1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICDescription1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICDescription1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICDescription2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICDescription2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICDescription2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSICDescription3(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSICDescription3']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSICDescription3 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSDescription1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSDescription1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSDescription1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSDescription2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSDescription2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSDescription2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutNAICSDescription3(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outNAICSDescription3']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outNAICSDescription3 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLatitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLatitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLongitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLongitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountyName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountyName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountyName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountyFIPS(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountyFIPS']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountyFIPS = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCensusTract(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCensusTract']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCensusTract = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCensusBlock(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCensusBlock']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCensusBlock = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlaceCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlaceCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlaceCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPlaceName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPlaceName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPlaceName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutContacts = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outContacts;
  }
  this.getOutProcessedResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outProcessedResults;
  }
  this.getOutResponseURL = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResponseURL;
  }
  this.getOutLocationType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLocationType;
  }
  this.getOutPhone = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhone;
  }
  this.getOutStockTicker = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outStockTicker;
  }
  this.getOutWebAddress = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outWebAddress;
  }
  this.getOutEmployeesEstimate = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outEmployeesEstimate;
  }
  this.getOutSalesEstimate = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSalesEstimate;
  }
  this.getOutCompanyName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCompanyName;
  }
  this.getOutAddressLine1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine1;
  }
  this.getOutSuite = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSuite;
  }
  this.getOutCity = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCity;
  }
  this.getOutState = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outState;
  }
  this.getOutPostalCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPostalCode;
  }
  this.getOutPlus4 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlus4;
  }
  this.getOutDeliveryIndicator = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDeliveryIndicator;
  }
  this.getOutMelissaAddressKey = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMelissaAddressKey;
  }
  this.getOutMelissaAddressKeyBase = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMelissaAddressKeyBase;
  }
  this.getOutCountryName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountryName;
  }
  this.getOutCountryCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountryCode;
  }
  this.getOutSICCode1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICCode1;
  }
  this.getOutSICCode2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICCode2;
  }
  this.getOutSICCode3 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICCode3;
  }
  this.getOutNAICSCode1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSCode1;
  }
  this.getOutNAICSCode2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSCode2;
  }
  this.getOutNAICSCode3 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSCode3;
  }
  this.getOutSICDescription1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICDescription1;
  }
  this.getOutSICDescription2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICDescription2;
  }
  this.getOutSICDescription3 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSICDescription3;
  }
  this.getOutNAICSDescription1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSDescription1;
  }
  this.getOutNAICSDescription2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSDescription2;
  }
  this.getOutNAICSDescription3 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outNAICSDescription3;
  }
  this.getOutLatitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLatitude;
  }
  this.getOutLongitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLongitude;
  }
  this.getOutCountyName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountyName;
  }
  this.getOutCountyFIPS = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountyFIPS;
  }
  this.getOutCensusTract = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCensusTract;
  }
  this.getOutCensusBlock = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCensusBlock;
  }
  this.getOutPlaceCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlaceCode;
  }
  this.getOutPlaceName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPlaceName;
  }

  //Set property values from responseXml constructor parameter
  _setOutContacts(responseXml);
  _setOutProcessedResults(responseXml);
  _setOutResponseURL(responseXml);
  _setOutLocationType(responseXml);
  _setOutPhone(responseXml);
  _setOutStockTicker(responseXml);
  _setOutWebAddress(responseXml);
  _setOutEmployeesEstimate(responseXml);
  _setOutSalesEstimate(responseXml);
  _setOutCompanyName(responseXml);
  _setOutAddressLine1(responseXml);
  _setOutSuite(responseXml);
  _setOutCity(responseXml);
  _setOutState(responseXml);
  _setOutPostalCode(responseXml);
  _setOutPlus4(responseXml);
  _setOutDeliveryIndicator(responseXml);
  _setOutMelissaAddressKey(responseXml);
  _setOutMelissaAddressKeyBase(responseXml);
  _setOutCountryName(responseXml);
  _setOutCountryCode(responseXml);
  _setOutSICCode1(responseXml);
  _setOutSICCode2(responseXml);
  _setOutSICCode3(responseXml);
  _setOutNAICSCode1(responseXml);
  _setOutNAICSCode2(responseXml);
  _setOutNAICSCode3(responseXml);
  _setOutSICDescription1(responseXml);
  _setOutSICDescription2(responseXml);
  _setOutSICDescription3(responseXml);
  _setOutNAICSDescription1(responseXml);
  _setOutNAICSDescription2(responseXml);
  _setOutNAICSDescription3(responseXml);
  _setOutLatitude(responseXml);
  _setOutLongitude(responseXml);
  _setOutCountyName(responseXml);
  _setOutCountyFIPS(responseXml);
  _setOutCensusTract(responseXml);
  _setOutCensusBlock(responseXml);
  _setOutPlaceCode(responseXml);
  _setOutPlaceName(responseXml);
 }
 this.md_BusinessVerifyResponse.__class = true;
}).call(Sdk)

Sdk.md_BusinessVerifyRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_BusinessVerifyResponse.prototype = new Sdk.OrganizationResponse();
