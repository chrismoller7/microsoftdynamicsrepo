"use strict";
(function () {
this.md_PhoneVerifyRequest = function (
inDefaultCallingCode,
inCustomerID,
inOptions,
inPhoneNumber,
inCountry,
inCountryOfOrigin
)
{
///<summary>
/// 
///</summary>
///<param name="inDefaultCallingCode" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCustomerID"  type="String">
/// [Add Description]
///</param>
///<param name="inOptions"  type="String">
/// [Add Description]
///</param>
///<param name="inPhoneNumber"  type="String">
/// [Add Description]
///</param>
///<param name="inCountry"  type="String">
/// [Add Description]
///</param>
///<param name="inCountryOfOrigin"  type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_PhoneVerifyRequest)) {
return new Sdk.md_PhoneVerifyRequest(inDefaultCallingCode, inCustomerID, inOptions, inPhoneNumber, inCountry, inCountryOfOrigin);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _inDefaultCallingCode = null;
var _inCustomerID = null;
var _inOptions = null;
var _inPhoneNumber = null;
var _inCountry = null;
var _inCountryOfOrigin = null;

// internal validation functions

function _setValidInDefaultCallingCode(value) {
 if (value == null || typeof value == "string") {
  _inDefaultCallingCode = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InDefaultCallingCode property must be a String or null.")
 }
}

function _setValidInCustomerID(value) {
 if (typeof value == "string") {
  _inCustomerID = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InCustomerID property is required and must be a String.")
 }
}

function _setValidInOptions(value) {
 if (typeof value == "string") {
  _inOptions = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InOptions property is required and must be a String.")
 }
}

function _setValidInPhoneNumber(value) {
 if (typeof value == "string") {
  _inPhoneNumber = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InPhoneNumber property is required and must be a String.")
 }
}

function _setValidInCountry(value) {
 if (typeof value == "string") {
  _inCountry = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InCountry property is required and must be a String.")
 }
}

function _setValidInCountryOfOrigin(value) {
 if (typeof value == "string") {
  _inCountryOfOrigin = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerifyRequest InCountryOfOrigin property is required and must be a String.")
 }
}

//Set internal properties from constructor parameters
  if (typeof inDefaultCallingCode != "undefined") {
   _setValidInDefaultCallingCode(inDefaultCallingCode);
  }
  if (typeof inCustomerID != "undefined") {
   _setValidInCustomerID(inCustomerID);
  }
  if (typeof inOptions != "undefined") {
   _setValidInOptions(inOptions);
  }
  if (typeof inPhoneNumber != "undefined") {
   _setValidInPhoneNumber(inPhoneNumber);
  }
  if (typeof inCountry != "undefined") {
   _setValidInCountry(inCountry);
  }
  if (typeof inCountryOfOrigin != "undefined") {
   _setValidInCountryOfOrigin(inCountryOfOrigin);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inDefaultCallingCode</b:key>",
           (_inDefaultCallingCode == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inDefaultCallingCode, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCustomerID</b:key>",
           (_inCustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inOptions</b:key>",
           (_inOptions == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inOptions, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPhoneNumber</b:key>",
           (_inPhoneNumber == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPhoneNumber, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountry</b:key>",
           (_inCountry == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountry, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountryOfOrigin</b:key>",
           (_inCountryOfOrigin == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountryOfOrigin, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_PhoneVerify</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_PhoneVerifyResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setInDefaultCallingCode = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInDefaultCallingCode(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPhoneNumber = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPhoneNumber(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountry(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountryOfOrigin = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountryOfOrigin(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_PhoneVerifyRequest.__class = true;

this.md_PhoneVerifyResponse = function (responseXml) {
  ///<summary>
  /// Response to md_PhoneVerifyRequest
  ///</summary>
  if (!(this instanceof Sdk.md_PhoneVerifyResponse)) {
   return new Sdk.md_PhoneVerifyResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outCallerID = null;
  var _outCarrierName = null;
  var _outPhoneNumber = null;
  var _outCountry = null;
  var _outError = null;
  var _outVersion = null;
  var _outTransmissionResults = null;
  var _outResults = null;
  var _outPhoneInternationalPrefix = null;
  var _outPhoneCountryDialingCode = null;
  var _outPhoneNationPrefix = null;
  var _outPhoneNationalDestinationCode = null;
  var _outPhoneSubscriberNumber = null;
  var _outLocality = null;
  var _outAdministrativeArea = null;
  var _outDST = null;
  var _outUTC = null;
  var _outLanguage = null;
  var _outLatitude = null;
  var _outLongitude = null;
  var _outPostalCode = null;
  var _outInternationalPhoneNumber = null;

  // Internal property setter functions

  function _setOutCallerID(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCallerID']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCallerID = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCarrierName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCarrierName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCarrierName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountry(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountry']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountry = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutError(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outError = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutVersion(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outVersion']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outVersion = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTransmissionResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTransmissionResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTransmissionResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneInternationalPrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneInternationalPrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneInternationalPrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneCountryDialingCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryDialingCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneCountryDialingCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNationPrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNationPrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNationPrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNationalDestinationCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNationalDestinationCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNationalDestinationCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneSubscriberNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneSubscriberNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneSubscriberNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLocality(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLocality']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLocality = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAdministrativeArea(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAdministrativeArea']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAdministrativeArea = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDST(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDST']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDST = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUTC(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUTC']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUTC = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLanguage(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLanguage']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLanguage = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLatitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLatitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLongitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLongitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPostalCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPostalCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPostalCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutInternationalPhoneNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outInternationalPhoneNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outInternationalPhoneNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutCallerID = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCallerID;
  }
  this.getOutCarrierName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCarrierName;
  }
  this.getOutPhoneNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNumber;
  }
  this.getOutCountry = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountry;
  }
  this.getOutError = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outError;
  }
  this.getOutVersion = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outVersion;
  }
  this.getOutTransmissionResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTransmissionResults;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutPhoneInternationalPrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneInternationalPrefix;
  }
  this.getOutPhoneCountryDialingCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneCountryDialingCode;
  }
  this.getOutPhoneNationPrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNationPrefix;
  }
  this.getOutPhoneNationalDestinationCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNationalDestinationCode;
  }
  this.getOutPhoneSubscriberNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneSubscriberNumber;
  }
  this.getOutLocality = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLocality;
  }
  this.getOutAdministrativeArea = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAdministrativeArea;
  }
  this.getOutDST = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDST;
  }
  this.getOutUTC = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUTC;
  }
  this.getOutLanguage = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLanguage;
  }
  this.getOutLatitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLatitude;
  }
  this.getOutLongitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLongitude;
  }
  this.getOutPostalCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPostalCode;
  }
  this.getOutInternationalPhoneNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outInternationalPhoneNumber;
  }

  //Set property values from responseXml constructor parameter
  _setOutCallerID(responseXml);
  _setOutCarrierName(responseXml);
  _setOutPhoneNumber(responseXml);
  _setOutCountry(responseXml);
  _setOutError(responseXml);
  _setOutVersion(responseXml);
  _setOutTransmissionResults(responseXml);
  _setOutResults(responseXml);
  _setOutPhoneInternationalPrefix(responseXml);
  _setOutPhoneCountryDialingCode(responseXml);
  _setOutPhoneNationPrefix(responseXml);
  _setOutPhoneNationalDestinationCode(responseXml);
  _setOutPhoneSubscriberNumber(responseXml);
  _setOutLocality(responseXml);
  _setOutAdministrativeArea(responseXml);
  _setOutDST(responseXml);
  _setOutUTC(responseXml);
  _setOutLanguage(responseXml);
  _setOutLatitude(responseXml);
  _setOutLongitude(responseXml);
  _setOutPostalCode(responseXml);
  _setOutInternationalPhoneNumber(responseXml);
 }
 this.md_PhoneVerifyResponse.__class = true;
}).call(Sdk)

Sdk.md_PhoneVerifyRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_PhoneVerifyResponse.prototype = new Sdk.OrganizationResponse();
