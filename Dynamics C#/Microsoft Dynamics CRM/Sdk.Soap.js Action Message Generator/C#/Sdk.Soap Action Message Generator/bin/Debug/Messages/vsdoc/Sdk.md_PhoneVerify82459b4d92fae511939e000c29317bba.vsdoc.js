"use strict";
(function () {
this.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest = function (
inCustomerID,
inOptions,
inPhoneNumber,
inCountry,
inCountryOfOrigin
)
{
///<summary>
/// 
///</summary>
///<param name="inCustomerID"  type="String">
/// [Add Description]
///</param>
///<param name="inOptions"  type="String">
/// [Add Description]
///</param>
///<param name="inPhoneNumber"  type="String">
/// [Add Description]
///</param>
///<param name="inCountry"  type="String">
/// [Add Description]
///</param>
///<param name="inCountryOfOrigin"  type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest)) {
return new Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest(inCustomerID, inOptions, inPhoneNumber, inCountry, inCountryOfOrigin);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _inCustomerID = null;
var _inOptions = null;
var _inPhoneNumber = null;
var _inCountry = null;
var _inCountryOfOrigin = null;

// internal validation functions

function _setValidInCustomerID(value) {
 if (typeof value == "string") {
  _inCustomerID = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest InCustomerID property is required and must be a String.")
 }
}

function _setValidInOptions(value) {
 if (typeof value == "string") {
  _inOptions = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest InOptions property is required and must be a String.")
 }
}

function _setValidInPhoneNumber(value) {
 if (typeof value == "string") {
  _inPhoneNumber = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest InPhoneNumber property is required and must be a String.")
 }
}

function _setValidInCountry(value) {
 if (typeof value == "string") {
  _inCountry = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest InCountry property is required and must be a String.")
 }
}

function _setValidInCountryOfOrigin(value) {
 if (typeof value == "string") {
  _inCountryOfOrigin = value;
 }
 else {
  throw new Error("Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest InCountryOfOrigin property is required and must be a String.")
 }
}

//Set internal properties from constructor parameters
  if (typeof inCustomerID != "undefined") {
   _setValidInCustomerID(inCustomerID);
  }
  if (typeof inOptions != "undefined") {
   _setValidInOptions(inOptions);
  }
  if (typeof inPhoneNumber != "undefined") {
   _setValidInPhoneNumber(inPhoneNumber);
  }
  if (typeof inCountry != "undefined") {
   _setValidInCountry(inCountry);
  }
  if (typeof inCountryOfOrigin != "undefined") {
   _setValidInCountryOfOrigin(inCountryOfOrigin);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCustomerID</b:key>",
           (_inCustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inOptions</b:key>",
           (_inOptions == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inOptions, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPhoneNumber</b:key>",
           (_inPhoneNumber == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPhoneNumber, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountry</b:key>",
           (_inCountry == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountry, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountryOfOrigin</b:key>",
           (_inCountryOfOrigin == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountryOfOrigin, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_PhoneVerify82459b4d92fae511939e000c29317bba</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setInCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPhoneNumber = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPhoneNumber(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountry(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountryOfOrigin = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountryOfOrigin(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest.__class = true;

this.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse = function (responseXml) {
  ///<summary>
  /// Response to md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest
  ///</summary>
  if (!(this instanceof Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse)) {
   return new Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outPhoneNumber = null;
  var _outCountry = null;
  var _outError = null;
  var _outVersion = null;
  var _outTransmissionResults = null;
  var _outResults = null;
  var _outPhoneInternationalPrefix = null;
  var _outPhoneCountryDialingCode = null;
  var _outPhoneNationPrefix = null;
  var _outPhoneNationalDestinationCode = null;
  var _outPhoneSubscriberNumber = null;
  var _outLocality = null;
  var _outAdministrativeArea = null;
  var _outDST = null;
  var _outUTC = null;
  var _outLanguage = null;
  var _outLatitude = null;
  var _outLongitude = null;

  // Internal property setter functions

  function _setOutPhoneNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountry(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountry']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountry = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutError(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outError = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutVersion(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outVersion']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outVersion = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTransmissionResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTransmissionResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTransmissionResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneInternationalPrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneInternationalPrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneInternationalPrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneCountryDialingCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneCountryDialingCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneCountryDialingCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNationPrefix(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNationPrefix']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNationPrefix = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneNationalDestinationCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneNationalDestinationCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneNationalDestinationCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPhoneSubscriberNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPhoneSubscriberNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPhoneSubscriberNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLocality(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLocality']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLocality = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAdministrativeArea(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAdministrativeArea']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAdministrativeArea = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDST(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDST']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDST = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutUTC(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outUTC']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outUTC = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLanguage(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLanguage']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLanguage = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLatitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLatitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLongitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLongitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutPhoneNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNumber;
  }
  this.getOutCountry = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountry;
  }
  this.getOutError = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outError;
  }
  this.getOutVersion = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outVersion;
  }
  this.getOutTransmissionResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTransmissionResults;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutPhoneInternationalPrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneInternationalPrefix;
  }
  this.getOutPhoneCountryDialingCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneCountryDialingCode;
  }
  this.getOutPhoneNationPrefix = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNationPrefix;
  }
  this.getOutPhoneNationalDestinationCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneNationalDestinationCode;
  }
  this.getOutPhoneSubscriberNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPhoneSubscriberNumber;
  }
  this.getOutLocality = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLocality;
  }
  this.getOutAdministrativeArea = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAdministrativeArea;
  }
  this.getOutDST = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDST;
  }
  this.getOutUTC = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outUTC;
  }
  this.getOutLanguage = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLanguage;
  }
  this.getOutLatitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLatitude;
  }
  this.getOutLongitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLongitude;
  }

  //Set property values from responseXml constructor parameter
  _setOutPhoneNumber(responseXml);
  _setOutCountry(responseXml);
  _setOutError(responseXml);
  _setOutVersion(responseXml);
  _setOutTransmissionResults(responseXml);
  _setOutResults(responseXml);
  _setOutPhoneInternationalPrefix(responseXml);
  _setOutPhoneCountryDialingCode(responseXml);
  _setOutPhoneNationPrefix(responseXml);
  _setOutPhoneNationalDestinationCode(responseXml);
  _setOutPhoneSubscriberNumber(responseXml);
  _setOutLocality(responseXml);
  _setOutAdministrativeArea(responseXml);
  _setOutDST(responseXml);
  _setOutUTC(responseXml);
  _setOutLanguage(responseXml);
  _setOutLatitude(responseXml);
  _setOutLongitude(responseXml);
 }
 this.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse.__class = true;
}).call(Sdk)

Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_PhoneVerify82459b4d92fae511939e000c29317bbaResponse.prototype = new Sdk.OrganizationResponse();
