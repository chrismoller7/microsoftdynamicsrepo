"use strict";
(function () {
this.new_ValidateGlobalAddressRequest = function (
customerID,
options,
inOrganization,
inAddressLine1,
inAddressLine2,
inAddressLine3,
inAddressLine4,
inAddressLine5,
inAddressLine6,
inAddressLine7,
inAddressLine8,
inDoubleDependentLocality,
inDependentLocality,
inLocality,
inSubAdministrativeArea,
inAdministrativeArea,
inSubNationalArea,
inPostalCode,
inCountry
)
{
///<summary>
/// 
///</summary>
///<param name="customerID"  type="String">
/// [Add Description]
///</param>
///<param name="options"  type="String">
/// [Add Description]
///</param>
///<param name="inOrganization" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine1"  type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine2" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine3" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine4" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine5" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine6" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine7" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAddressLine8" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inDoubleDependentLocality" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inDependentLocality" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inLocality" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inSubAdministrativeArea" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inAdministrativeArea" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inSubNationalArea" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inPostalCode" optional="true" type="String">
/// [Add Description]
///</param>
///<param name="inCountry"  type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.new_ValidateGlobalAddressRequest)) {
return new Sdk.new_ValidateGlobalAddressRequest(customerID, options, inOrganization, inAddressLine1, inAddressLine2, inAddressLine3, inAddressLine4, inAddressLine5, inAddressLine6, inAddressLine7, inAddressLine8, inDoubleDependentLocality, inDependentLocality, inLocality, inSubAdministrativeArea, inAdministrativeArea, inSubNationalArea, inPostalCode, inCountry);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _CustomerID = null;
var _Options = null;
var _inOrganization = null;
var _inAddressLine1 = null;
var _inAddressLine2 = null;
var _inAddressLine3 = null;
var _inAddressLine4 = null;
var _inAddressLine5 = null;
var _inAddressLine6 = null;
var _inAddressLine7 = null;
var _inAddressLine8 = null;
var _inDoubleDependentLocality = null;
var _inDependentLocality = null;
var _inLocality = null;
var _inSubAdministrativeArea = null;
var _inAdministrativeArea = null;
var _inSubNationalArea = null;
var _inPostalCode = null;
var _inCountry = null;

// internal validation functions

function _setValidCustomerID(value) {
 if (typeof value == "string") {
  _CustomerID = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest CustomerID property is required and must be a String.")
 }
}

function _setValidOptions(value) {
 if (typeof value == "string") {
  _Options = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest Options property is required and must be a String.")
 }
}

function _setValidInOrganization(value) {
 if (value == null || typeof value == "string") {
  _inOrganization = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InOrganization property must be a String or null.")
 }
}

function _setValidInAddressLine1(value) {
 if (typeof value == "string") {
  _inAddressLine1 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine1 property is required and must be a String.")
 }
}

function _setValidInAddressLine2(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine2 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine2 property must be a String or null.")
 }
}

function _setValidInAddressLine3(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine3 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine3 property must be a String or null.")
 }
}

function _setValidInAddressLine4(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine4 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine4 property must be a String or null.")
 }
}

function _setValidInAddressLine5(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine5 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine5 property must be a String or null.")
 }
}

function _setValidInAddressLine6(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine6 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine6 property must be a String or null.")
 }
}

function _setValidInAddressLine7(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine7 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine7 property must be a String or null.")
 }
}

function _setValidInAddressLine8(value) {
 if (value == null || typeof value == "string") {
  _inAddressLine8 = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAddressLine8 property must be a String or null.")
 }
}

function _setValidInDoubleDependentLocality(value) {
 if (value == null || typeof value == "string") {
  _inDoubleDependentLocality = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InDoubleDependentLocality property must be a String or null.")
 }
}

function _setValidInDependentLocality(value) {
 if (value == null || typeof value == "string") {
  _inDependentLocality = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InDependentLocality property must be a String or null.")
 }
}

function _setValidInLocality(value) {
 if (value == null || typeof value == "string") {
  _inLocality = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InLocality property must be a String or null.")
 }
}

function _setValidInSubAdministrativeArea(value) {
 if (value == null || typeof value == "string") {
  _inSubAdministrativeArea = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InSubAdministrativeArea property must be a String or null.")
 }
}

function _setValidInAdministrativeArea(value) {
 if (value == null || typeof value == "string") {
  _inAdministrativeArea = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InAdministrativeArea property must be a String or null.")
 }
}

function _setValidInSubNationalArea(value) {
 if (value == null || typeof value == "string") {
  _inSubNationalArea = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InSubNationalArea property must be a String or null.")
 }
}

function _setValidInPostalCode(value) {
 if (value == null || typeof value == "string") {
  _inPostalCode = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InPostalCode property must be a String or null.")
 }
}

function _setValidInCountry(value) {
 if (typeof value == "string") {
  _inCountry = value;
 }
 else {
  throw new Error("Sdk.new_ValidateGlobalAddressRequest InCountry property is required and must be a String.")
 }
}

//Set internal properties from constructor parameters
  if (typeof customerID != "undefined") {
   _setValidCustomerID(customerID);
  }
  if (typeof options != "undefined") {
   _setValidOptions(options);
  }
  if (typeof inOrganization != "undefined") {
   _setValidInOrganization(inOrganization);
  }
  if (typeof inAddressLine1 != "undefined") {
   _setValidInAddressLine1(inAddressLine1);
  }
  if (typeof inAddressLine2 != "undefined") {
   _setValidInAddressLine2(inAddressLine2);
  }
  if (typeof inAddressLine3 != "undefined") {
   _setValidInAddressLine3(inAddressLine3);
  }
  if (typeof inAddressLine4 != "undefined") {
   _setValidInAddressLine4(inAddressLine4);
  }
  if (typeof inAddressLine5 != "undefined") {
   _setValidInAddressLine5(inAddressLine5);
  }
  if (typeof inAddressLine6 != "undefined") {
   _setValidInAddressLine6(inAddressLine6);
  }
  if (typeof inAddressLine7 != "undefined") {
   _setValidInAddressLine7(inAddressLine7);
  }
  if (typeof inAddressLine8 != "undefined") {
   _setValidInAddressLine8(inAddressLine8);
  }
  if (typeof inDoubleDependentLocality != "undefined") {
   _setValidInDoubleDependentLocality(inDoubleDependentLocality);
  }
  if (typeof inDependentLocality != "undefined") {
   _setValidInDependentLocality(inDependentLocality);
  }
  if (typeof inLocality != "undefined") {
   _setValidInLocality(inLocality);
  }
  if (typeof inSubAdministrativeArea != "undefined") {
   _setValidInSubAdministrativeArea(inSubAdministrativeArea);
  }
  if (typeof inAdministrativeArea != "undefined") {
   _setValidInAdministrativeArea(inAdministrativeArea);
  }
  if (typeof inSubNationalArea != "undefined") {
   _setValidInSubNationalArea(inSubNationalArea);
  }
  if (typeof inPostalCode != "undefined") {
   _setValidInPostalCode(inPostalCode);
  }
  if (typeof inCountry != "undefined") {
   _setValidInCountry(inCountry);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>CustomerID</b:key>",
           (_CustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _CustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>Options</b:key>",
           (_Options == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _Options, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inOrganization</b:key>",
           (_inOrganization == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inOrganization, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine1</b:key>",
           (_inAddressLine1 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine1, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine2</b:key>",
           (_inAddressLine2 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine2, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine3</b:key>",
           (_inAddressLine3 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine3, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine4</b:key>",
           (_inAddressLine4 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine4, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine5</b:key>",
           (_inAddressLine5 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine5, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine6</b:key>",
           (_inAddressLine6 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine6, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine7</b:key>",
           (_inAddressLine7 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine7, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAddressLine8</b:key>",
           (_inAddressLine8 == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAddressLine8, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inDoubleDependentLocality</b:key>",
           (_inDoubleDependentLocality == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inDoubleDependentLocality, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inDependentLocality</b:key>",
           (_inDependentLocality == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inDependentLocality, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inLocality</b:key>",
           (_inLocality == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inLocality, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inSubAdministrativeArea</b:key>",
           (_inSubAdministrativeArea == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inSubAdministrativeArea, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inAdministrativeArea</b:key>",
           (_inAdministrativeArea == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inAdministrativeArea, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inSubNationalArea</b:key>",
           (_inSubNationalArea == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inSubNationalArea, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inPostalCode</b:key>",
           (_inPostalCode == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inPostalCode, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCountry</b:key>",
           (_inCountry == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCountry, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>new_ValidateGlobalAddress</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.new_ValidateGlobalAddressResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInOrganization = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInOrganization(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine1 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine1(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine2 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine2(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine3 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine3(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine4 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine4(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine5 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine5(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine6 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine6(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine7 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine7(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAddressLine8 = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAddressLine8(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInDoubleDependentLocality = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInDoubleDependentLocality(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInDependentLocality = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInDependentLocality(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInLocality = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInLocality(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInSubAdministrativeArea = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInSubAdministrativeArea(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInAdministrativeArea = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInAdministrativeArea(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInSubNationalArea = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInSubNationalArea(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInPostalCode = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInPostalCode(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInCountry = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCountry(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.new_ValidateGlobalAddressRequest.__class = true;

this.new_ValidateGlobalAddressResponse = function (responseXml) {
  ///<summary>
  /// Response to new_ValidateGlobalAddressRequest
  ///</summary>
  if (!(this instanceof Sdk.new_ValidateGlobalAddressResponse)) {
   return new Sdk.new_ValidateGlobalAddressResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _errorMessage = null;
  var _outOrganization = null;
  var _outAddressLine1 = null;
  var _outAddressLine2 = null;
  var _outAddressLine3 = null;
  var _outAddressLine4 = null;
  var _outAddressLine5 = null;
  var _outAddressLine6 = null;
  var _outAddressLine7 = null;
  var _outAddressLine8 = null;
  var _outDoubleDependentLocality = null;
  var _outDependentLocality = null;
  var _outLocality = null;
  var _outSubAdministrativeArea = null;
  var _outAdministrativeArea = null;
  var _outSubNationalArea = null;
  var _outPostalCode = null;
  var _outCountryName = null;
  var _outResults = null;
  var _outFormattedAddress = null;
  var _outBuilding = null;
  var _outDependentThoroughfare = null;
  var _outDependentThoroughfareLeadingType = null;
  var _outDependentThoroughfareName = null;
  var _outDependentThoroughfarePostDirection = null;
  var _outDependentThoroughfarePreDirection = null;
  var _outDependentThoroughfareTrailingType = null;
  var _outISO2Code = null;
  var _outISO3Code = null;
  var _outISONumCode = null;
  var _outPostBox = null;
  var _outPremisesNumber = null;
  var _outPremisesType = null;
  var _outSubPremises = null;
  var _outSubPremisesType = null;
  var _outSubPremisesNumber = null;
  var _outThoroughfare = null;
  var _outThoroughfareLeadingType = null;
  var _outThoroughfareName = null;
  var _outThoroughfarePostDirection = null;
  var _outThoroughfarePreDirection = null;
  var _outThoroughfareTrailingType = null;
  var _outLatitude = null;
  var _outLongitude = null;

  // Internal property setter functions

  function _setErrorMessage(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='ErrorMessage']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _errorMessage = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutOrganization(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outOrganization']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outOrganization = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine1(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine1']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine1 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine2(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine2']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine2 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine3(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine3']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine3 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine4(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine4']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine4 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine5(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine5']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine5 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine6(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine6']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine6 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine7(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine7']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine7 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAddressLine8(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAddressLine8']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAddressLine8 = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDoubleDependentLocality(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDoubleDependentLocality']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDoubleDependentLocality = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentLocality(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentLocality']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentLocality = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLocality(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLocality']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLocality = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSubAdministrativeArea(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSubAdministrativeArea']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSubAdministrativeArea = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutAdministrativeArea(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outAdministrativeArea']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outAdministrativeArea = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSubNationalArea(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSubNationalArea']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSubNationalArea = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPostalCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPostalCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPostalCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutCountryName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outCountryName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outCountryName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutFormattedAddress(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outFormattedAddress']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outFormattedAddress = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutBuilding(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outBuilding']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outBuilding = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfare(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfare']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfare = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfareLeadingType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfareLeadingType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfareLeadingType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfareName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfareName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfareName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfarePostDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfarePostDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfarePostDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfarePreDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfarePreDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfarePreDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDependentThoroughfareTrailingType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDependentThoroughfareTrailingType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDependentThoroughfareTrailingType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutISO2Code(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outISO2Code']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outISO2Code = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutISO3Code(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outISO3Code']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outISO3Code = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutISONumCode(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outISONumCode']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outISONumCode = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPostBox(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPostBox']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPostBox = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPremisesNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPremisesNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPremisesNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutPremisesType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outPremisesType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outPremisesType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSubPremises(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSubPremises']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSubPremises = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSubPremisesType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSubPremisesType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSubPremisesType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutSubPremisesNumber(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outSubPremisesNumber']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outSubPremisesNumber = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfare(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfare']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfare = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfareLeadingType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfareLeadingType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfareLeadingType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfareName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfareName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfareName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfarePostDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfarePostDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfarePostDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfarePreDirection(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfarePreDirection']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfarePreDirection = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutThoroughfareTrailingType(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outThoroughfareTrailingType']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outThoroughfareTrailingType = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLatitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLatitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLatitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutLongitude(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outLongitude']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outLongitude = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getErrorMessage = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _errorMessage;
  }
  this.getOutOrganization = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outOrganization;
  }
  this.getOutAddressLine1 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine1;
  }
  this.getOutAddressLine2 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine2;
  }
  this.getOutAddressLine3 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine3;
  }
  this.getOutAddressLine4 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine4;
  }
  this.getOutAddressLine5 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine5;
  }
  this.getOutAddressLine6 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine6;
  }
  this.getOutAddressLine7 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine7;
  }
  this.getOutAddressLine8 = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAddressLine8;
  }
  this.getOutDoubleDependentLocality = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDoubleDependentLocality;
  }
  this.getOutDependentLocality = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentLocality;
  }
  this.getOutLocality = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLocality;
  }
  this.getOutSubAdministrativeArea = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSubAdministrativeArea;
  }
  this.getOutAdministrativeArea = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outAdministrativeArea;
  }
  this.getOutSubNationalArea = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSubNationalArea;
  }
  this.getOutPostalCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPostalCode;
  }
  this.getOutCountryName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outCountryName;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutFormattedAddress = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outFormattedAddress;
  }
  this.getOutBuilding = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outBuilding;
  }
  this.getOutDependentThoroughfare = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfare;
  }
  this.getOutDependentThoroughfareLeadingType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfareLeadingType;
  }
  this.getOutDependentThoroughfareName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfareName;
  }
  this.getOutDependentThoroughfarePostDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfarePostDirection;
  }
  this.getOutDependentThoroughfarePreDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfarePreDirection;
  }
  this.getOutDependentThoroughfareTrailingType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDependentThoroughfareTrailingType;
  }
  this.getOutISO2Code = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outISO2Code;
  }
  this.getOutISO3Code = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outISO3Code;
  }
  this.getOutISONumCode = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outISONumCode;
  }
  this.getOutPostBox = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPostBox;
  }
  this.getOutPremisesNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPremisesNumber;
  }
  this.getOutPremisesType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outPremisesType;
  }
  this.getOutSubPremises = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSubPremises;
  }
  this.getOutSubPremisesType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSubPremisesType;
  }
  this.getOutSubPremisesNumber = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outSubPremisesNumber;
  }
  this.getOutThoroughfare = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfare;
  }
  this.getOutThoroughfareLeadingType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfareLeadingType;
  }
  this.getOutThoroughfareName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfareName;
  }
  this.getOutThoroughfarePostDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfarePostDirection;
  }
  this.getOutThoroughfarePreDirection = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfarePreDirection;
  }
  this.getOutThoroughfareTrailingType = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outThoroughfareTrailingType;
  }
  this.getOutLatitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLatitude;
  }
  this.getOutLongitude = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outLongitude;
  }

  //Set property values from responseXml constructor parameter
  _setErrorMessage(responseXml);
  _setOutOrganization(responseXml);
  _setOutAddressLine1(responseXml);
  _setOutAddressLine2(responseXml);
  _setOutAddressLine3(responseXml);
  _setOutAddressLine4(responseXml);
  _setOutAddressLine5(responseXml);
  _setOutAddressLine6(responseXml);
  _setOutAddressLine7(responseXml);
  _setOutAddressLine8(responseXml);
  _setOutDoubleDependentLocality(responseXml);
  _setOutDependentLocality(responseXml);
  _setOutLocality(responseXml);
  _setOutSubAdministrativeArea(responseXml);
  _setOutAdministrativeArea(responseXml);
  _setOutSubNationalArea(responseXml);
  _setOutPostalCode(responseXml);
  _setOutCountryName(responseXml);
  _setOutResults(responseXml);
  _setOutFormattedAddress(responseXml);
  _setOutBuilding(responseXml);
  _setOutDependentThoroughfare(responseXml);
  _setOutDependentThoroughfareLeadingType(responseXml);
  _setOutDependentThoroughfareName(responseXml);
  _setOutDependentThoroughfarePostDirection(responseXml);
  _setOutDependentThoroughfarePreDirection(responseXml);
  _setOutDependentThoroughfareTrailingType(responseXml);
  _setOutISO2Code(responseXml);
  _setOutISO3Code(responseXml);
  _setOutISONumCode(responseXml);
  _setOutPostBox(responseXml);
  _setOutPremisesNumber(responseXml);
  _setOutPremisesType(responseXml);
  _setOutSubPremises(responseXml);
  _setOutSubPremisesType(responseXml);
  _setOutSubPremisesNumber(responseXml);
  _setOutThoroughfare(responseXml);
  _setOutThoroughfareLeadingType(responseXml);
  _setOutThoroughfareName(responseXml);
  _setOutThoroughfarePostDirection(responseXml);
  _setOutThoroughfarePreDirection(responseXml);
  _setOutThoroughfareTrailingType(responseXml);
  _setOutLatitude(responseXml);
  _setOutLongitude(responseXml);
 }
 this.new_ValidateGlobalAddressResponse.__class = true;
}).call(Sdk)

Sdk.new_ValidateGlobalAddressRequest.prototype = new Sdk.OrganizationRequest();
Sdk.new_ValidateGlobalAddressResponse.prototype = new Sdk.OrganizationResponse();
