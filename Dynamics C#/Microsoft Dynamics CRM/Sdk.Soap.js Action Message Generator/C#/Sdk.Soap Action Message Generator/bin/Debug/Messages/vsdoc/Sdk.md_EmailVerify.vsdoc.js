"use strict";
(function () {
this.md_EmailVerifyRequest = function (
inCustomerID,
inOptions,
inEmail
)
{
///<summary>
/// 
///</summary>
///<param name="inCustomerID"  type="String">
/// [Add Description]
///</param>
///<param name="inOptions"  type="String">
/// [Add Description]
///</param>
///<param name="inEmail"  type="String">
/// [Add Description]
///</param>
if (!(this instanceof Sdk.md_EmailVerifyRequest)) {
return new Sdk.md_EmailVerifyRequest(inCustomerID, inOptions, inEmail);
}
Sdk.OrganizationRequest.call(this);

  // Internal properties
var _inCustomerID = null;
var _inOptions = null;
var _inEmail = null;

// internal validation functions

function _setValidInCustomerID(value) {
 if (typeof value == "string") {
  _inCustomerID = value;
 }
 else {
  throw new Error("Sdk.md_EmailVerifyRequest InCustomerID property is required and must be a String.")
 }
}

function _setValidInOptions(value) {
 if (typeof value == "string") {
  _inOptions = value;
 }
 else {
  throw new Error("Sdk.md_EmailVerifyRequest InOptions property is required and must be a String.")
 }
}

function _setValidInEmail(value) {
 if (typeof value == "string") {
  _inEmail = value;
 }
 else {
  throw new Error("Sdk.md_EmailVerifyRequest InEmail property is required and must be a String.")
 }
}

//Set internal properties from constructor parameters
  if (typeof inCustomerID != "undefined") {
   _setValidInCustomerID(inCustomerID);
  }
  if (typeof inOptions != "undefined") {
   _setValidInOptions(inOptions);
  }
  if (typeof inEmail != "undefined") {
   _setValidInEmail(inEmail);
  }

  function getRequestXml() {
return ["<d:request>",
        "<a:Parameters>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inCustomerID</b:key>",
           (_inCustomerID == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inCustomerID, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inOptions</b:key>",
           (_inOptions == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inOptions, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

          "<a:KeyValuePairOfstringanyType>",
            "<b:key>inEmail</b:key>",
           (_inEmail == null) ? "<b:value i:nil=\"true\" />" :
           ["<b:value i:type=\"c:string\">", _inEmail, "</b:value>"].join(""),
          "</a:KeyValuePairOfstringanyType>",

        "</a:Parameters>",
        "<a:RequestId i:nil=\"true\" />",
        "<a:RequestName>md_EmailVerify</a:RequestName>",
      "</d:request>"].join("");
  }

  this.setResponseType(Sdk.md_EmailVerifyResponse);
  this.setRequestXml(getRequestXml());

  // Public methods to set properties
  this.setInCustomerID = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInCustomerID(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInOptions = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInOptions(value);
   this.setRequestXml(getRequestXml());
  }

  this.setInEmail = function (value) {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<param name="value" type="String">
  /// [Add Description]
  ///</param>
   _setValidInEmail(value);
   this.setRequestXml(getRequestXml());
  }

 }
 this.md_EmailVerifyRequest.__class = true;

this.md_EmailVerifyResponse = function (responseXml) {
  ///<summary>
  /// Response to md_EmailVerifyRequest
  ///</summary>
  if (!(this instanceof Sdk.md_EmailVerifyResponse)) {
   return new Sdk.md_EmailVerifyResponse(responseXml);
  }
  Sdk.OrganizationResponse.call(this)

  // Internal properties
  var _outEmail = null;
  var _outError = null;
  var _outTransmissionResults = null;
  var _outDomainName = null;
  var _outMailBoxName = null;
  var _outRecordID = null;
  var _outResults = null;
  var _outTopLevelDomain = null;
  var _outTopLevelDomainName = null;

  // Internal property setter functions

  function _setOutEmail(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outEmail']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outEmail = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutError(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outError']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outError = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTransmissionResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTransmissionResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTransmissionResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutDomainName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outDomainName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outDomainName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutMailBoxName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outMailBoxName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outMailBoxName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutRecordID(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outRecordID']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outRecordID = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutResults(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outResults']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outResults = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTopLevelDomain(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTopLevelDomain']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTopLevelDomain = Sdk.Xml.getNodeText(valueNode);
   }
  }
  function _setOutTopLevelDomainName(xml) {
   var valueNode = Sdk.Xml.selectSingleNode(xml, "//a:KeyValuePairOfstringanyType[b:key='outTopLevelDomainName']/b:value");
   if (!Sdk.Xml.isNodeNull(valueNode)) {
    _outTopLevelDomainName = Sdk.Xml.getNodeText(valueNode);
   }
  }
  //Public Methods to retrieve properties
  this.getOutEmail = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outEmail;
  }
  this.getOutError = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outError;
  }
  this.getOutTransmissionResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTransmissionResults;
  }
  this.getOutDomainName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outDomainName;
  }
  this.getOutMailBoxName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outMailBoxName;
  }
  this.getOutRecordID = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outRecordID;
  }
  this.getOutResults = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outResults;
  }
  this.getOutTopLevelDomain = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTopLevelDomain;
  }
  this.getOutTopLevelDomainName = function () {
  ///<summary>
  /// [Add Description]
  ///</summary>
  ///<returns type="String">
  /// [Add Description]
  ///</returns>
   return _outTopLevelDomainName;
  }

  //Set property values from responseXml constructor parameter
  _setOutEmail(responseXml);
  _setOutError(responseXml);
  _setOutTransmissionResults(responseXml);
  _setOutDomainName(responseXml);
  _setOutMailBoxName(responseXml);
  _setOutRecordID(responseXml);
  _setOutResults(responseXml);
  _setOutTopLevelDomain(responseXml);
  _setOutTopLevelDomainName(responseXml);
 }
 this.md_EmailVerifyResponse.__class = true;
}).call(Sdk)

Sdk.md_EmailVerifyRequest.prototype = new Sdk.OrganizationRequest();
Sdk.md_EmailVerifyResponse.prototype = new Sdk.OrganizationResponse();
