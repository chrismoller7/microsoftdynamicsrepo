This folder contains the most recent .sln files for Business Coder, Global, and Personator given to Chris M. by Michael B. before he left.

These are used by the PluginRegistration.exe found in the Microsoft Dynamics CRM folder.  To get to the .exe, refer to the ReadMe in that folder.

(MAY NEED TO UPDATE)
To attach the proper file using PluginRegistration.exe:
	1. Select desired solution folder (Business Coder/Global/Personator)
	2. C# (or C#3 for Personator)
	3a. (THIS IS FOR BUSINESS CODER AND PERSONATOR) Select folder that IS NOT the .sln file.
	4a. Select bin folder
	5a. Select Debug folder
	6a. Select the .dll with the name corresponding to the solution (ex: BusinessCoderCRM.dll or DynCRMPersonatorVerifyREST.dll)
	3b. (THIS IS FOR GLOBAL) Select folder for desired Global function (Address -> AddVerifyActivity, Email, Name, or Phone)
	4b. Follow steps 3a -> 6a.