﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DynCRMGlobalPhone
{
    [DataContract]
    public class GPhoneRecords
    {
        //Structure of the response Records element

        [DataMember(Name = "RecordID", IsRequired = true)]
        public string RecordID { get; set; }

        [DataMember(Name = "Results", IsRequired = true)]
        public string Results { get; set; }

        [DataMember(Name = "PhoneNumber", IsRequired = true)]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "AdministrativeArea", IsRequired = true)]
        public string AdministrativeArea { get; set; }

        [DataMember(Name = "CountryAbbreviation", IsRequired = true)]
        public string CountryAbbreviation { get; set; }
        
        [DataMember(Name = "CountryName", IsRequired = true)]
        public string CountryName { get; set; }

        [DataMember(Name = "Carrier", IsRequired = true)]
        public string Carrier { get; set; }

        [DataMember(Name = "DST", IsRequired = true)]
        public string DST { get; set; }

        [DataMember(Name = "Language", IsRequired = true)]
        public string Language { get; set; }

        [DataMember(Name = "InternationalPhoneNumber", IsRequired = true)]
        public string InternationalPhoneNumber { get; set; }

        [DataMember(Name = "Latitude", IsRequired = true)]
        public string Latitude { get; set; }

        [DataMember(Name = "Locality", IsRequired = true)]
        public string Locality { get; set; }

        [DataMember(Name = "Longitude", IsRequired = true)]
        public string Longitude { get; set; }

        [DataMember(Name = "PhoneInternationalPrefix", IsRequired = true)]
        public string PhoneInternationalPrefix { get; set; }

        [DataMember(Name = "PhoneCountryDialingCode", IsRequired = true)]
        public string PhoneCountryDialingCode { get; set; }

        [DataMember(Name = "PhoneNationPrefix", IsRequired = true)]
        public string PhoneNationPrefix { get; set; }

        [DataMember(Name = "PhoneNationalDestinationCode", IsRequired = true)]
        public string PhoneNationalDestinationCode { get; set; }

        [DataMember(Name = "PhoneSubscriberNumber", IsRequired = true)]
        public string PhoneSubscriberNumber { get; set; }

        [DataMember(Name = "CallerID", IsRequired = false)]
        public string CallerID { get; set; }

        [DataMember(Name = "UTC", IsRequired = true)]
        public string UTC { get; set; }

        [DataMember(Name = "PostalCode", IsRequired = true)]
        public string PostalCode { get; set; }

        [DataMember(Name = "Suggestions", IsRequired = false)]
        public GPhoneSuggestions[] Suggestions { get; set; }

    }
}
