﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Diagnostics;
using System.ServiceModel;

namespace DynCRMGlobalPhone
{
    public class DynCRMGlobalPhone : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inPhoneNumber")]
        [Output("outPhoneNumber")]
        public InOutArgument<String> PhoneNumber { get; set; }

        [Input("inCountry")]
        [Output("outCountry")]
        public InOutArgument<String> Country { get; set; }

        [Input("inCountryOfOrigin")]
        public InArgument<String> CountryOfOrigin { get; set; }

        [Output("outAdministrativeArea")]
        public OutArgument<String> AdministrativeArea { get; set; }

        [Output("outDST")]
        public OutArgument<String> DST { get; set; }

        [Output("outLanguage")]
        public OutArgument<String> Language { get; set; }

        [Output("outLatitude")]
        public OutArgument<String> Latitude { get; set; }

        [Output("outLocality")]
        public OutArgument<String> Locality { get; set; }

        [Output("outLongitude")]
        public OutArgument<String> Longitude { get; set; }

        [Output("outPhoneCountryDialingCode")]
        public OutArgument<String> PhoneCountryDialingCode { get; set; }

        [Output("outPhoneInternationalPrefix")]
        public OutArgument<String> PhoneInternationalPrefix { get; set; }

        [Output("outPhoneNationalDestinationCode")]
        public OutArgument<String> PhoneNationalDestinationCode { get; set; }

        [Output("outPhoneNationPrefix")]
        public OutArgument<String> PhoneNationPrefix { get; set; }

        [Output("outPhoneSubscriberNumber")]
        public OutArgument<String> PhoneSubscriberNumber { get; set; }

        [Output("outCallerID")]
        public OutArgument<String> CallerID { get; set; }

        [Output("outRecordID")]
        public OutArgument<String> RecordID { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outUTC")]
        public OutArgument<String> UTC { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        [Output("outVersion")]
        public OutArgument<String> Version { get; set; }

        /* New Fields */
        //Input
        [Input("inDefault")]
        public InArgument<String> iDefault { get; set; }

        //Output
        [Output("outCarrierName")]
        public OutArgument<String> oCarrier { get; set; }

        [Output("outPostalCode")]
        public OutArgument<String> oPostalCode { get; set; }

        [Output("outInternationalPhoneNumber")]
        public OutArgument<String> oInternationalPhoneNumber { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            
            /* New Code */

            HttpWebRequest request;
            HttpWebResponse response;
            System.IO.Stream responseStream;
            System.IO.MemoryStream JSONStream;
            Uri address;

            String license = CustomerID.Get(context);
            //http://globalphone.melissadata.net/v4/WEB/GlobalPhone/doGlobalPhone?t=&id=103114769&opt=VERIFYPHONE:Express&phone=+14084161450
            String tServer = "http://globalphone.melissadata.net/v4/WEB/GlobalPhone/doGlobalPhone";

            //License to CustomerID generation
            HttpWebRequest tokenRequest;
            HttpWebResponse tokenResponse;
            System.IO.Stream tokenResponseStream;
            Uri tokenAddress;

            String tokenServer = "https://token.melissadata.net/v3/WEB/service.svc/QueryCustomerInfo?P=&K=&L=";
            license = Uri.EscapeDataString(license);
            tokenServer += license;
            tokenAddress = new Uri(tokenServer);
            tokenRequest = (HttpWebRequest)WebRequest.Create(tokenAddress);
            tokenResponse = (HttpWebResponse)tokenRequest.GetResponse();
            tokenResponseStream = tokenResponse.GetResponseStream();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(tokenResponseStream);

            XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("/QueryCustomerInfoResponse/CustomerId");
            String id = node.InnerText;

            tokenResponseStream.Close();
            tokenResponse.Close();

            StreamReader streamReader;

            //License input
            tServer += "?id=" + id;

            //Option input
            //This is where we need to modify the code to include premium
            /*String options = Options.Get(context);
            if (options.Equals("1"))
            {
                tServer += "&opt=VERIFYPHONE:Express";
            } else if(options.Equals("2"))
            {
                tServer += "&opt=VERIFYPHONE:Premium";
            }*/
            tServer += "&opt=" + Options.Get(context);

            //Phone input
            tServer += "&phone=" + PhoneNumber.Get(context);

            //Country input
            tServer += "&ctry=" + Country.Get(context);

            //CountryOfOrigin
            tServer += "&ctryOrg=" + CountryOfOrigin.Get(context);

            //DefaultCallingCode input
            tServer += "&defaultCallingCode=" + iDefault.Get(context);


            //Create URI and request

            //String outPhoneNumber = tServer;
            //PhoneNumber.Set(context, outPhoneNumber);
            address = new Uri(tServer);

            request = (HttpWebRequest)WebRequest.Create(address);
      

            //Grab response
            response = (HttpWebResponse)request.GetResponse();

            //Grab stream of response
            responseStream = response.GetResponseStream();
            System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(GPhoneResponse));
            GPhoneResponse outputData = (GPhoneResponse)jsonSerializer.ReadObject(responseStream);

            String outVersion = "";
                String outTransmissionResults = "";

                String outPhoneNumber = "";
                String outAdministrativeArea = "";
                String outCountryAbbreviation = "";
                String outCountryName = "";
                String outCarrierName = "";
                String outDST = "";
                String outLanguage = "";
                String outLatitude = "";
                String outLocality = "";
                String outLongitude = "";
                String outPhoneInternationalPrefix = "";
                String outPhoneCountryDialingCode = "";
                String outPhoneNationPrefix = "";
                String outPhoneNationalDestinationCode = "";
                String outPhoneSubscriberNumber = "";
                String outUTC = "";
                String outRecordID = "";
                String outResults = "";
                String outError = "";
                String outPostalCode = "";
                String outInternationalPhoneNumber = "";
                String outCallID = "";


            //Sample:
            //outputData.Records[0].Latitude;
            if (!(outputData.TransmissionResults.Length > 0))
            {
                

                outVersion = outputData.Version;

                outPhoneNumber = outputData.Records[0].PhoneNumber;
                outCountryName = outputData.Records[0].CountryName;
                outResults = outputData.Records[0].Results;
                outAdministrativeArea = outputData.Records[0].AdministrativeArea;
                //outCarrierName = outputData.Records[0].CarrierName;
                outDST = outputData.Records[0].DST;
                outLanguage = outputData.Records[0].Language;
                outLocality = outputData.Records[0].Locality;
                outLatitude = outputData.Records[0].Latitude;
                outLongitude = outputData.Records[0].Longitude;
                outUTC = outputData.Records[0].UTC;
                outPhoneNationPrefix = outputData.Records[0].PhoneNationPrefix;
                outPhoneInternationalPrefix = outputData.Records[0].PhoneInternationalPrefix;
                outPhoneCountryDialingCode = outputData.Records[0].PhoneCountryDialingCode;
                outPhoneNationalDestinationCode = outputData.Records[0].PhoneNationalDestinationCode;
                outPhoneSubscriberNumber = outputData.Records[0].PhoneSubscriberNumber;

                outPostalCode = outputData.Records[0].PostalCode;
                outInternationalPhoneNumber = outputData.Records[0].InternationalPhoneNumber;

                outCallID = outputData.Records[0].CallerID;

                //Metadata
                //Version.Set(context, outVersion);
                //TransmissionResults.Set(context, outTransmissionResults);


            }
            else
            {

                outError = outputData.TransmissionResults;
                //Results.Set(context, "GE08");
            }

            //Phone results
            //Not included as of v4: Carrier Name, 
            //outPhoneNumber = tServer;
            PhoneNumber.Set(context, outPhoneNumber);
            AdministrativeArea.Set(context, outAdministrativeArea);
            Country.Set(context, outCountryName);
            DST.Set(context, outDST);
            Language.Set(context, outLanguage);
            Latitude.Set(context, outLatitude);
            Locality.Set(context, outLocality);
            Longitude.Set(context, outLongitude);
            PhoneCountryDialingCode.Set(context, outPhoneCountryDialingCode);
            PhoneInternationalPrefix.Set(context, outPhoneInternationalPrefix);
            PhoneNationalDestinationCode.Set(context, outPhoneNationalDestinationCode);
            PhoneNationPrefix.Set(context, outPhoneNationPrefix);
            PhoneSubscriberNumber.Set(context, outPhoneSubscriberNumber);
            RecordID.Set(context, outRecordID);
            Results.Set(context, outResults);
            UTC.Set(context, outUTC);
            Error.Set(context, outError);
            Version.Set(context, outVersion);
            TransmissionResults.Set(context, outTransmissionResults);
            oCarrier.Set(context, outCarrierName);

            oInternationalPhoneNumber.Set(context, outInternationalPhoneNumber);
            oPostalCode.Set(context, outPostalCode);

            CallerID.Set(context, outCallID);
        }
    }
}
