﻿using System;
using System.Runtime.Serialization;

namespace DynCRMGlobalPhone
{
    [DataContract]

    public class GPhoneResponse
    {
        //Structure of the response

        [DataMember(Name = "Version", IsRequired = true)]
        public string Version { get; set; }

        [DataMember(Name = "TransmissionReference", IsRequired = true)]
        public string TransmissionReference { get; set; }

        [DataMember(Name = "TransmissionResults", IsRequired = true)]
        public string TransmissionResults { get; set; }

        [DataMember(Name = "Records", IsRequired = false)]
        public GPhoneRecords[] Records { get; set; }
    }
}

