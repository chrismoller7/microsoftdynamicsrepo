﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;
using System.Web;

namespace DynCRMGlobalAddVerifyActivity
{
    public class DynCRMGlobalAddVerifyActivity : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inOrganization")]
        [Output("outOrganization")]
        public InOutArgument<String> Organization { get; set; }

        [Input("inAddressLine1")]
        [Output("outAddressLine1")]
        [RequiredArgument]
        public InOutArgument<String> AddressLine1 { get; set; }

        [Input("inAddressLine2")]
        [Output("outAddressLine2")]
        public InOutArgument<String> AddressLine2 { get; set; }

        [Input("inAddressLine3")]
        [Output("outAddressLine3")]
        public InOutArgument<String> AddressLine3 { get; set; }

        [Input("inAddressLine4")]
        [Output("outAddressLine4")]
        public InOutArgument<String> AddressLine4 { get; set; }

        [Input("inAddressLine5")]
        [Output("outAddressLine5")]
        public InOutArgument<String> AddressLine5 { get; set; }

        [Input("inAddressLine6")]
        [Output("outAddressLine6")]
        public InOutArgument<String> AddressLine6 { get; set; }

        [Input("inAddressLine7")]
        [Output("outAddressLine7")]
        public InOutArgument<String> AddressLine7 { get; set; }

        [Input("inAddressLine8")]
        [Output("outAddressLine8")]
        public InOutArgument<String> AddressLine8 { get; set; }

        [Input("inDoubleDependentLocality")]
        [Output("outDoubleDependentLocality")]
        public InOutArgument<String> DoubleDependentLocality { get; set; }
        
        [Input("inDependentLocality")]
        [Output("outDependentLocality")]
        public InOutArgument<String> DependentLocality { get; set; }

        [Input("inLocality")]
        [Output("outLocality")]
        public InOutArgument<String> Locality { get; set; }

        [Input("inSubAdministrativeArea")]
        [Output("outSubAdministrativeArea")]
        public InOutArgument<String> SubAdministrativeArea { get; set; }

        [Input("inAdministrativeArea")]
        [Output("outAdministrativeArea")]
        public InOutArgument<String> AdministrativeArea { get; set; }

        [Input("inSubNationalArea")]
        [Output("outSubNationalArea")]
        public InOutArgument<String> SubNationalArea { get; set; }

        [Input("inPostalCode")]
        [Output("outPostalCode")]
        public InOutArgument<String> PostalCode { get; set; }

        [Input("inCountry")]
        [RequiredArgument]
        public InArgument<String> Country { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outFormattedAddress")]
        public OutArgument<String> FormattedAddress { get; set; }

        [Output("outBuilding")]
        public OutArgument<String> Building { get; set; }
        
        [Output("outCountryName")]
        public OutArgument<String> CountryName { get; set; }

        [Output("outDependentThoroughfare")]
        public OutArgument<String> DependentThoroughfare { get; set; }

        [Output("outDependentThoroughfareLeadingType")]
        public OutArgument<String> DependentThoroughfareLeadingType { get; set; }

        [Output("outDependentThoroughfareName")]
        public OutArgument<String> DependentThoroughfareName { get; set; }

        [Output("outDependentThoroughfarePostDirection")]
        public OutArgument<String> DependentThoroughfarePostDirection { get; set; }

        [Output("outDependentThoroughfarePreDirection")]
        public OutArgument<String> DependentThoroughfarePreDirection { get; set; }

        [Output("outDependentThoroughfareTrailingType")]
        public OutArgument<String> DependentThoroughfareTrailingType { get; set; }

        [Output("outISO2Code")]
        public OutArgument<String> ISO2Code { get; set; }

        [Output("outISO3Code")]
        public OutArgument<String> ISO3Code { get; set; }

        [Output("outISONumCode")]
        public OutArgument<String> ISONumCode { get; set; }

        [Output("outPostBox")]
        public OutArgument<String> PostBox { get; set; }

        [Output("outPremisesNumber")]
        public OutArgument<String> PremisesNumber { get; set; }

        [Output("outPremisesType")]
        public OutArgument<String> PremisesType { get; set; }

        [Output("outSubPremises")]
        public OutArgument<String> SubPremises { get; set; }
        
        [Output("outSubPremisesNumber")]
        public OutArgument<String> SubPremisesNumber { get; set; }

        [Output("outSubPremisesType")]
        public OutArgument<String> SubPremisesType { get; set; }

        [Output("outThoroughfare")]
        public OutArgument<String> Thoroughfare { get; set; }

        [Output("outThoroughfareLeadingType")]
        public OutArgument<String> ThoroughfareLeadingType { get; set; }

        [Output("outThoroughfareName")]
        public OutArgument<String> ThoroughfareName { get; set; }

        [Output("outThoroughfarePostDirection")]
        public OutArgument<String> ThoroughfarePostDirection { get; set; }

        [Output("outThoroughfarePreDirection")]
        public OutArgument<String> ThoroughfarePreDirection { get; set; }

        [Output("outThoroughfareTrailingType")]
        public OutArgument<String> ThoroughfareTrailingType { get; set; }

        [Output("outLatitude")]
        public OutArgument<String> Latitude { get; set; }

        [Output("outLongitude")]
        public OutArgument<String> Longitude { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            GlobalWS.soapSSLEndpointGlobalAddressCheck service = new GlobalWS.soapSSLEndpointGlobalAddressCheck();
            GlobalWS.Request request = new GlobalWS.Request();
            GlobalWS.Response response = new GlobalWS.Response();
            GlobalWS.RequestRecord reqRecord = new GlobalWS.RequestRecord();

            String license = CustomerID.Get(context);

            //License to CustomerID generation
            HttpWebRequest tokenRequest;
            HttpWebResponse tokenResponse;
            System.IO.Stream tokenResponseStream;
            Uri tokenAddress;

            String tokenServer = "https://token.melissadata.net/v3/WEB/service.svc/QueryCustomerInfo?P=&K=&L=";
            license = Uri.EscapeDataString(license);
            tokenServer += license;
            tokenAddress = new Uri(tokenServer);
            tokenRequest = (HttpWebRequest)WebRequest.Create(tokenAddress);
            tokenResponse = (HttpWebResponse)tokenRequest.GetResponse();
            tokenResponseStream = tokenResponse.GetResponseStream();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(tokenResponseStream);

            XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("/QueryCustomerInfoResponse/CustomerId");
            String id = node.InnerText;

            tokenResponseStream.Close();
            tokenResponse.Close();

            request.CustomerID = id;
            request.Options = Options.Get(context);
            reqRecord.AddressLine1 = AddressLine1.Get(context);
            reqRecord.AddressLine2 = AddressLine2.Get(context);
            reqRecord.AddressLine3 = AddressLine3.Get(context);
            reqRecord.AddressLine4 = AddressLine4.Get(context);
            reqRecord.AddressLine5 = AddressLine5.Get(context);
            reqRecord.AddressLine6 = AddressLine6.Get(context);
            reqRecord.AddressLine7 = AddressLine7.Get(context);
            reqRecord.AddressLine8 = AddressLine8.Get(context);
            reqRecord.Organization = Organization.Get(context);
            reqRecord.DoubleDependentLocality = DoubleDependentLocality.Get(context);
            reqRecord.DependentLocality = DependentLocality.Get(context);
            reqRecord.Locality = Locality.Get(context);
            reqRecord.SubAdministrativeArea = SubAdministrativeArea.Get(context);
            reqRecord.AdministrativeArea = AdministrativeArea.Get(context);
            reqRecord.SubNationalArea = SubNationalArea.Get(context);
            reqRecord.PostalCode = PostalCode.Get(context);
            reqRecord.Country = Country.Get(context);
            request.Records = new GlobalWS.RequestRecord[1];
            request.Records[0] = reqRecord;

            String errorMessage = "";
            try
            {
                response = service.doGlobalAddress(request);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact melissa data support. Error Message: " + ex.Message;
            }

            String outOrganization = "";
            String outAddress1 = "";
            String outAddress2 = "";
            String outAddress3 = "";
            String outAddress4 = "";
            String outAddress5 = "";
            String outAddress6 = "";
            String outAddress7 = "";
            String outAddress8 = "";
            String outDoubleDependentLocality = "";
            String outDependentLocality = "";
            String outLocality = "";
            String outSubAdministrativeArea = "";
            String outAdministrativeArea = "";
            String outSubNationalArea = "";
            String outPostalCode = "";
            String outCountryName = "";
            String outResults = "";
            String outBuilding = "";
            String outISO2 = "";
            String outISO3 = "";
            String outISONum = "";
            String outDependentThoroughfare = "";
            String outDependentThoroughfareLeadingType = "";
            String outDependentThoroughfareName = "";
            String outDependentThoroughfarePostDirection = "";
            String outDependentThoroughfarePreDirection = "";
            String outDependentThoroughfareTrailingType = "";
            String outFormattedAddress = "";
            String outPostBox = "";
            String outPremisesNumber = "";
            String outPremisesType = "";
            String outSubPremises = "";
            String outSubPremisesNumber = "";
            String outSubPremisesType = "";
            String outThoroughfare = "";
            String outThoroughfareLeadingType = "";
            String outThoroughfareName = "";
            String outThoroughfarePostDirection = "";
            String outThoroughfarePreDirection = "";
            String outThoroughfareTrailingType = "";
            String outLatitude = "";
            String outLongitude = "";
            String outError = "";

            if (errorMessage.Length > 0)
            {
                outError = errorMessage;
            }
            else if (response.TransmissionResults.Length > 0)
            {
                outError = response.TransmissionResults;
            }
            else
            {
                GlobalWS.ResponseRecord resRecord = new GlobalWS.ResponseRecord();
                resRecord = response.Records[0];

                outOrganization = resRecord.Organization;
                outAddress1 = resRecord.AddressLine1;
                outAddress2 = resRecord.AddressLine2;
                outAddress3 = resRecord.AddressLine3;
                outAddress4 = resRecord.AddressLine4;
                outAddress5 = resRecord.AddressLine5;
                outAddress6 = resRecord.AddressLine6;
                outAddress7 = resRecord.AddressLine7;
                outAddress8 = resRecord.AddressLine8;
                outDoubleDependentLocality = resRecord.DoubleDependentLocality;
                outDependentLocality = resRecord.DependentLocality;
                outLocality = resRecord.Locality;
                outSubAdministrativeArea = resRecord.SubAdministrativeArea;
                outAdministrativeArea = resRecord.AdministrativeArea;
                outSubNationalArea = resRecord.SubNationalArea;
                outPostalCode = resRecord.PostalCode;
                outCountryName = resRecord.CountryName;
                outISO2 = resRecord.CountryISO3166_1_Alpha2;
                outISO3 = resRecord.CountryISO3166_1_Alpha3;
                outISONum = resRecord.CountryISO3166_1_Numeric;
                outDependentThoroughfare = resRecord.DependentThoroughfare;
                outDependentThoroughfareLeadingType = resRecord.DependentThoroughfareLeadingType;
                outDependentThoroughfareName = resRecord.DependentThoroughfareName;
                outDependentThoroughfarePostDirection = resRecord.DependentThoroughfarePostDirection;
                outDependentThoroughfarePreDirection = resRecord.DependentThoroughfarePreDirection;
                outDependentThoroughfareTrailingType = resRecord.DependentThoroughfareTrailingType;
                outResults = resRecord.Results;
                outBuilding = resRecord.Building;
                outThoroughfare = resRecord.Thoroughfare;
                outThoroughfareLeadingType = resRecord.ThoroughfareLeadingType;
                outThoroughfareName = resRecord.ThoroughfareName;
                outThoroughfarePostDirection = resRecord.ThoroughfarePostDirection;
                outThoroughfarePreDirection = resRecord.ThoroughfarePreDirection;
                outThoroughfareTrailingType = resRecord.ThoroughfareTrailingType;
                outPremisesNumber = resRecord.PremisesNumber;
                outPremisesType = resRecord.PremisesType;
                outSubPremises = resRecord.SubPremises;
                outSubPremisesNumber = resRecord.SubPremisesNumber;
                outSubPremisesType = resRecord.SubPremisesType;
                outFormattedAddress = resRecord.FormattedAddress;
                outPostBox = resRecord.PostBox;
                outLatitude = resRecord.Latitude;
                outLongitude = resRecord.Longitude;
            }

            Organization.Set(context, outOrganization);
            AddressLine1.Set(context, outAddress1);
            AddressLine2.Set(context, outAddress2);
            AddressLine3.Set(context, outAddress3);
            AddressLine4.Set(context, outAddress4);
            AddressLine5.Set(context, outAddress5);
            AddressLine6.Set(context, outAddress6);
            AddressLine7.Set(context, outAddress7);
            AddressLine8.Set(context, outAddress8);
            DoubleDependentLocality.Set(context, outDoubleDependentLocality);
            DependentLocality.Set(context, outDependentLocality);
            Locality.Set(context, outLocality);
            SubAdministrativeArea.Set(context, outSubAdministrativeArea);
            AdministrativeArea.Set(context, outAdministrativeArea);
            SubNationalArea.Set(context, outSubNationalArea);
            PostalCode.Set(context, outPostalCode);
            CountryName.Set(context, outCountryName);
            Results.Set(context, outResults);
            Building.Set(context, outBuilding);
            ISO2Code.Set(context, outISO2);
            ISO3Code.Set(context, outISO3);
            ISONumCode.Set(context, outISONum);
            DependentThoroughfare.Set(context, outDependentThoroughfare);
            DependentThoroughfareLeadingType.Set(context, outDependentThoroughfareLeadingType);
            DependentThoroughfareName.Set(context, outDependentThoroughfareName);
            DependentThoroughfarePostDirection.Set(context, outDependentThoroughfarePostDirection);
            DependentThoroughfarePreDirection.Set(context, outDependentThoroughfarePreDirection);
            DependentThoroughfareTrailingType.Set(context, outDependentThoroughfareTrailingType);
            FormattedAddress.Set(context, outFormattedAddress);
            PostBox.Set(context, outPostBox);
            PremisesNumber.Set(context, outPremisesNumber);
            PremisesType.Set(context, outPremisesType);
            SubPremises.Set(context, outSubPremises);
            SubPremisesNumber.Set(context, outSubPremisesNumber);
            SubPremisesType.Set(context, outSubPremisesType);
            Thoroughfare.Set(context, outThoroughfare);
            ThoroughfareLeadingType.Set(context, outThoroughfareLeadingType);
            ThoroughfareName.Set(context, outThoroughfareName);
            ThoroughfarePostDirection.Set(context, outThoroughfarePostDirection);
            ThoroughfarePreDirection.Set(context, outThoroughfarePreDirection);
            ThoroughfareTrailingType.Set(context, outThoroughfareTrailingType);
            Latitude.Set(context, outLatitude);
            Longitude.Set(context, outLongitude);
            Error.Set(context, outError);
        }
    }
}
