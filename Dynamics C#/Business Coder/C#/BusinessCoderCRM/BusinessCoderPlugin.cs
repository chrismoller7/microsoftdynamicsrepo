using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

namespace DynCRMBusinessCoderVerify {
	public class DynCRMBusinessCoderVerify : CodeActivity
	{
        /****************************** INPUTS ******************************/
        //[RequiredArgument]

		[Input("inLicense")]
        public InArgument<String> License { get; set; }

        [Input("inColumns")]
        public InArgument<String> Columns { get; set; }

        [Input("inOptions")]
        public InArgument<String> Options { get; set; }

        [Input("inCompanyName")]
        public InArgument<String> iCompanyName { get; set; }

        [Input("inPhoneNumber")]
        public InArgument<String> PhoneNumber { get; set; }

        [Input("inAddress1")]
        public InArgument<String> Address1 { get; set; }

        [Input("inAddress2")]
        public InArgument<String> Address2 { get; set; }

        [Input("inCity")]
        public InArgument<String> iCity { get; set; }

        [Input("inState")]
        public InArgument<String> iState { get; set; }

        [Input("inPostal")]
        public InArgument<String> Postal { get; set; }

        [Input("inCountry")]
        public InArgument<String> iCountry { get; set; }

        [Input("inMAK")]
        public InArgument<String> MAK { get; set; }

        [Input("inStockTicker")]
        public InArgument<String> iStockTicker { get; set; }

        [Input("inWebAddress")]
        public InArgument<String> iWebAddress { get; set; }

        [Input("inTransmissionReference")]
        public InArgument<String> TransmissionReference { get; set; }

        [Input("inRecordID")]
        public InArgument<String> RecordID { get; set; }

        [Input("inInputOption")]
        [RequiredArgument]
        public InArgument<String> InputOption { get; set; }
		
		/****************************** OUTPUTS ******************************/
		//No Group
		[Output("outLocationType")]
		public OutArgument<String> LocationType { get; set; }
		
		[Output("outPhone")]
		public OutArgument<String> Phone { get; set; }
		
		[Output("outStockTicker")]
		public OutArgument<String> StockTicker { get; set; }
		
		[Output("outWebAddress")]
		public OutArgument<String> WebAddress { get; set; }
		
		[Output("outEmployeesEstimate")]
		public OutArgument<String> EmployeesEstimate { get; set; }
		
		[Output("outSalesEstimate")]
		public OutArgument<String> SalesEstimate { get; set; }
		
		//Default
		[Output("outCompanyName")]
		public OutArgument<String> CompanyName { get; set; }
		
		[Output("outAddressLine1")]
		public OutArgument<String> AddressLine1 { get; set; }
		
		[Output("outSuite")]
		public OutArgument<String> Suite { get; set; }
		
		[Output("outCity")]
		public OutArgument<String> City { get; set; }
		
		[Output("outState")]
		public OutArgument<String> State { get; set; }
		
		[Output("outPostalCode")]
		public OutArgument<String> PostalCode { get; set; }
		
		//GrpAddressDetails
		[Output("outPlus4")]
		public OutArgument<String> Plus4 { get; set; }
		
		[Output("outDeliveryIndicator")]
		public OutArgument<String> DeliveryIndicator { get; set; }
		
		[Output("outMelissaAddressKey")]
		public OutArgument<String> MelissaAddressKey { get; set; }
		
		[Output("outMelissaAddressKeyBase")]
		public OutArgument<String> MelissaAddressKeyBase { get; set; }
		
		[Output("outCountryName")]
		public OutArgument<String> CountryName { get; set; }
		
		[Output("outCountryCode")]
		public OutArgument<String> CountryCode { get; set; }
		
		//GrpBusinessCodes
		[Output("outSICCode1")]
		public OutArgument<String> SICCode1 { get; set; }
		
		[Output("outSICCode2")]
		public OutArgument<String> SICCode2 { get; set; }
		
		[Output("outSICCode3")]
		public OutArgument<String> SICCode3 { get; set; }
		
		[Output("outNAICSCode1")]
		public OutArgument<String> NAICSCode1 { get; set; }
		
		[Output("outNAICSCode2")]
		public OutArgument<String> NAICSCode2 { get; set; }
		
		[Output("outNAICSCode3")]
		public OutArgument<String> NAICSCode3 { get; set; }
		
		//GrpBusinessDescription
		[Output("outSICDescription1")]
		public OutArgument<String> SICDescription1 { get; set; }
		
		[Output("outSICDescription2")]
		public OutArgument<String> SICDescription2 { get; set; }
		
		[Output("outSICDescription3")]
		public OutArgument<String> SICDescription3 { get; set; }
		
		[Output("outNAICSDescription1")]
		public OutArgument<String> NAICSDescription1 { get; set; }
		
		[Output("outNAICSDescription2")]
		public OutArgument<String> NAICSDescription2 { get; set; }
		
		[Output("outNAICSDescription3")]
		public OutArgument<String> NAICSDescription3 { get; set; }
		
		//GrpGeoCode
		[Output("outLatitude")]
		public OutArgument<String> Latitude { get; set; }
		
		[Output("outLongitude")]
		public OutArgument<String> Longitude { get; set; }
		
		//GrpCensus
		[Output("outCountyName")]
		public OutArgument<String> CountyName { get; set; }
		
		[Output("outCountyFIPS")]
		public OutArgument<String> CountyFIPS { get; set; }
		
		[Output("outCensusTract")]
		public OutArgument<String> CensusTract { get; set; }
		
		[Output("outCensusBlock")]
		public OutArgument<String> CensusBlock { get; set; }
		
		[Output("outPlaceCode")]
		public OutArgument<String> PlaceCode { get; set; }
		
		[Output("outPlaceName")]
		public OutArgument<String> PlaceName { get; set; }

        [Output("outResponseURL")]
        public OutArgument<String> ResponseURL { get; set; }

        [Output("outProcessedResults")]
        public OutArgument<String> ProcessedResults { get; set; }

        [Output("outContacts")]
        public OutArgument<String> Contacts { get; set; }
		
		protected override void Execute(CodeActivityContext context) {
			
			HttpWebRequest request;
			HttpWebResponse response;
			System.IO.Stream responseStream;
			System.IO.MemoryStream JSONStream;
			Uri address;
            String tServer = "http://businesscoder.melissadata.net/WEB/BusinessCoder/doBusinessCoderUS";
			StreamReader streamReader;

            String selInput = InputOption.Get(context);
			
			//License input
			String auth = License.Get(context);
			tServer += "?id=" + auth;
			
			//Columns input
            String colms = Columns.Get(context);
            tServer += "&cols=" + colms; // Columns.Get(context);
			
			//Options input
			tServer += "&opt=" + Options.Get(context);

            if (iCompanyName.Get(context).Length > 0)
            {
                tServer += "&comp=" + iCompanyName.Get(context);
            }

            if (PhoneNumber.Get(context).Length > 0)
            {
                //Phone Number input
                tServer += "&phone=" + PhoneNumber.Get(context);
            }

            if (Address1.Get(context).Length > 0)
            {

                //Address1 input
                tServer += "&a1=" + Address1.Get(context);

                //Address2 input
                tServer += "&a2=" + Address2.Get(context);

                //City input
                tServer += "&city=" + iCity.Get(context);

                //State input
                tServer += "&state=" + iState.Get(context);

                //Postal input
                tServer += "&postal=" + Postal.Get(context);

                //Country input
                tServer += "&ctry?=" + iCountry.Get(context);
            }

            if (MAK.Get(context).Length > 0)
            {
                //MAK input
                tServer += "&mak=" + MAK.Get(context);
            }

            if (iStockTicker.Get(context).Length > 0)
            {
                //Stock Ticker input
                tServer += "&stock=" + iStockTicker.Get(context);
            }

            if (iWebAddress.Get(context).Length > 0)
            {
                //WebAddress input
                tServer += "&web=" + iWebAddress.Get(context);
            }

			//TransmissionReference input
			tServer += "&t=" + TransmissionReference.Get(context);
			
			//RecordID input
			tServer += "&rec=" + RecordID.Get(context);
			
			//Create Uri and request
			address = new Uri(tServer);
			request = (HttpWebRequest)WebRequest.Create(address);
			
			//Grab response
            response = (HttpWebResponse)request.GetResponse();
			
			//Grab stream of response
            responseStream = response.GetResponseStream();
			
            JSONStream = new System.IO.MemoryStream();
            responseStream.CopyTo(JSONStream);
			streamReader = new StreamReader(JSONStream);
			JSONStream.Position = 0;
			System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(BusinessCoderResponse));
			BusinessCoderResponse businessCoderData = (BusinessCoderResponse) serializer.ReadObject(JSONStream);
			
		
			//Check options
			//If option A/B/C/etc.. are checked, output them? (Correction - the JS handles this, not the plugin)
			
			//All output variables
			//[NO GROUP]
			String outLocationType = "";
			String outPhone = "";
			String outStockTicker = "";
			String outWebAddress = "";
			String outEmployeesEstimate = "";
			String outSalesEstimate = "";
			//[DEFAULT]
			String outCompanyName = "";
			String outAddressLine1 = "";
			String outSuite = "";
			String outCity = "";
			String outState = "";
			String outPostalCode = "";
			//GrpAddressDetails
			String outPlus4 = "";
			String outDeliveryIndicator = "";
			String outMelissaAddressKey = "";
			String outMelissaAddressKeyBase = "";
			String outCountryName = "";
			String outCountryCode = "";
			//GrpBusinessCodes
			String outSICCode1 = "";
			String outSICCode2 = "";
			String outSICCode3 = "";
			String outNAICSCode1 = "";
			String outNAICSCode2 = "";
			String outNAICSCode3 = "";
			//GrpBusinessDescription
			String outSICDescription1 = "";
			String outSICDescription2 = "";
			String outSICDescription3 = "";
			String outNAICSDescription1 = "";
			String outNAICSDescription2= "";
			String outNAICSDescription3 = "";
			//GrpGeoCode
			String outLatitude = "";
			String outLongitude = "";
			//GrpCensus
			String outCountyName = "";
			String outCountyFIPS = "";
			String outCensusTract = "";
			String outCensusBlock = "";
			String outPlaceCode = "";
			String outPlaceName = "";

            //Processing
            String outProcessedResults = "";
            String outProcessDate = "";

            String outTransmissionResults = "";

            //Contacts
            String outContacts = "";

            //Contacts setting
            if (colms.Contains("Contacts"))
            {
                for (int i = 0; i < businessCoderData.Records[0].Contacts.Count; i++)
                {
                    String first = businessCoderData.Records[0].Contacts[i].NameFirst;
                    String last = businessCoderData.Records[0].Contacts[i].NameLast;
                    String title = businessCoderData.Records[0].Contacts[i].Title;
                    String email = businessCoderData.Records[0].Contacts[i].Email;
                    outContacts += "[NAME]: " + first + " " + last + "\n[TITLE]: " + title + "\n[EMAIL]: " + email + "\n\n";

                }
            }
            //outContacts = tServer;

			//Additional Information
            if (colms.Contains("Phone"))
            {
                outLocationType = businessCoderData.Records[0].LocationType;
                outPhone = businessCoderData.Records[0].Phone;
                outStockTicker = businessCoderData.Records[0].StockTicker;
                outWebAddress = businessCoderData.Records[0].WebAddress;
                outEmployeesEstimate = businessCoderData.Records[0].EmployeesEstimate;
                outSalesEstimate = businessCoderData.Records[0].SalesEstimate;
            }

            //Base information
            outCompanyName = businessCoderData.Records[0].CompanyName;
			outAddressLine1 = businessCoderData.Records[0].AddressLine1;
			outSuite = businessCoderData.Records[0].Suite;
			outCity = businessCoderData.Records[0].City;
			outState = businessCoderData.Records[0].State;
			outPostalCode = businessCoderData.Records[0].PostalCode;

            //GrpAddressDetails
            if(colms.Contains("GrpAddressDetails")) {
			    outPlus4 = businessCoderData.Records[0].Plus4;
			    outDeliveryIndicator = businessCoderData.Records[0].DeliveryIndicator;
			    outMelissaAddressKey = businessCoderData.Records[0].MelissaAddressKey;
			    outMelissaAddressKeyBase = businessCoderData.Records[0].MelissaAddressKeyBase;
			    //outCountryName = businessCoderData.Records[0].CountryName; -- Business Coder does not have these columns yet
                //outCountryCode = businessCoderData.Records[0].CountryCode; -- Business Coder does not have these columns yet
            }

            //GrpBusinessCodes
            if(colms.Contains("GrpBusinessCodes")) {
			    outSICCode1 = businessCoderData.Records[0].SICCode1;
			    outSICCode2 = businessCoderData.Records[0].SICCode2;
			    outSICCode3 = businessCoderData.Records[0].SICCode3;
			    outNAICSCode1 = businessCoderData.Records[0].NAICSCode1;
			    outNAICSCode2 = businessCoderData.Records[0].NAICSCode2;
			    outNAICSCode3 = businessCoderData.Records[0].NAICSCode3;
            }

            //GrpBusinessDescriptions
			if(colms.Contains("GrpBusinessDescription")) {
                outSICDescription1 = businessCoderData.Records[0].SICDescription1;
			    outSICDescription2 = businessCoderData.Records[0].SICDescription2;
			    outSICDescription3 = businessCoderData.Records[0].SICDescription3;
			    outNAICSDescription1 = businessCoderData.Records[0].NAICSDescription1;
			    outNAICSDescription2 = businessCoderData.Records[0].NAICSDescription2;
			    outNAICSDescription3 = businessCoderData.Records[0].NAICSDescription3;
            }
            
            //GrpGeocode
            if(colms.Contains("GrpGeoCode")) {
			    outLatitude = businessCoderData.Records[0].Latitude;
			    outLongitude = businessCoderData.Records[0].Longitude;
            }

            //GrpCensusDetails
			if(colms.Contains("GrpCensusDetails")) {
                outCountyName = businessCoderData.Records[0].CountyName;
			    outCountyFIPS = businessCoderData.Records[0].CountyFIPS;
			    outCensusTract = businessCoderData.Records[0].CensusTract;
			    outCensusBlock = businessCoderData.Records[0].CensusBlock;
			    outPlaceCode = businessCoderData.Records[0].PlaceCode;
			    outPlaceName = businessCoderData.Records[0].PlaceName;
            }

            //Processing
            outProcessedResults = businessCoderData.Records[0].Results;
            outTransmissionResults = businessCoderData.TransmissionResults;


			/************/
			
			LocationType.Set(context, outLocationType);
			Phone.Set(context, outPhone);
			StockTicker.Set(context, outStockTicker);
			WebAddress.Set(context, outWebAddress);
			EmployeesEstimate.Set(context, outEmployeesEstimate);
			SalesEstimate.Set(context, outSalesEstimate);
			CompanyName.Set(context, outCompanyName);
			AddressLine1.Set(context, outAddressLine1);
			Suite.Set(context, outSuite);
			City.Set(context, outCity);
			State.Set(context, outState);
			PostalCode.Set(context, outPostalCode);
			Plus4.Set(context, outPlus4);
			DeliveryIndicator.Set(context, outDeliveryIndicator);
			MelissaAddressKey.Set(context, outMelissaAddressKey);
			MelissaAddressKeyBase.Set(context, outMelissaAddressKeyBase);
			CountryName.Set(context, outCountryName);
			CountryCode.Set(context, outCountryCode);
			SICCode1.Set(context, outSICCode1);
			SICCode2.Set(context, outSICCode2);
			SICCode3.Set(context, outSICCode3);
			NAICSCode1.Set(context, outNAICSCode1);
			NAICSCode2.Set(context, outNAICSCode2);
			NAICSCode3.Set(context, outNAICSCode3);
			SICDescription1.Set(context, outSICDescription1);
			SICDescription2.Set(context, outSICDescription2);
			SICDescription3.Set(context, outSICDescription3);
			NAICSDescription1.Set(context, outNAICSDescription1);
			NAICSDescription2.Set(context, outNAICSDescription2);
			NAICSDescription3.Set(context, outNAICSDescription3);
			Latitude.Set(context, outLatitude);
			Longitude.Set(context, outLongitude);
			CountyName.Set(context, outCountyName);
			CountyFIPS.Set(context, outCountyFIPS);
			CensusTract.Set(context, outCensusTract);
			CensusBlock.Set(context, outCensusBlock);
			PlaceCode.Set(context, outPlaceCode);
			PlaceName.Set(context, outPlaceName);

            ProcessedResults.Set(context, outProcessedResults);

            ResponseURL.Set(context, outTransmissionResults);

            Contacts.Set(context, outContacts);
		}
	}
}