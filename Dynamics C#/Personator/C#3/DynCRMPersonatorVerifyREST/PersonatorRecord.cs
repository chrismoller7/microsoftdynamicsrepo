﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;



namespace DynCRMPersonatorVerifyREST
{
    [DataContract]
    public class PersonatorRecord
    {
        [DataMember(Name = "AddressDeliveryInstallation")]
        public string AddressDeliveryInstallation { get; set; }

        [DataMember(Name = "AddressExtras")]
        public string AddressExtras { get; set; }

        [DataMember(Name = "AddressHouseNumber")]
        public string AddressHouseNumber { get; set; }

        [DataMember(Name = "AddressKey")]
        public string AddressKey { get; set; }

        [DataMember(Name = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "AddressLine2")]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "AddressLockBox")]
        public string AddressLockBox { get; set; }

        [DataMember(Name = "AddressPostDirection")]
        public string AddressPostDirection { get; set; }

        [DataMember(Name = "AddressPreDirection")]
        public string AddressPreDirection { get; set; }

        [DataMember(Name = "AddressPrivateMailboxName")]
        public string AddressPrivateMailboxName { get; set; }

        [DataMember(Name = "AddressPrivateMailboxRange")]
        public string AddressPrivateMailboxRange { get; set; }

        [DataMember(Name = "AddressRouteService")]
        public string AddressRouteService { get; set; }

        [DataMember(Name = "AddressStreetName")]
        public string AddressStreetName { get; set; }

        [DataMember(Name = "AddressStreetSuffix")]
        public string AddressStreetSuffix { get; set; }

        [DataMember(Name = "AddressSuiteName")]
        public string AddressSuiteName { get; set; }

        [DataMember(Name = "AddressSuiteNumber")]
        public string AddressSuiteNumber { get; set; }

        [DataMember(Name = "AddressTypeCode")]
        public string AddressTypeCode { get; set; }

        [DataMember(Name = "AreaCode")]
        public string AreaCode { get; set; }

        [DataMember(Name = "CarrierRoute")]
        public string CarrierRoute { get; set; }

        [DataMember(Name = "CBSACode")]
        public string CBSACode { get; set; }

        [DataMember(Name = "CBSADivisionCode")]
        public string CBSADivisionCode { get; set; }

        [DataMember(Name = "CBSADivisionLevel")]
        public string CBSADivisionLevel { get; set; }

        [DataMember(Name = "CBSADivisionTitle")]
        public string CBSADivisionTitle { get; set; }

        [DataMember(Name = "CBSALevel")]
        public string CBSALevel { get; set; }

        [DataMember(Name = "CBSATitle")]
        public string CBSATitle { get; set; }

        [DataMember(Name = "CensusBlock")]
        public string CensusBlock { get; set; }

        [DataMember(Name = "CensusKey")]
        public string CensusKey { get; set; }

        [DataMember(Name = "CensusTract")]
        public string CensusTract { get; set; }

        [DataMember(Name = "City")]
        public string City { get; set; }

        [DataMember(Name = "CityAbbreviation")]
        public string CityAbbreviation { get; set; }

        [DataMember(Name = "CompanyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "CongressionalDistrict")]
        public string CongressionalDistrict { get; set; }

        [DataMember(Name = "CountryCode")]
        public string CountryCode { get; set; }

        [DataMember(Name = "CountryName")]
        public string CountryName { get; set; }

        [DataMember(Name = "CountyFIPS")]
        public string CountyFIPS { get; set; }

        [DataMember(Name = "CountyName")]
        public string CountyName { get; set; }

        [DataMember(Name = "CountySubdivisionCode")]
        public string CountySubdivisionCode { get; set; }

        [DataMember(Name = "CountySubdivisionName")]
        public string CountySubdivisionName { get; set; }

        [DataMember(Name = "DateOfBirth")]
        public string DateOfBirth { get; set; }

        [DataMember(Name = "DateOfDeath")]
        public string DateOfDeath { get; set; }

        [DataMember(Name = "DeliveryIndicator")]
        public string DeliveryIndicator { get; set; }

        [DataMember(Name = "DeliveryPointCheckDigit")]
        public string DeliveryPointCheckDigit { get; set; }

        [DataMember(Name = "DeliveryPointCode")]
        public string DeliveryPointCode { get; set; }

        [DataMember(Name = "DemographicsGender")]
        public string DemographicsGender { get; set; }

        [DataMember(Name = "DemographicsResults")]
        public string DemographicsResults { get; set; }

        [DataMember(Name = "DomainName")]
        public string DomainName { get; set; }

        [DataMember(Name = "ElementarySchoolDistrictCode")]
        public string ElementarySchoolDistrictCode { get; set; }

        [DataMember(Name = "ElementarySchoolDistrictName")]
        public string ElementarySchoolDistrictName { get; set; }

        [DataMember(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name = "Gender")]
        public string Gender { get; set; }

        [DataMember(Name = "Gender2")]
        public string Gender2 { get; set; }

        [DataMember(Name = "HouseholdIncome")]
        public string HouseholdIncome { get; set; }

        [DataMember(Name = "Latitude")]
        public string Latitude { get; set; }

        [DataMember(Name = "LengthOfResidence")]
        public string LengthOfResidence { get; set; }

        [DataMember(Name = "Longitude")]
        public string Longitude { get; set; }

        [DataMember(Name = "MailboxName")]
        public string MailboxName { get; set; }

        [DataMember(Name = "MaritalStatus")]
        public string MaritalStatus { get; set; }

        [DataMember(Name = "MelissaAddressKey")]
        public string MelissaAddressKey { get; set; }

        [DataMember(Name = "MelissaAddressKeyBase")]
        public string MelissaAddressKeyBase { get; set; }

        [DataMember(Name = "NameFirst")]
        public string NameFirst { get; set; }

        [DataMember(Name = "NameFirst2")]
        public string NameFirst2 { get; set; }

        [DataMember(Name = "NameFull")]
        public string NameFull { get; set; }

        [DataMember(Name = "NameLast")]
        public string NameLast { get; set; }

        [DataMember(Name = "NameLast2")]
        public string NameLast2 { get; set; }

        [DataMember(Name = "NameMiddle")]
        public string NameMiddle { get; set; }

        [DataMember(Name = "NameMiddle2")]
        public string NameMiddle2 { get; set; }

        [DataMember(Name = "NamePrefix")]
        public string NamePrefix { get; set; }

        [DataMember(Name = "NamePrefix2")]
        public string NamePrefix2 { get; set; }

        [DataMember(Name = "NameSuffix")]
        public string NameSuffix { get; set; }

        [DataMember(Name = "NameSuffix2")]
        public string NameSuffix2 { get; set; }

        [DataMember(Name = "NewAreaCode")]
        public string NewAreaCode { get; set; }

        [DataMember(Name = "Occupation")]
        public string Occupation { get; set; }

        [DataMember(Name = "OwnRent")]
        public string OwnRent { get; set; }

        [DataMember(Name = "PhoneCountryCode")]
        public string PhoneCountryCode { get; set; }

        [DataMember(Name = "PhoneCountryName")]
        public string PhoneCountryName { get; set; }

        [DataMember(Name = "PhoneExtension")]
        public string PhoneExtension { get; set; }

        [DataMember(Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "PhonePrefix")]
        public string PhonePrefix { get; set; }

        [DataMember(Name = "PhoneSuffix")]
        public string PhoneSuffix { get; set; }

        [DataMember(Name = "PlaceCode")]
        public string PlaceCode { get; set; }

        [DataMember(Name = "PlaceName")]
        public string PlaceName { get; set; }

        [DataMember(Name = "Plus4")]
        public string Plus4 { get; set; }

        [DataMember(Name = "PostalCode")]
        public string PostalCode { get; set; }

        [DataMember(Name = "PresenceOfChildren")]
        public string PresenceOfChildren { get; set; }

        [DataMember(Name = "PrivateMailBox")]
        public string PrivateMailBox { get; set; }

        [DataMember(Name = "RecordExtras")]
        public string RecordExtras { get; set; }

        [DataMember(Name = "RecordID")]
        public string RecordID { get; set; }

        [DataMember(Name = "Reserved")]
        public string Reserved { get; set; }

        [DataMember(Name = "Results")]
        public string Results { get; set; }

        [DataMember(Name = "Salutation")]
        public string Salutation { get; set; }

        [DataMember(Name = "SecondarySchoolDistrictCode")]
        public string SecondarySchoolDistrictCode { get; set; }

        [DataMember(Name = "SecondarySchoolDistrictName")]
        public string SecondarySchoolDistrictName { get; set; }

        [DataMember(Name = "State")]
        public string State { get; set; }

        [DataMember(Name = "StateDistrictLower")]
        public string StateDistrictLower { get; set; }

        [DataMember(Name = "StateDistrictUpper")]
        public string StateDistrictUpper { get; set; }

        [DataMember(Name = "StateName")]
        public string StateName { get; set; }

        [DataMember(Name = "Suite")]
        public string Suite { get; set; }

        [DataMember(Name = "TopLevelDomain")]
        public string TopLevelDomain { get; set; }

        [DataMember(Name = "UnifiedSchoolDistrictCode")]
        public string UnifiedSchoolDistrictCode { get; set; }

        [DataMember(Name = "UnifiedSchoolDistrictName")]
        public string UnifiedSchoolDistrictName { get; set; }

        [DataMember(Name = "UrbanizationName")]
        public string UrbanizationName { get; set; }

        [DataMember(Name = "UTC")]
        public string UTC { get; set; }

        [DataMember(Name = "Error")]
        public string Error { get; set; }

        [DataMember(Name = "TransmissionResults")]
        public string TransmissionResults { get; set; }

        [DataMember(Name = "MoveDate")]
        public string MoveDate { get; set; }

        [DataMember(Name = "HouseholdSize")]
        public string HouseholdSize { get; set; }
        
        [DataMember(Name = "CreditCardUser")]
        public string CreditCardUser { get; set; }
        
        [DataMember(Name = "PresenceOfSenior")]
        public string PresenceOfSenior { get; set; }
        
        [DataMember(Name = "ChildrenAgeRange")]
        public string ChildrenAgeRange { get; set; }
        
        [DataMember(Name = "Education")]
        public string Education { get; set; }
        
        [DataMember(Name = "PoliticalParty")]
        public string PoliticalParty { get; set; }
        
        [DataMember(Name = "TypeOfVehicles")]
        public string TypeOfVehicles { get; set; }
        

    }
}
