﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Xml.Linq;

namespace DynCRMPersonatorVerifyREST
{
    public class DynCRMPersonatorVerify : CodeActivity
    {
        [Input("inCustomerID")]
        [RequiredArgument]
        public InArgument<String> CustomerID { get; set; }

        [Input("inOptions")]
        [RequiredArgument]
        public InArgument<String> Options { get; set; }

        [Input("inActions")]
        [RequiredArgument]
        public InArgument<String> Actions { get; set; }

        [Input("inColumns")]
        [RequiredArgument]
        public InArgument<String> Columns { get; set; }

        [Output("outAddressDeliveryInstallation")]
        public OutArgument<String> AddressDeliveryInstallation { get; set; }

        [Output("outAddressExtras")]
        public OutArgument<String> AddressExtras { get; set; }

        [Output("outAddressHouseNumber")]
        public OutArgument<String> AddressHouseNumber { get; set; }

        [Output("outAddressKey")]
        public OutArgument<String> AddressKey { get; set; }

        [Input("inAddressLine1")]
        [Output("outAddressLine1")]
        public InOutArgument<String> AddressLine1 { get; set; }

        [Input("inAddressLine2")]
        [Output("outAddressLine2")]
        public InOutArgument<String> AddressLine2 { get; set; }

        [Output("outAddressLockBox")]
        public OutArgument<String> AddressLockBox { get; set; }

        [Output("outAddressPostDirection")]
        public OutArgument<String> AddressPostDirection { get; set; }

        [Output("outAddressPreDirection")]
        public OutArgument<String> AddressPreDirection { get; set; }

        [Output("outAddressPrivateMailboxName")]
        public OutArgument<String> AddressPrivateMailboxName { get; set; }

        [Output("outAddressPrivateMailboxRange")]
        public OutArgument<String> AddressPrivateMailboxRange { get; set; }

        [Output("outAddressRouteService")]
        public OutArgument<String> AddressRouteService { get; set; }

        [Output("outAddressStreetName")]
        public OutArgument<String> AddressStreetName { get; set; }

        [Output("outAddressStreetSuffix")]
        public OutArgument<String> AddressStreetSuffix { get; set; }

        [Output("outAddressSuiteName")]
        public OutArgument<String> AddressSuiteName { get; set; }

        [Output("outAddressSuiteNumber")]
        public OutArgument<String> AddressSuiteNumber { get; set; }

        [Output("outAddressTypeCode")]
        public OutArgument<String> AddressTypeCode { get; set; }

        [Output("outAreaCode")]
        public OutArgument<String> AreaCode { get; set; }

        [Output("outCarrierRoute")]
        public OutArgument<String> CarrierRoute { get; set; }

        [Output("outCBSACode")]
        public OutArgument<String> CBSACode { get; set; }

        [Output("outCBSADivisionCode")]
        public OutArgument<String> CBSADivisionCode { get; set; }

        [Output("outCBSADivisionLevel")]
        public OutArgument<String> CBSADivisionLevel { get; set; }

        [Output("outCBSADivisionTitle")]
        public OutArgument<String> CBSADivisionTitle { get; set; }

        [Output("outCBSALevel")]
        public OutArgument<String> CBSALevel { get; set; }

        [Output("outCBSATitle")]
        public OutArgument<String> CBSATitle { get; set; }

        [Output("outCensusBlock")]
        public OutArgument<String> CensusBlock { get; set; }

        [Output("outCensusKey")]
        public OutArgument<String> CensusKey { get; set; }

        [Output("outCensusTract")]
        public OutArgument<String> CensusTract { get; set; }

        [Input("inCity")]
        [Output("outCity")]
        public InOutArgument<String> City { get; set; }

        [Output("outCityAbbreviation")]
        public OutArgument<String> CityAbbreviation { get; set; }

        [Input("inCompanyName")]
        [Output("outCompanyName")]
        public InOutArgument<String> CompanyName { get; set; }

        [Output("outCongressionalDistrict")]
        public OutArgument<String> CongressionalDistrict { get; set; }

        [Input("inCountry")]
        public InArgument<String> Country { get; set; }

        [Output("outCountryCode")]
        public OutArgument<String> CountryCode { get; set; }

        [Output("outCountryName")]
        public OutArgument<String> CountryName { get; set; }

        [Output("outCountyFIPS")]
        public OutArgument<String> CountyFIPS { get; set; }

        [Output("outCountyName")]
        public OutArgument<String> CountyName { get; set; }

        [Output("outCountySubdivisionCode")]
        public OutArgument<String> CountySubdivisionCode { get; set; }

        [Output("outCountySubdivisionName")]
        public OutArgument<String> CountySubdivisionName { get; set; }

        [Output("outDateOfBirth")]
        public OutArgument<String> DateOfBirth { get; set; }

        [Output("outDateOfDeath")]
        public OutArgument<String> DateOfDeath { get; set; }

        [Output("outDeliveryIndicator")]
        public OutArgument<String> DeliveryIndicator { get; set; }

        [Output("outDeliveryPointCheckDigit")]
        public OutArgument<String> DeliveryPointCheckDigit { get; set; }

        [Output("outDeliveryPointCode")]
        public OutArgument<String> DeliveryPointCode { get; set; }

        [Output("outDemographicsGender")]
        public OutArgument<String> DemographicsGender { get; set; }

        [Output("outDemographicsResults")]
        public OutArgument<String> DemographicsResults { get; set; }

        [Output("outDomainName")]
        public OutArgument<String> DomainName { get; set; }

        [Output("outElementarySchoolDistrictCode")]
        public OutArgument<String> ElementarySchoolDistrictCode { get; set; }

        [Output("outElementarySchoolDistrictName")]
        public OutArgument<String> ElementarySchoolDistrictName { get; set; }

        [Input("inEmailAddress")]
        [Output("outEmailAddress")]
        public InOutArgument<String> EmailAddress { get; set; }

        [Output("outGender")]
        public OutArgument<String> Gender { get; set; }

        [Output("outGender2")]
        public OutArgument<String> Gender2 { get; set; }

        [Input("inFirstName")]
        public InArgument<String> FirstName { get; set; }

        [Input("inFreeForm")]
        public InArgument<String> FreeForm { get; set; }

        [Input("inFullName")]
        public InArgument<String> FullName { get; set; }

        [Output("outHouseholdIncome")]
        public OutArgument<String> HouseholdIncome { get; set; }

        [Input("inLastLine")]
        public InArgument<String> LastLine { get; set; }

        [Input("inLastName")]
        public InArgument<String> LastName { get; set; }

        [Output("outLatitude")]
        public OutArgument<String> Latitude { get; set; }

        [Output("outLengthOfResidence")]
        public OutArgument<String> LengthOfResidence { get; set; }

        [Output("outLongitude")]
        public OutArgument<String> Longitude { get; set; }

        [Output("outMailboxName")]
        public OutArgument<String> MailboxName { get; set; }

        [Output("outMaritalStatus")]
        public OutArgument<String> MaritalStatus { get; set; }

        [Output("outMelissaAddressKey")]
        public OutArgument<String> MelissaAddressKey { get; set; }

        [Output("outMelissaAddressKeyBase")]
        public OutArgument<String> MelissaAddressKeyBase { get; set; }

        [Output("outNameFirst")]
        public OutArgument<String> NameFirst { get; set; }

        [Output("outNameFirst2")]
        public OutArgument<String> NameFirst2 { get; set; }

        [Output("outNameFull")]
        public OutArgument<String> NameFull { get; set; }

        [Output("outNameLast")]
        public OutArgument<String> NameLast { get; set; }

        [Output("outNameLast2")]
        public OutArgument<String> NameLast2 { get; set; }

        [Output("outNameMiddle")]
        public OutArgument<String> NameMiddle { get; set; }

        [Output("outNameMiddle2")]
        public OutArgument<String> NameMiddle2 { get; set; }

        [Output("outNamePrefix")]
        public OutArgument<String> NamePrefix { get; set; }

        [Output("outNamePrefix2")]
        public OutArgument<String> NamePrefix2 { get; set; }

        [Output("outNameSuffix")]
        public OutArgument<String> NameSuffix { get; set; }

        [Output("outNameSuffix2")]
        public OutArgument<String> NameSuffix2 { get; set; }

        [Output("outNewAreaCode")]
        public OutArgument<String> NewAreaCode { get; set; }

        [Output("outOccupation")]
        public OutArgument<String> Occupation { get; set; }

        [Output("outOwnRent")]
        public OutArgument<String> OwnRent { get; set; }

        [Output("outPhoneCountryCode")]
        public OutArgument<String> PhoneCountryCode { get; set; }

        [Output("outPhoneCountryName")]
        public OutArgument<String> PhoneCountryName { get; set; }

        [Output("outPhoneExtension")]
        public OutArgument<String> PhoneExtension { get; set; }

        [Input("inPhoneNumber")]
        [Output("outPhoneNumber")]
        public InOutArgument<String> PhoneNumber { get; set; }

        [Output("outPhonePrefix")]
        public OutArgument<String> PhonePrefix { get; set; }

        [Output("outPhoneSuffix")]
        public OutArgument<String> PhoneSuffix { get; set; }

        [Output("outPlaceCode")]
        public OutArgument<String> PlaceCode { get; set; }

        [Output("outPlaceName")]
        public OutArgument<String> PlaceName { get; set; }

        [Output("outPlus4")]
        public OutArgument<String> Plus4 { get; set; }

        [Input("inPostalCode")]
        [Output("outPostalCode")]
        public InOutArgument<String> PostalCode { get; set; }

        [Output("outPresenceOfChildren")]
        public OutArgument<String> PresenceOfChildren { get; set; }

        [Output("outPrivateMailBox")]
        public OutArgument<String> PrivateMailBox { get; set; }

        [Output("outRecordExtras")]
        public OutArgument<String> RecordExtras { get; set; }

        [Input("inRecordID")]
        [Output("outRecordID")]
        public InOutArgument<String> RecordID { get; set; }

        [Input("inReserved")]
        [Output("outReserved")]
        public InOutArgument<String> Reserved { get; set; }

        [Output("outResults")]
        public OutArgument<String> Results { get; set; }

        [Output("outSalutation")]
        public OutArgument<String> Salutation { get; set; }

        [Output("outSecondarySchoolDistrictCode")]
        public OutArgument<String> SecondarySchoolDistrictCode { get; set; }

        [Output("outSecondarySchoolDistrictName")]
        public OutArgument<String> SecondarySchoolDistrictName { get; set; }

        [Input("inState")]
        [Output("outState")]
        public InOutArgument<String> State { get; set; }

        [Output("outStateDistrictLower")]
        public OutArgument<String> StateDistrictLower { get; set; }

        [Output("outStateDistrictUpper")]
        public OutArgument<String> StateDistrictUpper { get; set; }

        [Output("outStateName")]
        public OutArgument<String> StateName { get; set; }

        [Output("outSuite")]
        public OutArgument<String> Suite { get; set; }

        [Output("outTopLevelDomain")]
        public OutArgument<String> TopLevelDomain { get; set; }

        [Output("outUnifiedSchoolDistrictCode")]
        public OutArgument<String> UnifiedSchoolDistrictCode { get; set; }

        [Output("outUnifiedSchoolDistrictName")]
        public OutArgument<String> UnifiedSchoolDistrictName { get; set; }

        [Output("outUrbanizationName")]
        public OutArgument<String> UrbanizationName { get; set; }

        [Output("outUTC")]
        public OutArgument<String> UTC { get; set; }

        [Output("outError")]
        public OutArgument<String> Error { get; set; }

        [Output("outTransmissionResults")]
        public OutArgument<String> TransmissionResults { get; set; }

        [Output("outProcessedResults")]
        public OutArgument<String> ProcessedResults { get; set; }

        [Output("outMoveDate")]
        public OutArgument<String> MoveDate { get; set; }

        [Output("outHouseholdSize")]
        public OutArgument<String> HouseholdSize { get; set; }

        [Output("outCreditCardUser")]
        public OutArgument<String> CreditCardUser { get; set; }

        [Output("outPresenceOfSenior")]
        public OutArgument<String> PresenceOfSenior { get; set; }

        [Output("outChildrenAgeRange")]
        public OutArgument<String> ChildrenAgeRange { get; set; }

        [Output("outEducation")]
        public OutArgument<String> Education { get; set; }

        [Output("outPoliticalParty")]
        public OutArgument<String> PoliticalParty { get; set; }

        [Output("outTypeOfVehicles")]
        public OutArgument<String> TypeOfVehicles { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //initialize
            HttpWebRequest request;
            //HttpWebResponse response;
            WebResponse response;
            System.IO.Stream responseStream;
            System.IO.MemoryStream JSONStream;
            Uri address;
            String tServer = "https://personator.melissadata.net/v3/WEB/ContactVerify/doContactVerify";
            StreamReader streamReader;

            String license = CustomerID.Get(context);

            //License to CustomerID generation
            HttpWebRequest tokenRequest;
            HttpWebResponse tokenResponse;
            System.IO.Stream tokenResponseStream;
            Uri tokenAddress;

            String tokenServer = "https://token.melissadata.net/v3/WEB/service.svc/QueryCustomerInfo?P=&K=&L=";
            license = Uri.EscapeDataString(license);
            tokenServer += license;
            tokenAddress = new Uri(tokenServer);
            tokenRequest = (HttpWebRequest)WebRequest.Create(tokenAddress);
            tokenResponse = (HttpWebResponse)tokenRequest.GetResponse();
            tokenResponseStream = tokenResponse.GetResponseStream();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(tokenResponseStream);

            XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("/QueryCustomerInfoResponse/CustomerId");
            String id = node.InnerText;

            tokenResponseStream.Close();
            tokenResponse.Close();

            //set input
            tServer += "?t=" + "mdSrc:{product:DYN2016;version:1.5.5.16}";
            tServer += "&format=" + "json";
            tServer += "&id=" + id;
            tServer += "&act=" + Actions.Get(context);
            tServer += "&cols=" + Columns.Get(context);
            tServer += "&opt=" + Options.Get(context);
            tServer += "&first=";
            tServer += "&last=";
            tServer += "&full=" + FullName.Get(context);
            tServer += "&comp=" + CompanyName.Get(context);
            tServer += "&a1=" + AddressLine1.Get(context);
            tServer += "&a2=" + AddressLine2.Get(context);
            tServer += "&city=" + City.Get(context);
            tServer += "&state=" + State.Get(context);
            tServer += "&postal=" + PostalCode.Get(context);
            tServer += "&ctry=" + Country.Get(context);
            tServer += "&lastlines=" + LastLine.Get(context);
            tServer += "&freeform=" + FreeForm.Get(context);
            tServer += "&email=" + EmailAddress.Get(context);
            tServer += "&phone=" + PhoneNumber.Get(context);
            tServer += "&reserved=" + Reserved.Get(context);

            //form URL and create request
            //tServer = Uri.EscapeDataString(tServer);

            /*String outCity = tServer;
            City.Set(context, outCity);*/

            address = new Uri(tServer); //The format of the URI could not be determined error happens here
            request = (HttpWebRequest)WebRequest.Create(address);

            //Grab response
            String errorMessage = "";
            try
            {
                response = request.GetResponse();

                //Grab stream of response
                responseStream = response.GetResponseStream();

                JSONStream = new System.IO.MemoryStream();
                responseStream.CopyTo(JSONStream);
                streamReader = new StreamReader(JSONStream);
                JSONStream.Position = 0;
                System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(PersonatorResponse));
                PersonatorResponse PersonatorData = (PersonatorResponse)serializer.ReadObject(JSONStream);

                String outAddressDeliveryInstallation = "";
                String outAddressExtras = "";
                String outAddressHouseNumber = "";
                String outAddressKey = "";
                String outAddressLine1 = "";
                String outAddressLine2 = "";
                String outAddressLockBox = "";
                String outAddressPostDirection = "";
                String outAddressPreDirection = "";
                String outAddressPrivateMailboxName = "";
                String outAddressPrivateMailboxRange = "";
                String outAddressRouteService = "";
                String outAddressStreetName = "";
                String outAddressStreetSuffix = "";
                String outAddressSuiteName = "";
                String outAddressSuiteNumber = "";
                String outAddressTypeCode = "";
                String outAreaCode = "";
                String outCarrierRoute = "";
                String outCBSACode = "";
                String outCBSADivisionCode = "";
                String outCBSADivisionLevel = "";
                String outCBSADivisionTitle = "";
                String outCBSALevel = "";
                String outCBSATitle = "";
                String outCensusBlock = "";
                String outCensusKey = "";
                String outCensusTract = "";
                String outCity = "";
                String outCityAbbreviation = "";
                String outCompanyName = "";
                String outCongressionalDistrict = "";
                String outCountryCode = "";
                String outCountryName = "";
                String outCountyFIPS = "";
                String outCountyName = "";
                String outCountySubdivisionCode = "";
                String outCountySubdivisionName = "";
                String outDateOfBirth = "";
                String outDateOfDeath = "";
                String outDeliveryIndicator = "";
                String outDeliveryPointCheckDigit = "";
                String outDeliveryPointCode = "";
                String outDemographicsGender = "";
                String outDemographicsResults = "";
                String outDomainName = "";
                String outElementarySchoolDistrictCode = "";
                String outElementarySchoolDistrictName = "";
                String outEmailAddress = "";
                String outGender = "";
                String outGender2 = "";
                String outHouseholdIncome = "";
                String outLatitude = "";
                String outLengthOfResidence = "";
                String outLongitude = "";
                String outMailboxName = "";
                String outMaritalStatus = "";
                String outMelissaAddressKey = "";
                String outMelissaAddressKeyBase = "";
                String outNameFirst = "";
                String outNameFirst2 = "";
                String outNameFull = "";
                String outNameLast = "";
                String outNameLast2 = "";
                String outNameMiddle = "";
                String outNameMiddle2 = "";
                String outNamePrefix = "";
                String outNamePrefix2 = "";
                String outNameSuffix = "";
                String outNameSuffix2 = "";
                String outNewAreaCode = "";
                String outOccupation = "";
                String outOwnRent = "";
                String outPhoneCountryCode = "";
                String outPhoneCountryName = "";
                String outPhoneExtension = "";
                String outPhoneNumber = "";
                String outPhonePrefix = "";
                String outPhoneSuffix = "";
                String outPlaceCode = "";
                String outPlaceName = "";
                String outPlus4 = "";
                String outPostalCode = "";
                String outPresenceOfChildren = "";
                String outPrivateMailBox = "";
                String outRecordExtras = "";
                String outRecordID = "";
                String outReserved = "";
                String outResults = ""; //here
                String outSalutation = "";
                String outSecondarySchoolDistrictCode = "";
                String outSecondarySchoolDistrictName = "";
                String outState = "";
                String outStateDistrictLower = "";
                String outStateDistrictUpper = "";
                String outStateName = "";
                String outSuite = "";
                String outTopLevelDomain = "";
                String outUnifiedSchoolDistrictCode = "";
                String outUnifiedSchoolDistrictName = "";
                String outUrbanizationName = "";
                String outUTC = "";
                String outTransmissionResults = "";
                //outTransmissionResults = response.TransmissionResults;
                String outError = errorMessage;
                String outProcessedResults = "";

                String outMoveDate = "";
                String outHouseholdSize = "";
                String outCreditCardUser = "";
                String outPresenceOfSenior = "";
                String outChildrenAgeRange = "";
                String outEducation = "";
                String outPoliticalParty = "";
                String outTypeOfVehicles = "";


                if (errorMessage.Length > 0)
                {
                    outError = errorMessage;
                }
                else if (PersonatorData.TransmissionResults.Length - 1 > 0)
                {
                    outError = "Transmission Results: " + PersonatorData.Records[0].TransmissionResults + ", CustomerID: " + CustomerID.Get(context) + ", Actions:" + Actions.Get(context) + ", Options:" + Options.Get(context) + ", Columns:" + Columns.Get(context);
                }
                else
                {
                    //PersonatorWS.ResponseRecord resRecord = new PersonatorWS.ResponseRecord();
                    //response.Records[0] = response.Records[0];



                    //Basic
                    outAddressExtras = PersonatorData.Records[0].AddressExtras; //start
                    outAddressKey = PersonatorData.Records[0].AddressKey;
                    outAddressLine1 = PersonatorData.Records[0].AddressLine1;
                    outAddressLine2 = PersonatorData.Records[0].AddressLine2;
                    outCity = PersonatorData.Records[0].City;
                    outCompanyName = PersonatorData.Records[0].CompanyName;
                    outEmailAddress = PersonatorData.Records[0].EmailAddress;
                    outNameFull = PersonatorData.Records[0].NameFull;
                    outPhoneNumber = PersonatorData.Records[0].PhoneNumber;
                    outPostalCode = PersonatorData.Records[0].PostalCode;
                    outState = PersonatorData.Records[0].State;
                    outRecordID = PersonatorData.Records[0].RecordID;
                    outReserved = PersonatorData.Records[0].Reserved;

                    //Address Details
                    if (Columns.Get(context).Contains("GrpAddressDetails"))
                    {
                        outAddressTypeCode = PersonatorData.Records[0].AddressTypeCode;
                        outCarrierRoute = PersonatorData.Records[0].CarrierRoute;
                        outCityAbbreviation = PersonatorData.Records[0].CityAbbreviation;
                        outCountryCode = PersonatorData.Records[0].CountryCode;
                        outCountryName = PersonatorData.Records[0].CountryName;
                        outDeliveryIndicator = PersonatorData.Records[0].DeliveryIndicator;
                        outDeliveryPointCheckDigit = PersonatorData.Records[0].DeliveryPointCheckDigit;
                        outDeliveryPointCode = PersonatorData.Records[0].DeliveryPointCode;
                        outStateName = PersonatorData.Records[0].StateName;
                        outUrbanizationName = PersonatorData.Records[0].UrbanizationName;
                        outUTC = PersonatorData.Records[0].UTC;
                    }

                    //Census Details
                    if (Columns.Get(context).Contains("GrpCensus"))
                    {
                        outCBSACode = PersonatorData.Records[0].CBSACode;
                        outCBSADivisionCode = PersonatorData.Records[0].CBSADivisionCode;
                        outCBSADivisionLevel = PersonatorData.Records[0].CBSADivisionLevel;
                        outCBSADivisionTitle = PersonatorData.Records[0].CBSADivisionTitle;
                        outCBSALevel = PersonatorData.Records[0].CBSALevel;
                        outCBSATitle = PersonatorData.Records[0].CBSATitle;
                        outCensusBlock = PersonatorData.Records[0].CensusBlock;
                        outCensusTract = PersonatorData.Records[0].CensusTract;
                        outCongressionalDistrict = PersonatorData.Records[0].CongressionalDistrict;
                        outCountyFIPS = PersonatorData.Records[0].CountyFIPS;
                        outCountyName = PersonatorData.Records[0].CountyName;
                        outPlaceCode = PersonatorData.Records[0].PlaceCode;
                        outPlaceName = PersonatorData.Records[0].PlaceName;
                    }



                    //Census 2
                    if (Columns.Get(context).Contains("GrpCensus2"))
                    {
                        outCensusKey = PersonatorData.Records[0].CensusKey;
                        outCountySubdivisionCode = PersonatorData.Records[0].CountySubdivisionCode;
                        outCountySubdivisionName = PersonatorData.Records[0].CountySubdivisionName;
                        outElementarySchoolDistrictCode = PersonatorData.Records[0].ElementarySchoolDistrictCode;
                        outElementarySchoolDistrictName = PersonatorData.Records[0].ElementarySchoolDistrictName;
                        outSecondarySchoolDistrictCode = PersonatorData.Records[0].SecondarySchoolDistrictCode;
                        outSecondarySchoolDistrictName = PersonatorData.Records[0].SecondarySchoolDistrictName;
                        outUnifiedSchoolDistrictCode = PersonatorData.Records[0].UnifiedSchoolDistrictCode;
                        outUnifiedSchoolDistrictName = PersonatorData.Records[0].UnifiedSchoolDistrictName;
                        outStateDistrictLower = PersonatorData.Records[0].StateDistrictLower;
                        outStateDistrictUpper = PersonatorData.Records[0].StateDistrictUpper;
                    }


                    //Geocode
                    if (Columns.Get(context).Contains("GrpGeocode"))
                    {
                        outLatitude = PersonatorData.Records[0].Latitude;
                        outLongitude = PersonatorData.Records[0].Longitude;
                    }

                    //Demographics
                    if (Columns.Get(context).Contains("GrpDemographicBasic"))
                    {
                        outDateOfBirth = PersonatorData.Records[0].DateOfBirth;
                        outHouseholdIncome = PersonatorData.Records[0].HouseholdIncome;
                        outLengthOfResidence = PersonatorData.Records[0].LengthOfResidence;
                        outPresenceOfChildren = PersonatorData.Records[0].PresenceOfChildren;
                        outMaritalStatus = PersonatorData.Records[0].MaritalStatus;
                        outDateOfDeath = PersonatorData.Records[0].DateOfDeath;
                        outDemographicsGender = PersonatorData.Records[0].DemographicsGender;
                        outDemographicsResults = PersonatorData.Records[0].DemographicsResults;
                        outOwnRent = PersonatorData.Records[0].OwnRent;
                        outOccupation = PersonatorData.Records[0].Occupation;
                    }

                    //Additional Demographics
                    if(false)
                    {
                        outMoveDate = PersonatorData.Records[0].MoveDate;
                        outHouseholdSize = PersonatorData.Records[0].HouseholdSize;
                        outCreditCardUser = PersonatorData.Records[0].CreditCardUser;
                        outPresenceOfSenior = PersonatorData.Records[0].PresenceOfSenior;
                        outChildrenAgeRange = PersonatorData.Records[0].ChildrenAgeRange;
                        outEducation = PersonatorData.Records[0].Education;
                        outPoliticalParty = PersonatorData.Records[0].PoliticalParty;
                        outTypeOfVehicles = PersonatorData.Records[0].TypeOfVehicles;

                    }


                    //Name Details
                    if (Columns.Get(context).Contains("GrpNameDetails"))
                    {
                        outGender = PersonatorData.Records[0].Gender;
                        outGender2 = PersonatorData.Records[0].Gender2;
                        outNameFirst = PersonatorData.Records[0].NameFirst;
                        outNameFirst2 = PersonatorData.Records[0].NameFirst2;
                        outNameLast = PersonatorData.Records[0].NameLast;
                        outNameLast2 = PersonatorData.Records[0].NameLast2;
                        outNameMiddle = PersonatorData.Records[0].NameMiddle;
                        outNameMiddle2 = PersonatorData.Records[0].NameMiddle2;
                        outNamePrefix = PersonatorData.Records[0].NamePrefix;
                        outNamePrefix2 = PersonatorData.Records[0].NamePrefix2;
                        outNameSuffix = PersonatorData.Records[0].NameSuffix;
                        outNameSuffix2 = PersonatorData.Records[0].NameSuffix2;
                        outSalutation = PersonatorData.Records[0].Salutation;
                    }

                    //Parsed Address
                    if (Columns.Get(context).Contains("GrpParsedAddress"))
                    {
                        outAddressDeliveryInstallation = PersonatorData.Records[0].AddressDeliveryInstallation;
                        outAddressHouseNumber = PersonatorData.Records[0].AddressHouseNumber;
                        outAddressLockBox = PersonatorData.Records[0].AddressLockBox;
                        outAddressPostDirection = PersonatorData.Records[0].AddressPostDirection;
                        outAddressPreDirection = PersonatorData.Records[0].AddressPreDirection;
                        outAddressPrivateMailboxName = PersonatorData.Records[0].AddressPrivateMailboxName;
                        outAddressPrivateMailboxRange = PersonatorData.Records[0].AddressPrivateMailboxRange;
                        outAddressRouteService = PersonatorData.Records[0].AddressRouteService;
                        outAddressStreetName = PersonatorData.Records[0].AddressStreetName;
                        outAddressStreetSuffix = PersonatorData.Records[0].AddressStreetSuffix;
                        outAddressSuiteName = PersonatorData.Records[0].AddressSuiteName;
                        outAddressSuiteNumber = PersonatorData.Records[0].AddressSuiteNumber;
                    }

                    //Parsed Email
                    if (Columns.Get(context).Contains("GrpParsedEmail"))
                    {
                        outDomainName = PersonatorData.Records[0].DomainName;
                        outMailboxName = PersonatorData.Records[0].MailboxName;
                        outTopLevelDomain = PersonatorData.Records[0].TopLevelDomain;
                    }

                    //Parsed Phone
                    if (Columns.Get(context).Contains("GrpParsedPhone"))
                    {
                        outAreaCode = PersonatorData.Records[0].AreaCode;
                        outNewAreaCode = PersonatorData.Records[0].NewAreaCode;
                        outPhoneCountryCode = PersonatorData.Records[0].PhoneCountryCode;
                        outPhoneCountryName = PersonatorData.Records[0].PhoneCountryName;
                        outPhoneExtension = PersonatorData.Records[0].PhoneExtension;
                        outPhonePrefix = PersonatorData.Records[0].PhonePrefix;
                        outPhoneSuffix = PersonatorData.Records[0].PhoneSuffix;
                    }


                    

                    //Additional output fields


                    outMelissaAddressKey = PersonatorData.Records[0].MelissaAddressKey;
                    outMelissaAddressKeyBase = PersonatorData.Records[0].MelissaAddressKeyBase;
                    outRecordExtras = PersonatorData.Records[0].RecordExtras;
                    outResults = PersonatorData.Records[0].Results;
                    outPlus4 = PersonatorData.Records[0].Plus4;
                    outPrivateMailBox = PersonatorData.Records[0].PrivateMailBox;
                    outSuite = PersonatorData.Records[0].Suite;
                    outError = "";


                    //Address Details
                    if (outResults.Contains("AS01"))
                    {
                        outProcessedResults += "AS01 - Good Address\n";
                    }
                    else if (outResults.Contains("AS02"))
                    {
                        outProcessedResults += "AS02 - Missing/Invalid Suit Information\n";
                    }
                    else if (outResults.Contains("AS03"))
                    {
                        outProcessedResults += "AS03 - Good Address\n";
                    }
                    else if (outResults.Contains("AS"))
                    {
                        int index = outResults.IndexOf("AS");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Address\n";
                    }
                    else if (outResults.Contains("AE"))
                    {
                        int index = outResults.IndexOf("AE");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Address\n";
                    }

                    //Email Results
                    if (outResults.Contains("ES01"))
                    {
                        outProcessedResults += "ES01 - Good Address\n";
                    }
                    else if (outResults.Contains("ES03"))
                    {
                        outProcessedResults += "ES03 - Status of Email Unknown\n";
                    }
                    else if (outResults.Contains("ES"))
                    {
                        int index = outResults.IndexOf("ES");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Email\n";
                    }
                    else if (outResults.Contains("EE"))
                    {
                        int index = outResults.IndexOf("EE");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Email\n";
                    }

                    //Geocode Details
                    if (outResults.Contains("GE"))
                    {
                        int index = outResults.IndexOf("GE");
                        outProcessedResults += outResults.Substring(index, 4) + " - Geocode Error\n";
                    }
                    else if (outResults.Contains("G"))
                    {
                        int index = outResults.IndexOf("G");
                        outProcessedResults += outResults.Substring(index, 4) + " - Geocode Added\n";
                    }


                    //Name Results
                    if (outResults.Contains("NE"))
                    {
                        int index = outResults.IndexOf("NE");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Name\n";
                    }
                    else if (outResults.Contains("NS"))
                    {
                        int index = outResults.IndexOf("NS");
                        outProcessedResults += outResults.Substring(index, 4) + " - Good Name\n";
                    }


                    //Append
                    if (outResults.Contains("DA"))
                    {
                        outProcessedResults += "DA - Data has been appended\n";
                    }

                    //Verified
                    if (outResults.Contains("VR"))
                    {
                        outProcessedResults += "VR - Match has been verified\n";
                    }


                    //Phone Results
                    if (outResults.Contains("PE"))
                    {
                        int index = outResults.IndexOf("PE");
                        outProcessedResults += outResults.Substring(index, 4) + " - Bad Phone Number\n";
                    }
                    else if (outResults.Contains("PS"))
                    {
                        int index = outResults.IndexOf("PS");
                        outProcessedResults += outResults.Substring(index, 4) + " - Good Phone Number\n";
                    }
                }

                AddressDeliveryInstallation.Set(context, outAddressDeliveryInstallation);
                AddressExtras.Set(context, outAddressExtras);
                AddressHouseNumber.Set(context, outAddressHouseNumber);
                AddressKey.Set(context, outAddressKey);
                AddressLine1.Set(context, outAddressLine1);
                AddressLine2.Set(context, outAddressLine2);
                AddressLockBox.Set(context, outAddressLockBox);
                AddressPostDirection.Set(context, outAddressPostDirection);
                AddressPreDirection.Set(context, outAddressPreDirection);
                AddressPrivateMailboxName.Set(context, outAddressPrivateMailboxName);
                AddressPrivateMailboxRange.Set(context, outAddressPrivateMailboxRange);
                AddressRouteService.Set(context, outAddressRouteService);
                AddressStreetName.Set(context, outAddressStreetName);
                AddressStreetSuffix.Set(context, outAddressStreetSuffix);
                AddressSuiteName.Set(context, outAddressSuiteName);
                AddressSuiteNumber.Set(context, outAddressSuiteNumber);
                AddressTypeCode.Set(context, outAddressTypeCode);
                AreaCode.Set(context, outAreaCode);
                CarrierRoute.Set(context, outCarrierRoute);
                CBSACode.Set(context, outCBSACode);
                CBSADivisionCode.Set(context, outCBSADivisionCode);
                CBSADivisionLevel.Set(context, outCBSADivisionLevel);
                CBSADivisionTitle.Set(context, outCBSADivisionTitle);
                CBSALevel.Set(context, outCBSALevel);
                CBSATitle.Set(context, outCBSATitle);
                CensusBlock.Set(context, outCensusBlock);
                CensusKey.Set(context, outCensusKey);
                CensusTract.Set(context, outCensusTract);
                City.Set(context, outCity);
                CityAbbreviation.Set(context, outCityAbbreviation);
                CompanyName.Set(context, outCompanyName);
                CongressionalDistrict.Set(context, outCongressionalDistrict);
                CountryCode.Set(context, outCountryCode);
                CountryName.Set(context, outCountryName);
                CountyFIPS.Set(context, outCountyFIPS);
                CountyName.Set(context, outCountyName);
                CountySubdivisionCode.Set(context, outCountySubdivisionCode);
                CountySubdivisionName.Set(context, outCountySubdivisionName);
                DateOfBirth.Set(context, outDateOfBirth);
                DateOfDeath.Set(context, outDateOfDeath);
                DeliveryIndicator.Set(context, outDeliveryIndicator);
                DeliveryPointCheckDigit.Set(context, outDeliveryPointCheckDigit);
                DeliveryPointCode.Set(context, outDeliveryPointCode);
                DemographicsGender.Set(context, outDemographicsGender);
                DemographicsResults.Set(context, outDemographicsResults);
                DomainName.Set(context, outDomainName);
                ElementarySchoolDistrictCode.Set(context, outElementarySchoolDistrictCode);
                ElementarySchoolDistrictName.Set(context, outElementarySchoolDistrictName);
                EmailAddress.Set(context, outEmailAddress);
                Gender.Set(context, outGender);
                Gender2.Set(context, outGender2);
                HouseholdIncome.Set(context, outHouseholdIncome);
                Latitude.Set(context, outLatitude);
                LengthOfResidence.Set(context, outLengthOfResidence);
                Longitude.Set(context, outLongitude);
                MailboxName.Set(context, outMailboxName);
                MaritalStatus.Set(context, outMaritalStatus);
                MelissaAddressKey.Set(context, outMelissaAddressKey);
                MelissaAddressKeyBase.Set(context, outMelissaAddressKeyBase);
                NameFirst.Set(context, outNameFirst);
                NameFirst2.Set(context, outNameFirst2);
                NameFull.Set(context, outNameFull);
                NameLast.Set(context, outNameLast);
                NameLast2.Set(context, outNameLast2);
                NameMiddle.Set(context, outNameMiddle);
                NameMiddle2.Set(context, outNameMiddle2);
                NamePrefix.Set(context, outNamePrefix);
                NamePrefix2.Set(context, outNamePrefix2);
                NameSuffix.Set(context, outNameSuffix);
                NameSuffix2.Set(context, outNameSuffix2);
                NewAreaCode.Set(context, outNewAreaCode);
                Occupation.Set(context, outOccupation);
                OwnRent.Set(context, outOwnRent);
                PhoneCountryCode.Set(context, outPhoneCountryCode);
                PhoneCountryName.Set(context, outPhoneCountryName);
                PhoneExtension.Set(context, outPhoneExtension);
                PhoneNumber.Set(context, outPhoneNumber);
                PhonePrefix.Set(context, outPhonePrefix);
                PhoneSuffix.Set(context, outPhoneSuffix);
                PlaceCode.Set(context, outPlaceCode);
                PlaceName.Set(context, outPlaceName);
                Plus4.Set(context, outPlus4);
                PostalCode.Set(context, outPostalCode);
                PresenceOfChildren.Set(context, outPresenceOfChildren);
                PrivateMailBox.Set(context, outPrivateMailBox);
                RecordExtras.Set(context, outRecordExtras);
                RecordID.Set(context, outRecordID);
                Reserved.Set(context, outReserved);
                Results.Set(context, outResults);
                Salutation.Set(context, outSalutation);
                SecondarySchoolDistrictCode.Set(context, outSecondarySchoolDistrictCode);
                SecondarySchoolDistrictName.Set(context, outSecondarySchoolDistrictName);
                State.Set(context, outState);
                StateDistrictLower.Set(context, outStateDistrictLower);
                StateDistrictUpper.Set(context, outStateDistrictUpper);
                StateName.Set(context, outStateName);
                Suite.Set(context, outSuite);
                TopLevelDomain.Set(context, outTopLevelDomain);
                UnifiedSchoolDistrictCode.Set(context, outUnifiedSchoolDistrictCode);
                UnifiedSchoolDistrictName.Set(context, outUnifiedSchoolDistrictName);
                UrbanizationName.Set(context, outUrbanizationName);
                UTC.Set(context, outUTC);
                Error.Set(context, outError);
                TransmissionResults.Set(context, outTransmissionResults);
                ProcessedResults.Set(context, outProcessedResults);

                MoveDate.Set(context, outMoveDate);
                HouseholdSize.Set(context, outHouseholdSize);
                CreditCardUser.Set(context, outCreditCardUser);
                PresenceOfSenior.Set(context, outPresenceOfSenior);
                ChildrenAgeRange.Set(context, outChildrenAgeRange);
                Education.Set(context, outEducation);
                PoliticalParty.Set(context, outPoliticalParty);
                TypeOfVehicles.Set(context, outTypeOfVehicles);
            }
            catch (Exception ex)
            {
                errorMessage = "An error occurred while communicating with the web service, please retry and if the problem persists contact Melissa Data support. Error Message: " + ex.Message;
                Error.Set(context, errorMessage);
            }

        }
    }
}
